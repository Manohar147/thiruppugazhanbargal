import {Action} from '@ngrx/store';
import {CommentData, Comment, Reply} from '../write-to-us/write-to-us.component';

export const TRIGGER_SIDENAV = 'TRIGGER_SIDENAV';
export const SAVE_USER_COMMENT = 'SAVE_USER_COMMENT';
export const TRY_SAVE_USER_COMMENT = 'TRY_SAVE_USER_COMMENT';
export const TRY_UPDATE_COMMENT = 'TRY_UPDATE_COMMENT';
export const UPDATE_REPLY = 'UPDATE_REPLY';

export class TriggerSideNav implements Action {
  readonly type = TRIGGER_SIDENAV;

  constructor() {
  }
}

export class TrySaveComment implements Action {
  readonly type = TRY_SAVE_USER_COMMENT;

  constructor(public payload: CommentData) {
  }
}

export class SaveUserComment implements Action {
  readonly type = SAVE_USER_COMMENT;

  constructor(public payload: Comment[]) {
  }
}

export class TryUpdateComment implements Action {
  readonly type = TRY_UPDATE_COMMENT;

  constructor() {
  }
}

export class UpdateReply implements Action {
  readonly type = UPDATE_REPLY;

  constructor(public  payload: Reply) {
  }
}

export type  ToasterActions = TriggerSideNav | TrySaveComment | SaveUserComment;
