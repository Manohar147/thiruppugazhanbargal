import {Injectable} from '@angular/core';
import {Actions, Effect, ofType} from '@ngrx/effects';
import {FirebaseService} from '../fire/firebase.service';
import * as appActions from '../app-store/app.actions';
import {first, map, mergeMap, switchMap} from 'rxjs/operators';
import {CommentData, Reply, ReplyData} from '../write-to-us/write-to-us.component';
import {from} from 'rxjs';
import * as ToastActions from '../toaster/store/toaster.actions';

@Injectable()
export class AppEffects {
  constructor(private actions$: Actions, private fireBaseService: FirebaseService) {
  }

  @Effect()
  saveUserComment = this.actions$.pipe(
    ofType(appActions.TRY_SAVE_USER_COMMENT),
    map((action: appActions.TrySaveComment) => action.payload),
    switchMap((commentData: CommentData) => {
      return from(this.fireBaseService.saveUserComment(commentData).then(
        () => {
          return 200;
        },
        () => {
          return 404;
        }
      ));
    }),
    mergeMap((status: number) => {
      const actions = [];
      if (status === 200) {
        actions.push({
          type: ToastActions.TRIGGER_TOAST,
          payload: {status: 'SUCCESS', title: 'Success', message: 'Your response was recorded'},
        });
      } else {
        actions.push({
          type: ToastActions.TRIGGER_TOAST,
          payload: {status: 'FAILED', title: 'Failed', message: 'Some error has occurred please try again later'},
        });
      }
      return actions;
    })
  );

  @Effect()
  UpdateComments = this.actions$.pipe(
    ofType(appActions.TRY_UPDATE_COMMENT),
    switchMap(() => {
      return this.fireBaseService.updateComments();
    }),
    mergeMap((data: any) => {
      const actions = [];
      if (data) {
        actions.push({
          type: appActions.SAVE_USER_COMMENT,
          payload: data,
        });
      }
      return actions;
    })
  );

  @Effect()
  updateReply = this.actions$.pipe(
    ofType(appActions.UPDATE_REPLY),
    map((action: appActions.UpdateReply) => action.payload),
    switchMap((data: Reply) => {
      return this.fireBaseService.getExistingReplies(data.id).pipe(
        first(),
        map((response: any) => {
          return {
            existingReplies: response.replies,
            current: data
          };
        })
      );
    }),
    switchMap((data: { existingReplies: ReplyData[], current: Reply }) => {
      return from(this.fireBaseService.updateReply(data.current, data.existingReplies).then(
        () => 200,
        () => 404
      ));
    }),
    mergeMap((status: number) => {
      const actions = [];
      if (status === 200) {
        actions.push({
          type: ToastActions.TRIGGER_TOAST,
          payload: {status: 'SUCCESS', title: 'Success', message: 'Your reply was recorded'},
        });
      } else {
        actions.push({
          type: ToastActions.TRIGGER_TOAST,
          payload: {status: 'FAILED', title: 'Failed', message: 'Some error has occurred please try again later'},
        });
      }
      return actions;
    })
  );

}
