import * as appActions from './app.actions';

import {Comment} from '../write-to-us/write-to-us.component';

export interface State {
  comments: Comment[];
  sideNav: boolean;
}

const initialState: State = {
  comments: [],
  sideNav: false,
};

export function appReducer(state = initialState, action: appActions.ToasterActions) {
  switch (action.type) {
    case appActions.TRIGGER_SIDENAV :
      return {
        ...state,
        sideNav: !state.sideNav
      };
    case  appActions.SAVE_USER_COMMENT :
      return {
        ...state,
        comments: action.payload
      };
    default:
      return {
        ...state
      };
  }
}

