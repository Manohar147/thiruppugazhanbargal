import {Component, OnDestroy, OnInit, ViewChild} from '@angular/core';
import {FirebaseService} from './fire/firebase.service';
import * as AuthActions from './user-auth/auth-store/auth.actions';
import * as fromAuth from './user-auth/auth-store/auth.reducer';
import {Store} from '@ngrx/store';
import {FormBuilder, FormGroup} from '@angular/forms';
import {DomSanitizer} from '@angular/platform-browser';
import * as fromApp from './app-store/app.reducer';
import * as appActions from './app-store/app.actions';
import {MatSidenav} from '@angular/material';
import {AngularFirestore} from '@angular/fire/firestore';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit, OnDestroy {
  title = 'thiruppugazh-anbargal';
  options: FormGroup;
  isOpen = false;
  @ViewChild('sidenav') sidenav: MatSidenav;

  ngOnInit(): void {
    this.fireBaseService.refreshUserDetails();
    this.appStore.dispatch(new appActions.TryUpdateComment());
    this.store.select('app').pipe().subscribe(
      (response: fromApp.State) => {
        this.isOpen = response.sideNav;
      }
    );
  }

  closeSideNav() {
    this.store.dispatch(new appActions.TriggerSideNav());

  }


  constructor(private fireBaseService: FirebaseService,
              private store: Store<fromAuth.State>,
              private appStore: Store<fromApp.State>,
              fb: FormBuilder) {
    this.options = fb.group({
      bottom: 0,
      fixed: true,
      top: 60
    });
  }

  ngOnDestroy(): void {
    this.store.dispatch(new AuthActions.TryLogOut());
  }

}
