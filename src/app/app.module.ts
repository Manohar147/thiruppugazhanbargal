import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {AppComponent} from './app.component';
import {HeaderComponent} from './header/header.component';
import {NavComponent} from './header/nav/nav.component';
import {NavMwebComponent} from './header/nav/nav-mweb/nav-mweb.component';
import {NavToggleDirective} from './directives/nav-toggle.directive';
import {NavHideDirective} from './directives/nav-hide.directive';
import {MalihuScrollbarModule} from 'ngx-malihu-scrollbar';
import {SignInComponent} from './user-auth/user-authentication/sign-in/sign-in.component';
import {SignUpComponent} from './user-auth/user-authentication/sign-up/sign-up.component';
import {UserAuthenticationComponent} from './user-auth/user-authentication/user-authentication.component';
import {StoreModule} from '@ngrx/store';
import {reducers} from './store/common.reducer';
import {AngularFireModule} from '@angular/fire';
import {AngularFirestore, AngularFirestoreModule} from '@angular/fire/firestore';
import {environment} from '../environments/environment';
import {EffectsModule} from '@ngrx/effects';
import {AuthEffects} from './user-auth/auth-store/auth.effects';
import {FirebaseService} from './fire/firebase.service';
import {AngularFireAuth, AngularFireAuthModule} from '@angular/fire/auth';
import {AngularFireStorage, AngularFireStorageModule} from '@angular/fire/storage';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {ToasterModule, ToasterService} from 'angular2-toaster';
import {ToasterComponent} from './toaster/toaster.component';
import {SharedModule} from './shared-module/shared.module';
import {FooterComponent} from './footer/footer.component';
import {AboutUsComponent} from './about-us/about-us.component';
import {LoginGuardService} from './guards/login-guard.service';
import {PageNotFoundComponent} from './page-not-found/page-not-found.component';
import {BodyLinksComponent} from './body-links/body-links.component';
import {HomeComponent} from './home/home.component';
import {WriteToUsComponent} from './write-to-us/write-to-us.component';
import {CommentCardComponent} from './write-to-us/comment-card/comment-card.component';
import {ReplyCardComponent} from './write-to-us/comment-card/reply-card/reply-card.component';
import {AppEffects} from './app-store/app.effects';
import {ForgotPasswordDialogComponent} from './user-auth/user-authentication/sign-in/forgot-password-dialog/forgot-password-dialog.component';
import {CommentSnackbarComponent} from './write-to-us/comment-snackbar/comment-snackbar.component';


const appRoutes: Routes = [
  {path: '', redirectTo: 'Home', pathMatch: 'full'},
  {path: 'welcome', loadChildren: './welcome/welcome.module#WelcomeModule'},
  {
    path: 'user-auth', component: UserAuthenticationComponent, children: [
      {path: 'sign-in', component: SignInComponent},
      {path: 'sign-up', component: SignUpComponent}
    ], canActivate: [LoginGuardService]
  },
  {path: 'AboutUs', component: AboutUsComponent},
  {path: 'WriteToUs', component: WriteToUsComponent},
  {path: 'Home', component: HomeComponent},
  {path: '**', component: PageNotFoundComponent},
];

@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    NavComponent,
    NavMwebComponent,
    NavToggleDirective,
    NavHideDirective,
    UserAuthenticationComponent,
    ToasterComponent,
    FooterComponent,
    AboutUsComponent,
    PageNotFoundComponent,
    BodyLinksComponent,
    HomeComponent,
    WriteToUsComponent,
    CommentCardComponent,
    ReplyCardComponent,
    ForgotPasswordDialogComponent,
    CommentSnackbarComponent,
  ],
  imports: [
    SharedModule,
    BrowserModule,
    AngularFireModule.initializeApp(environment.firebaseConfig),
    AngularFirestoreModule,
    AngularFireAuthModule,
    AngularFireStorageModule,
    BrowserAnimationsModule,
    MalihuScrollbarModule.forRoot(),
    StoreModule.forRoot(reducers),
    EffectsModule.forRoot([AuthEffects, AppEffects]),
    ToasterModule.forRoot(),
    RouterModule.forRoot(appRoutes),
  ],
  entryComponents: [ForgotPasswordDialogComponent, CommentSnackbarComponent],
  providers: [FirebaseService, AngularFirestore, AngularFireAuth, AngularFireStorage, ToasterService,
    LoginGuardService],
  bootstrap: [AppComponent]
})
export class AppModule {
}
