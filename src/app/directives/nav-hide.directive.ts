import {Directive, HostListener, Renderer2} from '@angular/core';
import * as appActions from '../app-store/app.actions';
import {Store} from '@ngrx/store';
import * as fromApp from '../app-store/app.reducer';

@Directive({
  selector: '[appNavHide]'
})
export class NavHideDirective {

  @HostListener('click') onClick() {
    this.store.dispatch(new appActions.TriggerSideNav());
  }

  constructor(private store: Store<fromApp.State>) {
  }

}
