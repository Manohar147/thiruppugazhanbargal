import {Directive, ElementRef, Renderer2, ViewChild} from '@angular/core';
import {HostListener} from '@angular/core';
import {Store} from '@ngrx/store';
import * as fromApp from '../app-store/app.reducer';
import * as appActions from '../app-store/app.actions';

@Directive({
  selector: '[appNavToggle]'
})
export class NavToggleDirective {

  @ViewChild('sideNav') sideNav: ElementRef;

  constructor(private element: ElementRef, private renderer: Renderer2, private store: Store<fromApp.State>) {
  }

  @HostListener('click') onClick() {
    this.store.dispatch(new appActions.TriggerSideNav());
  }

}
