import {Injectable} from '@angular/core';
import * as FileSaver from 'file-saver';
import * as XLSX from 'xlsx';
import PlayList from '../welcome/data/playlist';
import Song from '../welcome/data/song';
import {User} from 'firebase';
import {ColInfo} from 'xlsx';

export interface SongExport {
  songName: string;
  newNumber: number;
  oldNumber: number;
  meaning: string;
  ragam: string;
  tamilMeaning: string;
  thalam: string;
  classify1: string;
  classify2: string;
  classify3: string;
}

@Injectable({
  providedIn: 'root'
})
export class ExcelExportService {

  public static exportPlaylistToExcel(playlist: PlayList, user: User) {
    ExcelExportService.exportAsExcelFile(playlist, ExcelExportService.filterSongList(playlist.playList), user);
  }

  private static exportAsExcelFile(playlist: PlayList, filteredArray: SongExport[], user: User): void {
    const workbook: XLSX.WorkBook = XLSX.utils.book_new();
    workbook.Props = {
      Title: playlist.playListName,
      Subject: playlist.description,
      Author: user.displayName,
      CreatedDate: new Date()
    };
    workbook.SheetNames.push('Song Sheet');
    // workbook.Sheets['Song Sheet'] = XLSX.utils.json_to_sheet(filteredArray, {
    //   header: ['Song Name', 'New Number', 'Old Number', 'Meaning', 'Ragam', 'Tamil Meaning', 'Thalam', 'Classify 1', 'Classify 2', 'Classify 3']
    // });
    workbook.Sheets['Song Sheet'] = XLSX.utils.json_to_sheet(filteredArray);
    const sheetCols: ColInfo[] = [];
    for (let i = 0; i < 10; i++) {
      sheetCols.push({width: 25});
    }
    for (let i = 0; i < filteredArray.length; i++) {
      for (let j = 0; j < 11; j++) {
        const cellRef = XLSX.utils.encode_cell({c: j, r: i});
        switch (j) {
          case 0:
            XLSX.utils.cell_set_hyperlink(workbook.Sheets['Song Sheet'][cellRef], playlist.playList[i].songUrl, playlist.playList[i].songName);
            break;
          case 5:
            XLSX.utils.cell_set_hyperlink(workbook.Sheets['Song Sheet'][cellRef], playlist.playList[i].tamilMeaningUrl, playlist.playList[i].tamilMeaning);
            break;
          case 6:
            XLSX.utils.cell_set_hyperlink(workbook.Sheets['Song Sheet'][cellRef], playlist.playList[i].thalamUrl, playlist.playList[i].thalam);
            break;
        }
      }
    }
    workbook.Sheets['Song Sheet']['!cols'] = sheetCols;

    const excelOut = XLSX.write(workbook, {bookType: 'xlsx', type: 'binary'});
    FileSaver.saveAs(new Blob([ExcelExportService.s2ab(excelOut)], {type: 'application/octet-stream'}), playlist.playListName + '.xlsx');
  }

  private static s2ab(s) {
    const buf = new ArrayBuffer(s.length);
    const view = new Uint8Array(buf);
    for (let i = 0; i < s.length; i++) {
      view[i] = s.charCodeAt(i) & 0xFF;
    }
    return buf;
  }


  constructor() {
  }

  private static filterSongList(songs: Song[]): SongExport[] {
    const exportArray: SongExport[] = [];
    for (let i = 0; i < songs.length; i++) {
      exportArray[i] = {
        songName: songs[i].songName,
        newNumber: songs[i].newNumber,
        oldNumber: songs[i].oldNumber,
        meaning: songs[i].meaning,
        ragam: songs[i].ragam,
        tamilMeaning: songs[i].tamilMeaning,
        thalam: songs[i].thalam,
        classify1: songs[i].classify1,
        classify2: songs[i].classify2,
        classify3: songs[i].classify3
      };
    }
    return exportArray;
  }
}
