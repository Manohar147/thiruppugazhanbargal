import {Injectable} from '@angular/core';
import {AngularFirestore} from '@angular/fire/firestore';
import {AngularFireAuth} from '@angular/fire/auth';
import {AngularFireStorage} from '@angular/fire/storage';
import {Store} from '@ngrx/store';
import * as fromAuth from '../user-auth/auth-store/auth.reducer';
import * as AuthActions from '../user-auth/auth-store/auth.actions';
import {User} from 'firebase';
import {UserLibrary} from '../user-auth/ThirupUser/user';
import {catchError, map, skip} from 'rxjs/operators';
import {throwError} from 'rxjs';
import {CommentData, Reply, ReplyData} from '../write-to-us/write-to-us.component';
import {MatSnackBar} from '@angular/material';
import {CommentSnackbarComponent} from '../write-to-us/comment-snackbar/comment-snackbar.component';


@Injectable()
export class FirebaseService {


  constructor(private firestore: AngularFirestore,
              private fireAuth: AngularFireAuth,
              private fireStorage: AngularFireStorage,
              private store: Store<fromAuth.State>,
              private _snackBar: MatSnackBar) {
  }

  createUser(email: string, password: string) {
    return this.fireAuth.auth.createUserWithEmailAndPassword(email, password);
  }

  logOutUser() {
    return this.fireAuth.auth.signOut();
  }

  updateProfile(firstName, lastName, photoUrl) {
    return this.fireAuth.auth.currentUser.updateProfile({
      displayName: firstName + ' ' + lastName,
      photoURL: photoUrl
    });
  }

  saveUserImage(image, authToken: string, email: string) {
    return this.fireStorage.ref('userImages/' + email).put(image);
  }

  getImageDownloadUrl(email) {
    return this.fireStorage.ref('userImages/' + email).getDownloadURL();
  }

  getUserToken() {
    return this.fireAuth.auth.currentUser.getIdToken();
  }

  signInUser(email: string, password: string) {
    return this.fireAuth.auth.signInWithEmailAndPassword(email, password);
  }


  addUserToLibrary(user: UserLibrary) {
    return this.firestore.collection('users').doc(user.uid).set({
      uid: user.uid,
      isAdmin: false,
      username: user.username,
      emailId: user.emailId,
      phoneNumber: user.phoneNumber,
      state: user.state,
      photoUrl: user.photoUrl
    });
  }

  getUserFromLibrary(uid: string) {
    try {
      return this.firestore.collection('users').doc(uid).valueChanges().pipe(
        catchError(() => throwError('Authentication Error...'))
      );
    } catch (e) {
      console.error('An Error has occurred');
    }

  }

  refreshUserDetails() {
    this.fireAuth.user.subscribe(
      (user: User) => {
        this.store.dispatch(new AuthActions.UpdateUser(user));
        if (user) {
          this.checkNewComments(user);
          this.store.dispatch(new AuthActions.TrySyncUser(user.uid));
          this.store.dispatch(new AuthActions.SignIn(true));
        } else {
          this.store.dispatch(new AuthActions.Logout());
        }
      }
    );
  }

  saveUserComment(comment: CommentData) {
    return this.firestore.collection('comments').add(comment).catch(
      error => console.error(error)
    );
  }

  updateComments() {
    return this.firestore.collection('comments', ref => ref.orderBy('commentedOn', 'desc').limit(20)).snapshotChanges().pipe(
      map((response: any) => {
        return response.map((comment) => {
          return {
            id: comment.payload.doc.id,
            comment: {
              ...comment.payload.doc.data(),
              commentedOn: comment.payload.doc.data().commentedOn.toDate(),
              replies: comment.payload.doc.data().replies.map((reply) => {
                return {
                  ...reply,
                  commentedOn: reply.commentedOn.toDate()
                };
              })
            }
          };
        });
      }),
      catchError(() => throwError('Auth Signed out Error!'))
    );
  }

  checkNewComments(user: User) {
    this.firestore.collection('comments', ref => ref.orderBy('commentedOn', 'desc').limit(20))
      .stateChanges().pipe(
      skip(1),
      catchError(() => throwError('Auth Signed out Error!'))
    ).subscribe(
      (data: any) => {
        if (data && data[0] && data[0].payload.doc.data() && data[0].type === 'added') {
          if (data[0].payload.doc.data().commentedUserUid && data[0].payload.doc.data().commentedUserUid !== user.uid) {
            if (data[0].payload.doc.data().commentedBy) {
              this.openSnackBar(data[0].payload.doc.data().commentedBy);
            }
          }
        }
      }
    );
  }

  openSnackBar(name) {
    this._snackBar.openFromComponent(CommentSnackbarComponent, {
      duration: 5 * 1000,
      data: {commentFrom: name, route: 'WriteToUs', queryParams: {}}
    });
  }

  updateReply(reply: Reply, exiting: ReplyData[]) {
    let replies: ReplyData[] = [];
    if (exiting) {
      replies = [...exiting];
    }
    return this.firestore.collection('comments').doc(reply.id).update({
      replies: [...replies, reply.reply]
    });

  }

  getExistingReplies(id: string) {
    return this.firestore.collection('comments').doc(id).valueChanges();
  }

  resetPassword(email: string) {
    return this.fireAuth.auth.sendPasswordResetEmail(email);
  }
}
