import {Injectable} from '@angular/core';
import {ActivatedRouteSnapshot, Resolve, RouterStateSnapshot} from '@angular/router';
import {Observable} from 'rxjs';

interface PageTitle {
  name: string;
}

@Injectable()
export class PageResolveGuardService implements Resolve<PageTitle> {

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<PageTitle> | Promise<PageTitle> | PageTitle {
    let name = 'Welcome';
    switch (+route.queryParams.page) {
      case 0:
        name = 'About Guruji';
        break;
      default:
        name = 'Welcome';
    }
    return {
      name
    };
  }


  constructor() {
  }
}
