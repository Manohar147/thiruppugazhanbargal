import {Injectable} from '@angular/core';
import {ActivatedRouteSnapshot, Resolve, RouterStateSnapshot} from '@angular/router';
import Song from '../welcome/data/song';
import {Observable, throwError} from 'rxjs';
import {Store} from '@ngrx/store';
import * as fromWelcome from '../welcome/store/welcome.reducer';
import {catchError, map, take} from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class SongsResolveGuardService implements Resolve<Song[]> {

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<Song[]> | Promise<Song[]> | Song[] {
    return this.welcomeStore.select('welcome').pipe(
      take(1),
      catchError(error => throwError('Error occurred in songs-resolve-guard.service.ts')),
      map((data: fromWelcome.State) => {
        return data.songs;
      })
    );
  }


  constructor(private welcomeStore: Store<fromWelcome.State>) {
  }
}
