import {Component, OnInit} from '@angular/core';
import {Observable} from 'rxjs';
import * as fromAuth from '../user-auth/auth-store/auth.reducer';
import * as fromApp from '../app-store/app.reducer';
import * as AuthActions from '../user-auth/auth-store/auth.actions';
import {Store} from '@ngrx/store';
import * as appActions from '../app-store/app.actions';
import {first} from 'rxjs/operators';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {

  public authState: Observable<fromAuth.State>;

  constructor(private store: Store<fromAuth.State>, private appStore: Store<fromApp.State>) {
  }

  ngOnInit() {
    this.authState = this.store.select('auth');
  }

  darkTheme(event) {
    if (event.checked) {
      document.getElementById('themeLink').setAttribute('href', 'https://thiruppugazhanbargal-515e8.firebaseapp.com/assets/themes/pink-bluegrey.css');
      localStorage.setItem('theme', 'https://thiruppugazhanbargal-515e8.firebaseapp.com/assets/themes/pink-bluegrey.css');
    } else {
      document.getElementById('themeLink').setAttribute('href', 'https://thiruppugazhanbargal-515e8.firebaseapp.com/assets/themes/indigo-pink.css');
      localStorage.setItem('theme', 'https://thiruppugazhanbargal-515e8.firebaseapp.com/assets/themes/indigo-pink.css');
    }
  }

  isDark() {
    return localStorage.getItem('theme') === 'https://thiruppugazhanbargal-515e8.firebaseapp.com/assets/themes/pink-bluegrey.css';
  }

  logOut() {
    this.store.dispatch(new AuthActions.TryLogOut());
  }

  closeSideNav() {
    this.appStore.select('app').pipe(
      first()
    ).subscribe(
      (state: fromApp.State) => {
        if (state.sideNav === true) {
          this.store.dispatch(new appActions.TriggerSideNav());
        }
      }
    );
  }

}
