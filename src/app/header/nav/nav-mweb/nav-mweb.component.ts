import {Component, OnInit, OnDestroy, ChangeDetectorRef, ViewChild, ElementRef} from '@angular/core';
import {MediaMatcher} from '@angular/cdk/layout';
import * as fromApp from '../../../app-store/app.reducer';
import {Store} from '@ngrx/store';
import {first} from 'rxjs/operators';


@Component({
  selector: 'app-nav-mweb',
  templateUrl: './nav-mweb.component.html',
  styleUrls: ['./nav-mweb.component.css']
})
export class NavMwebComponent implements OnInit, OnDestroy {
  public scrollbarOptions = {axis: 'yx', theme: 'minimal-dark'};
  private readonly _mobileQueryListener: () => void;
  mobileQuery: MediaQueryList;
  @ViewChild('sideNav') sideNav;
  isOpened: boolean = false;

  fillerNav = Array.from({length: 50}, (_, i) => `Nav Item ${i + 1}`);


  constructor(changeDetectorRef: ChangeDetectorRef, media: MediaMatcher, private store: Store<fromApp.State>) {
    this.mobileQuery = media.matchMedia('(max-width: 600px)');
    this._mobileQueryListener = () => changeDetectorRef.detectChanges();
    this.mobileQuery.addListener(this._mobileQueryListener);
  }

  ngOnDestroy(): void {
    this.mobileQuery.removeListener(this._mobileQueryListener);
  }

  ngOnInit() {

  }

}
