import {Component, OnInit} from '@angular/core';
import {Observable} from 'rxjs';
import * as fromAuth from '../user-auth/auth-store/auth.reducer';
import {Store} from '@ngrx/store';
import {Router} from '@angular/router';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {

  authStore: Observable<fromAuth.State>;


  constructor(private store: Store<fromAuth.State>, private router: Router) {
  }

  ngOnInit() {
    this.authStore = this.store.select('auth');

  }

}
