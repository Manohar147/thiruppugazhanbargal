import {NgModule} from '@angular/core';
import {
  MatBadgeModule,
  MatButtonModule, MatCardModule,
  MatCheckboxModule, MatChipsModule, MatDialogModule,
  MatDividerModule,
  MatExpansionModule, MatFormFieldModule,
  MatIconModule, MatInputModule,
  MatListModule, MatMenuModule, MatPaginatorModule,
  MatProgressBarModule, MatRadioModule, MatSidenavModule, MatSlideToggleModule, MatSnackBarModule,
  MatTableModule,
  MatTabsModule, MatToolbarModule, MatTooltipModule, MatBottomSheetModule
} from '@angular/material';

import {SignInComponent} from '../user-auth/user-authentication/sign-in/sign-in.component';
import {SignUpComponent} from '../user-auth/user-authentication/sign-up/sign-up.component';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {CommonModule} from '@angular/common';
import {RouterModule} from '@angular/router';
import {TruncatePipePipe} from '../pipes/truncate-pipe.pipe';
import {MatStepperModule} from '@angular/material/stepper';
import {LoginErrorSnackbarComponent} from '../welcome/snackbar/login-error-snackbar/login-error-snackbar.component';
import {SignInDialogComponent} from '../welcome/dialog/sign-in-dialog/sign-in-dialog.component';


@NgModule({
  declarations: [
    SignInComponent,
    SignUpComponent,
    TruncatePipePipe,
    LoginErrorSnackbarComponent,
    SignInDialogComponent
  ],
  imports: [
    RouterModule,
    CommonModule,
    ReactiveFormsModule,
    FormsModule,
    MatTableModule,
    MatExpansionModule,
    MatProgressBarModule,
    MatTabsModule,
    MatDividerModule,
    MatCheckboxModule,
    MatDialogModule,
    MatSnackBarModule,
    MatChipsModule,
    MatBottomSheetModule,
    MatTooltipModule,
    MatPaginatorModule,
    MatBadgeModule,
    MatToolbarModule,
    MatStepperModule,
    MatRadioModule,
    MatIconModule,
    MatListModule,
    MatFormFieldModule,
    MatInputModule,
    MatSidenavModule,
    MatCardModule,
    MatSlideToggleModule,
    MatButtonModule,
    MatMenuModule,
  ],
  exports: [
    MatTableModule,
    MatExpansionModule,
    MatProgressBarModule,
    MatTabsModule,
    MatDividerModule,
    MatCheckboxModule,
    MatDialogModule,
    MatSnackBarModule,
    MatChipsModule,
    MatTooltipModule,
    MatPaginatorModule,
    MatBadgeModule,
    MatToolbarModule,
    MatRadioModule,
    MatIconModule,
    MatListModule,
    MatFormFieldModule,
    MatInputModule,
    MatStepperModule,
    MatSidenavModule,
    MatCardModule,
    MatSlideToggleModule,
    MatButtonModule,
    MatMenuModule,
    SignInComponent,
    SignUpComponent,
    ReactiveFormsModule,
    FormsModule,
    RouterModule,
    TruncatePipePipe,
  ],
  entryComponents: [LoginErrorSnackbarComponent, SignInDialogComponent]
})
export class SharedModule {

}
