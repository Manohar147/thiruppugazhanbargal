import {ActionReducerMap} from '@ngrx/store';

import * as fromAuth from '../user-auth/auth-store/auth.reducer';
import * as fromToast from '../toaster/store/toaster.reducer';
import * as fromApp from '../app-store/app.reducer';

export interface AppState {
  auth: fromAuth.State;
  toast: fromToast.State;
  app: fromApp.State
}


export const reducers: ActionReducerMap<AppState> = {
  auth: fromAuth.authReducer,
  toast: fromToast.toastReducer,
  app: fromApp.appReducer
};
