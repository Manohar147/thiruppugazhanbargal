import {Action} from '@ngrx/store';

export const TRIGGER_TOAST = 'TRIGGER_TOAST';

export class TriggerToast implements Action {
  readonly type = TRIGGER_TOAST;

  constructor(public payload: { status: string, message: string, title: string }) {
  }
}


export type  ToasterActions = TriggerToast;
