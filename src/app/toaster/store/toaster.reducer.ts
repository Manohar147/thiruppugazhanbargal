import * as toasterActions from './toaster.actions';

export interface State {
  showToast: boolean;
  toastType: string;
  toastMessage: string;
  toastTitle: string;
}

const initialState: State = {
  showToast: false,
  toastType: null,
  toastMessage: null,
  toastTitle: null
};

export function toastReducer(state = initialState, action: toasterActions.ToasterActions) {
  switch (action.type) {
    case toasterActions.TRIGGER_TOAST :
      return {
        ...state,
        showToast: !state.showToast,
        toastType: action.payload.status,
        toastMessage: action.payload.message,
        toastTitle: action.payload.title
      };
    default:
      return {
        ...state
      };
  }
}
