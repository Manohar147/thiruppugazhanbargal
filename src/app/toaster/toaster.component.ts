import {Component, OnInit} from '@angular/core';
import {ToasterConfig, ToasterService} from 'angular2-toaster';
import {Store} from '@ngrx/store';
import * as fromToast from './store/toaster.reducer';
import * as toastActions from './store/toaster.actions';

@Component({
  selector: 'app-toaster',
  templateUrl: './toaster.component.html',
  styleUrls: ['./toaster.component.css']
})
export class ToasterComponent implements OnInit {
  public config: ToasterConfig =
    new ToasterConfig({
      animation: 'flyRight',
      positionClass: 'toast-bottom-right',
      newestOnTop: false,
      showCloseButton: false,
      tapToDismiss: true,
      timeout: 7000,
      limit: 1
    });

  constructor(private toasterService: ToasterService, private store: Store<fromToast.State>) {
  }

  ngOnInit() {
    this.store.select('toast').subscribe(
      (state: fromToast.State) => {
        if (state.showToast && state.toastType && state.toastMessage) {
          switch (state.toastType) {
            case 'SUCCESS':
              this.popToastSuccess(state.toastMessage, state.toastTitle);
              break;
            case 'FAILED':
              this.popToastFailed(state.toastMessage, state.toastTitle);
              break;
          }
          this.store.dispatch(new toastActions.TriggerToast({status: null, message: null, title: null}));
        }
      }
    );

  }

  popToastSuccess(message: string, title: string) {
    this.toasterService.pop('success', title, message);
  }

  popToastFailed(message: string, title: string) {
    this.toasterService.pop('error', title, message);
  }
}
