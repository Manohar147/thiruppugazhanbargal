import PlayList from '../../welcome/data/playlist';


export interface UserLibrary {
  uid: string;
  isAdmin: boolean;
  playList: PlayList[];
  username: string;
  emailId: string;
  phoneNumber: string;
  state: string;
  photoUrl: string;
}

export class UserClass implements UserLibrary {
  uid: string;
  isAdmin: boolean;
  playList: PlayList[];
  username: string;
  emailId: string;
  phoneNumber: string;
  state: string;
  photoUrl: string;

  constructor(uid: string, isAdmin: boolean, playList: PlayList[], username: string, emailId: string, phoneNumber: string, state: string,photoUrl: string) {
    this.uid = uid;
    this.isAdmin = isAdmin;
    this.playList = playList;
    this.username = username;
    this.emailId = emailId;
    this.phoneNumber = phoneNumber;
    this.state = state;
    this.photoUrl = photoUrl;
  }
}
