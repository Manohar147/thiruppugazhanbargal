export class ThirupUserDetails {
  public firstName: string;
  public lastName: string;
  public email: string;
  public password: string;
  public image: File;
  public imageUrl: string;
  public authToken: string;
}
