import {Action} from '@ngrx/store';
import {ThirupUserDetails} from '../ThirupUser/userDetails';
import {User} from 'firebase';
import {UserLibrary} from '../ThirupUser/user';

export const SIGN_IN = 'SIGN_IN';
export const TRY_SIGN_IN = 'TRY_SIGN_IN';
export const CHANGE_TITLE = 'CHANGE_TITLE';
export const SIGN_UP = 'SIGN_UP';
export const TRY_SIGN_UP = 'TRY_SIGN_UP';
export const LOG_OUT = 'LOG_OUT';
export const TRY_LOG_OUT = 'TRY_LOG_OUT';
export const GET_USER_TOKEN = 'GET_USER_TOKEN';
export const UPLOAD_USER_IMAGE = 'UPLOAD_USER_IMAGE';
export const GET_IMAGE_URL = 'GET_IMAGE_URL';
export const TRY_UPDATE_USER_PROFILE = 'TRY_UPDATE_USER_PROFILE';
export const FAILED_SIGN_UP = 'FAILED_SIGN_UP';
export const FAILED = 'FAILED';
export const MONITOR_USER = 'MONITOR_USER';
export const UPDATE_USER = 'UPDATE_USER';
export const GOOGLE_SIGN_IN = 'GOOGLE_SIGN_IN';
export const CREATE_USER_LIBRARY = 'CREATE_USER_LIBRARY';
export const TRY_SYNC_USER = 'TRY_SYNC_USER';
export const SYNC_USER = 'SYNC_USER';
export const SET_REDIRECT_ROUTE = 'SET_REDIRECT_ROUTE';
export const FORGOT_PASSWORD = 'FORGOT_PASSWORD';


export class ForgotPassword implements Action {
  readonly type = FORGOT_PASSWORD;

  constructor(public payload: string) {
  }
}

export class SetRedirectRoute implements Action {
  readonly type = SET_REDIRECT_ROUTE;

  constructor(public payload: string) {
  }
}


export class SignIn implements Action {
  readonly type = SIGN_IN;

  constructor(public payload: boolean) {
  }
}

export class TrySignIn implements Action {
  readonly type = TRY_SIGN_IN;

  constructor(public payload: { email: string, password: string, route: string, queryParams: Object }) {
  }
}

export class ChangeTitle implements Action {
  readonly type = CHANGE_TITLE;

  constructor(public payload: string) {
  }
}

export class SignUp implements Action {
  readonly type = SIGN_UP;

  constructor(public payload: boolean) {
  }
}

export class TrySignUp implements Action {
  readonly type = TRY_SIGN_UP;

  constructor(public payload: ThirupUserDetails) {
  }
}

export class GetUserToken implements Action {
  readonly type = GET_USER_TOKEN;

  constructor(public payload: ThirupUserDetails) {
  }
}

export class FailedSignUp implements Action {
  readonly type = FAILED_SIGN_UP;

  constructor(public payload: ThirupUserDetails) {

  }
}

export class UploadUserImage implements Action {
  readonly type = UPLOAD_USER_IMAGE;

  constructor(public payload: ThirupUserDetails) {

  }
}

export class GetImageUrl implements Action {
  readonly type = GET_IMAGE_URL;

  constructor(public payload: ThirupUserDetails) {

  }
}

export class TryUpdateUserProfile implements Action {
  readonly type = TRY_UPDATE_USER_PROFILE;

  constructor(public payload: ThirupUserDetails) {

  }
}


export class Logout implements Action {
  readonly type = LOG_OUT;
}

export class TryLogOut implements Action {
  readonly type = TRY_LOG_OUT;

}

export class MonitorUser implements Action {
  readonly type = MONITOR_USER;

  constructor(public payload: User) {
  }

}

export class UpdateUser implements Action {
  readonly type = UPDATE_USER;

  constructor(public payload: User) {
  }

}

export class GoogleSignIn implements Action {
  readonly type = GOOGLE_SIGN_IN;

  constructor(public payload: any) {
  }

}

export class CreateUserLibrary implements Action {
  readonly type = CREATE_USER_LIBRARY;

  constructor(public payload: UserLibrary) {
  }

}

export class TrySyncUser implements Action {
  readonly type = TRY_SYNC_USER;

  constructor(public payload: string) {
  }

}


export class SyncUser implements Action {
  readonly type = SYNC_USER;

  constructor(public payload: UserLibrary) {
  }

}


export type AuthActions = SignIn | SignUp | TrySignUp | GetUserToken |
  FailedSignUp | UploadUserImage | GetImageUrl | TryUpdateUserProfile | TrySyncUser | SyncUser
  | Logout | TryLogOut | ChangeTitle | MonitorUser | TrySignIn | UpdateUser | GoogleSignIn | CreateUserLibrary | SetRedirectRoute;
