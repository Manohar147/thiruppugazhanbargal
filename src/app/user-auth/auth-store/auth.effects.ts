import {Effect, Actions, ofType} from '@ngrx/effects';
import {Injectable} from '@angular/core';
import * as AuthActions from './auth.actions';
import * as ToastActions from '../../toaster/store/toaster.actions';
import * as fromAuth from './auth.reducer';
import {map, mergeMap, switchMap} from 'rxjs/operators';
import {FirebaseService} from '../../fire/firebase.service';
import {from} from 'rxjs';
import {ThirupUserDetails} from '../ThirupUser/userDetails';
import {Store} from '@ngrx/store';
import {Router} from '@angular/router';
import {UserClass, UserLibrary} from '../ThirupUser/user';

const ERROR_MESSAGE_1 = 'FAILED TO GET AUTH TOKEN';
const ERROR_MESSAGE_2 = 'FAILED TO UPLOAD USER IMAGE';
const ERROR_MESSAGE_3 = 'FAILED TO GET IMAGE URL';
const ERROR_MESSAGE_4 = 'ERROR';

@Injectable()
export class AuthEffects {
  @Effect()
  authSignUp = this.actions$.pipe(
    ofType(AuthActions.TRY_SIGN_UP),
    map((action: AuthActions.TrySignUp) => {
      return action.payload;
    }), switchMap(
      (authData: ThirupUserDetails) => {
        return from(
          this.fireBaseService.createUser(authData.email, authData.password)
            .then((success) => {
                this.router.navigate(['Home']).catch(
                  error => console.error('Routing Error : ', error)
                );
                return {
                  authData: {
                    ...authData
                  },
                  status: 200,
                  response: success
                };
              },
              failed => {
                return {
                  authData: {
                    ...authData
                  },
                  status: 400,
                  response: failed
                };
              })
            .catch(error => {
              return {
                authData: {
                  ...authData
                },
                status: 400,
                response: error
              };
            })
        );
      }
    ), mergeMap((response: { status: number, authData: ThirupUserDetails, response: any }) => {
      const actions = [];
      let type = AuthActions.FAILED_SIGN_UP;
      if (response.status === 200) {
        type = AuthActions.GET_USER_TOKEN;
        actions.push(
          {type: ToastActions.TRIGGER_TOAST, payload: {status: 'SUCCESS', title: 'Success', message: 'Welcome Anbargal'}},
          {type, payload: response},
          {type: AuthActions.SIGN_IN, payload: true},
          {
            type: AuthActions.TRY_UPDATE_USER_PROFILE,
            payload: response.authData
          }
        );
        if (response.response.additionalUserInfo.isNewUser) {
          actions.push(
            {
              type: AuthActions.CREATE_USER_LIBRARY,
              payload: new UserClass(
                response.response.user.uid,
                false,
                [],
                response.response.user.displayName,
                response.response.user.email,
                response.response.user.phoneNumber,
                null,
                response.response.user.photoURL),
            }
          );
        } else {
          actions.push(
            {
              type: AuthActions.TRY_SYNC_USER,
              payload: response.response.user.uid
            }
          );
        }
      } else {
        actions.push(
          {type, payload: {...response}},
          {type: AuthActions.SIGN_IN, payload: false}
        );
      }
      return actions;
    })
  );

  @Effect()
  getAuthToken = this.actions$.pipe(
    ofType(AuthActions.GET_USER_TOKEN)
    , map((action: AuthActions.GetUserToken) => action.payload)
    , switchMap((response: { status: number, authData: ThirupUserDetails } | any) => {
      return from(
        this.fireBaseService.getUserToken().then(
          token => {
            return {
              authData: {
                ...response.authData,
                authToken: token
              },
              status: 200
            };
          },
          () => {
            return {
              authData: {
                ...response,
                authToken: null
              },
              status: 401
            };
          }).catch(() => {
          return {
            authData: {
              ...response,
              authToken: null
            },
            status: 401
          };
        })
      );
    })
    , mergeMap((response: { status: number, authData: ThirupUserDetails } | any) => {
      const actions = [];
      let type = AuthActions.FAILED_SIGN_UP;
      if (response.status === 200 && response.authData.authToken) {
        type = AuthActions.UPLOAD_USER_IMAGE;
        actions.push({type, payload: response});
      } else {
        actions.push({type, payload: {...response, response: null}});
      }
      return actions;
    })
  );

  @Effect()
  uploadUserImage = this.actions$.pipe(
    ofType(AuthActions.UPLOAD_USER_IMAGE),
    map((action: AuthActions.UploadUserImage) => action.payload),
    switchMap((response: { status: number, authData: ThirupUserDetails } | any) => {
      return from(
        this.fireBaseService.saveUserImage(response.authData.image, response.authData.authToken, response.authData.email)
          .then(() => {
            return {
              authData: {...response.authData},
              status: 200
            };
          }, () => {
            return {
              authData: {...response.authData},
              status: 402
            };
          }).catch(() => {
          return {
            authData: {...response.authData},
            status: 402
          };
        })
      );
    }),
    mergeMap((response: { status: number, authData: ThirupUserDetails } | any) => {
      const actions = [];
      let type = AuthActions.FAILED_SIGN_UP;
      if (response.status === 200) {
        type = AuthActions.GET_IMAGE_URL;
        actions.push({type, payload: response.authData});
      } else {
        actions.push({type, payload: {...response, response: null}});
      }
      return actions;
    })
  );

  @Effect()
  getImageUrl = this.actions$.pipe(
    ofType(AuthActions.GET_IMAGE_URL),
    map((action: AuthActions.GetImageUrl) => action.payload),
    switchMap((response: ThirupUserDetails) => {
      return this.fireBaseService.getImageDownloadUrl(response.email).pipe(
        map((imageURl: any) => {
          return {
            ...response,
            imageUrl: imageURl
          };
        })
      );
    }),
    map((response: ThirupUserDetails) => {
      return response;
    }),
    mergeMap((response: ThirupUserDetails) => {
      const actions = [];
      if (response.imageUrl) {
        actions.push(
          {
            type: AuthActions.TRY_UPDATE_USER_PROFILE,
            payload: response
          });
      } else {
        actions.push({type: AuthActions.FAILED_SIGN_UP, payload: {status: 403, authData: null, response: null}});
      }
      return actions;
    })
  );

  @Effect()
  updateUserProfileToServer = this.actions$.pipe(
    ofType(AuthActions.TRY_UPDATE_USER_PROFILE),
    map((action: AuthActions.TryUpdateUserProfile) => action.payload),
    switchMap((response: ThirupUserDetails) => {
      return from(
        this.fireBaseService.updateProfile(response.firstName, response.lastName, response.imageUrl)
      );
    }),
    mergeMap(() => {
      return [];
    })
  );

  @Effect()
  logout = this.actions$.pipe(
    ofType(AuthActions.TRY_LOG_OUT),
    switchMap(() => {
      return from(
        this.fireBaseService.logOutUser().catch(
          error => console.error(error)
        )
      );
    }),
    mergeMap(() => {
      this.router.navigate(['user-auth/sign-in']).catch(
        error => console.error('Routing Error : ', error)
      );
      return [
        {
          type: AuthActions.LOG_OUT
        },
        {type: ToastActions.TRIGGER_TOAST, payload: {status: 'SUCCESS', title: 'Success', message: 'You are successfully logged out'}},
      ];
    })
  );

  @Effect()
  signIn = this.actions$.pipe(
    ofType(AuthActions.TRY_SIGN_IN),
    map((action: AuthActions.TrySignIn) => action.payload),
    switchMap((response) => {
      return from(
        this.fireBaseService.signInUser(response.email, response.password).then(
          success => {
            this.router.navigate([response.route], {queryParams: {...response.queryParams}}).catch(
              error => console.error('Routing Error : ', error)
            );
            return {
              status: 200,
              response: success
            };
          },
          failed => {
            return {
              status: 405,
              response: failed
            };
          }
        ).catch(
          error => {
            return {
              status: 405,
              response: error
            };
          }
        )
      );
    }),
    mergeMap((response: { status: number, response: any }) => {
      const actions = [];
      if (response.status === 200) {
        actions.push(
          {
            type: ToastActions.TRIGGER_TOAST,
            payload: {status: 'SUCCESS', title: 'Success', message: 'Welcome Anbargal'},
          },
          {
            type: AuthActions.SIGN_IN,
            payload: true
          }
        );
        if (response.response.additionalUserInfo.isNewUser) {
          actions.push(
            {
              type: AuthActions.CREATE_USER_LIBRARY,
              payload: new UserClass(
                response.response.user.uid,
                false,
                [],
                response.response.user.displayName,
                response.response.user.email,
                response.response.user.phoneNumber,
                null,
                response.response.user.photoURL),
            }
          );
        } else {
          actions.push(
            {
              type: AuthActions.TRY_SYNC_USER,
              payload: response.response.user.uid
            }
          );
        }
      } else {
        actions.push(
          {
            type: AuthActions.FAILED_SIGN_UP,
            payload: {...response, authData: null}
          }
        );
      }
      return actions;
    })
  );

  @Effect()
  googleSignIn = this.actions$.pipe(
    ofType(AuthActions.GOOGLE_SIGN_IN),
    map((action: AuthActions.FailedSignUp) => action.payload),
    mergeMap((response: any) => {
      const actions = [];
      if (!response.result) {
        actions.push(
          {
            type: AuthActions.FAILED_SIGN_UP,
            payload: {...response.result, authData: null}
          }
        );
      } else {
        this.router.navigate([response.route], {queryParams: {...response.queryParams}}).catch(
          error => console.error('Routing Error : ', error)
        );
        actions.push({
            type: ToastActions.TRIGGER_TOAST,
            payload: {status: 'SUCCESS', title: 'Success', message: 'Welcome Anbargal'},
          },
          {
            type: AuthActions.SIGN_IN,
            payload: true
          }
        );
      }
      return actions;
    })
  );

  @Effect()
  onFailedSignUp = this.actions$.pipe(
    ofType(AuthActions.FAILED_SIGN_UP)
    , map((action: AuthActions.FailedSignUp) => action.payload)
    , map((response: { response: any, status: number, authData: ThirupUserDetails } | any) => {
      return response;
    }),
    mergeMap((response: { response: any, status: number, authData: ThirupUserDetails }) => {
      let status: number;
      let message = null;
      status = response.status;
      switch (response.status) {
        case 400:
          if (response.response) {
            message = response.response.message;
          }
          break;
        case  401:
          message = ERROR_MESSAGE_1;
          break;
        case 402:
          message = ERROR_MESSAGE_2;
          break;
        case 403:
          message = ERROR_MESSAGE_3;
          break;
        case 405:
          message = response.response.message;
          break;
        default:
          status = 404;
          message = ERROR_MESSAGE_4;
      }
      console.error('FAILED : ', response, ' Error Message : ', message, ' status : ', status);
      return [
        {
          type: ToastActions.TRIGGER_TOAST,
          payload: {status: 'FAILED', message, title: 'Error'}
        }
      ];
    })
  );

  @Effect()
  createUserLibrary = this.actions$.pipe(
    ofType(AuthActions.CREATE_USER_LIBRARY),
    map((action: AuthActions.CreateUserLibrary) => action.payload),
    switchMap((data) => {
      return from(this.fireBaseService.addUserToLibrary(data).then(
        () => {
          return data.uid;
        }, (error) => {
          console.error(error);
          return null;
        }
      ).catch(
        error => {
          console.error(error);
          return null;
        }
      ));
    }),
    mergeMap(response => {
      const actions = [];
      if (response) {
        actions.push({
          type: AuthActions.TRY_SYNC_USER,
          payload: response,
        });
      }
      return actions;
    })
  );

  @Effect()
  tryUserSync = this.actions$.pipe(
    ofType(AuthActions.TRY_SYNC_USER),
    map((action: AuthActions.TrySyncUser) => action.payload),
    switchMap(uid => {
      return this.fireBaseService.getUserFromLibrary(uid);
    }),
    mergeMap((user: UserLibrary) => {
      const actions = [];
      if (user) {
        actions.push({
          type: AuthActions.SYNC_USER,
          payload: user,
        });
      }
      return actions;
    })
  );

  @Effect()
  forgotPassword = this.actions$.pipe(
    ofType(AuthActions.FORGOT_PASSWORD),
    map((action: AuthActions.ForgotPassword) => action.payload),
    switchMap((email: string) => {
      return from(this.fireBaseService.resetPassword(email).then(
        () => 200
      ).catch(() => 405));
    }),
    mergeMap((status: number) => {
      const actions = [];
      if (status === 200) {
        actions.push(
          {
            type: ToastActions.TRIGGER_TOAST,
            payload: {
              status: 'SUCCESS',
              title: 'Success',
              message: `An email has been sent to your inbox,
               to reset your password.
               Please check your email for further instructions`
            },
          }
        );
      } else {
        actions.push(
          {
            type: ToastActions.TRIGGER_TOAST,
            payload: {status: 'FAILED', message: 'An Error has occurred please try again later', title: 'Error'}
          }
        );
      }
      return actions;
    })
  );

  constructor(private actions$: Actions, private fireBaseService: FirebaseService,
              private store: Store<fromAuth.State>,
              private router: Router) {
  }
}
