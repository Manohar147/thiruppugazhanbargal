import * as AuthActions from './auth.actions';
import {User} from 'firebase';
import {UserLibrary} from '../ThirupUser/user';

export interface State {
  user: User;
  userLibrary: UserLibrary
  authTitle: string;
  isLoggedIn: boolean;
  redirectRoute: string;
}

const initialState: State = {
  user: null,
  authTitle: null,
  isLoggedIn: false,
  userLibrary: null,
  redirectRoute: 'Home'
};

export function authReducer(state = initialState, action: AuthActions.AuthActions) {
  switch (action.type) {
    case AuthActions.SET_REDIRECT_ROUTE:
      return {
        ...state,
        redirectRoute: action.payload
      };
    case AuthActions.CHANGE_TITLE:
      return {
        ...state,
        authTitle: action.payload
      };
    case AuthActions.SIGN_IN:
      return {
        ...state,
        isLoggedIn: action.payload
      };
    case  AuthActions.SIGN_UP:
      return {
        ...state,
        isLoggedIn: action.payload
      };
    case  AuthActions.LOG_OUT:
      return {
        ...state,
        isLoggedIn: false
      };
    case AuthActions.MONITOR_USER:
      return {
        ...state,
        user: action.payload
      };
    case AuthActions.UPDATE_USER:
      return {
        ...state,
        user: action.payload
      };
    case AuthActions.SYNC_USER:
      return {
        ...state,
        userLibrary: action.payload
      };
    default:
      return {
        ...state
      };
  }
}
