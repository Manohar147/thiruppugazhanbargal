import {Component, Input, OnDestroy, OnInit} from '@angular/core';
import {Store} from '@ngrx/store';
import * as AuthActions from '../../auth-store/auth.actions';
import * as fromAuth from '../../auth-store/auth.reducer';
import {FormControl, FormGroup, FormGroupDirective, NgForm, Validators} from '@angular/forms';
import * as firebaseUi from 'firebaseui';
import * as firebase from 'firebase/app';
import {AngularFireAuth} from '@angular/fire/auth';
import {ErrorStateMatcher, MatDialog} from '@angular/material';
import {UserClass} from '../../ThirupUser/user';
import {ForgotPasswordDialogComponent} from './forgot-password-dialog/forgot-password-dialog.component';

export class MyErrorStateMatcher implements ErrorStateMatcher {
  isErrorState(control: FormControl | null, form: FormGroupDirective | NgForm | null): boolean {
    const isSubmitted = form && form.submitted;
    return !!(control && control.invalid && (control.dirty || control.touched || isSubmitted));
  }
}

@Component({
  selector: 'app-sign-in',
  templateUrl: './sign-in.component.html',
  styleUrls: ['./sign-in.component.css']
})
export class SignInComponent implements OnInit, OnDestroy {
  signInForm: FormGroup;
  ui: firebaseUi.auth.AuthUI;
  matcher = new MyErrorStateMatcher();
  passIconColor: string = '';
  route: string;

  constructor(private store: Store<fromAuth.State>, private afAuth: AngularFireAuth, public dialog: MatDialog) {
  }

  ngOnInit() {
    this.store.dispatch(new AuthActions.ChangeTitle('Sign In'));

    this.signInForm = new FormGroup({
      email: new FormControl(null, [Validators.required, Validators.email]),
      password: new FormControl(null, [Validators.required])
    });
    const uiConfig = {
      signInOptions: [
        firebase.auth.GoogleAuthProvider.PROVIDER_ID
      ],
      callbacks: {
        signInSuccessWithAuthResult: this.onLoginSuccess.bind(this)
      }
    };
    this.ui = new firebaseUi.auth.AuthUI(this.afAuth.auth);
    this.ui.start('#firebaseui-auth-container', uiConfig);
    this.store.select('auth').subscribe(
      (data: fromAuth.State) => {
        this.route = data.redirectRoute;
      }
    );
  }

  onLoginSuccess(result) {
    if (result.additionalUserInfo.isNewUser) {
      this.store.dispatch(new AuthActions.CreateUserLibrary(new UserClass(result.user.uid, false, [], result.user.displayName, result.user.email, result.user.phoneNumber, null, result.user.photoURL)));
    } else {
      this.store.dispatch(new AuthActions.TrySyncUser(result.user.uid));
    }
    this.store.dispatch(new AuthActions.GoogleSignIn({result: result, route: this.route}));
  }

  onSubmit() {
    if (this.signInForm.valid) {
      this.store.dispatch(new AuthActions.TrySignIn({...this.signInForm.value, route: this.route}));
    }
  }

  ngOnDestroy(): void {
    this.ui.delete().catch(error => console.error('Error in Sign-in-component OnDestroy() Error : ', error));
  }

  togglePassword(password, type) {
    password.setAttribute('type', type);
    if (type === 'text') {
      this.passIconColor = 'primary';
    } else {
      this.passIconColor = '';
    }
  }

  openForgotPasswordDialog(): void {
    const dialogRef = this.dialog.open(ForgotPasswordDialogComponent, {
    });
  }

}
