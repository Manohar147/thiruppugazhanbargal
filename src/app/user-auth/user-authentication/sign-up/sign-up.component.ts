import {Component, OnInit, Renderer2} from '@angular/core';
import {Store} from '@ngrx/store';
import * as fromAuth from '../../auth-store/auth.reducer';
import * as AuthActions from '../../auth-store/auth.actions';
import * as $ from 'jquery';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {ThirupUserDetails} from '../../ThirupUser/userDetails';
import {MatSnackBar} from '@angular/material';


@Component({
  selector: 'app-sign-up',
  templateUrl: './sign-up.component.html',
  styleUrls: ['./sign-up.component.css']
})
export class SignUpComponent implements OnInit {
  public imageSrc: File;

  firstFormGroup: FormGroup;
  secondFormGroup: FormGroup;
  thirdFormGroup: FormGroup;


  constructor(private store: Store<fromAuth.State>, private renderer: Renderer2,
              private _snackBar: MatSnackBar) {
  }

  ngOnInit() {
    this.store.dispatch(new AuthActions.ChangeTitle('Sign Up'));
    this.imageSrc = null;

    this.firstFormGroup = new FormGroup({
      firstName: new FormControl('', [Validators.required]),
      lastName: new FormControl('', [Validators.required]),
    });
    this.secondFormGroup = new FormGroup({
      email: new FormControl('', [Validators.required, Validators.email]),
      password: new FormControl('', [Validators.required])
    });
    this.thirdFormGroup = new FormGroup({
      image: new FormControl('', [Validators.required]),
    });
  }


  onSubmit() {
    if (this.firstFormGroup.valid && this.secondFormGroup.valid && this.thirdFormGroup.valid) {
      if (this.imageSrc.size < 2097152) {
        const profileDetails: ThirupUserDetails = {
          ...this.firstFormGroup.value,
          ...this.secondFormGroup.value,
          image: this.imageSrc
        };
        this.store.dispatch(new AuthActions.TrySignUp(profileDetails));
      } else {
        this._snackBar.open(`Please upload an image with size less than 2MB`, null, {
          duration: 3000
        });
      }
    }
  }


  onAvatarChange(event, input, imagePreview) {
    if (input.files && input.files[0]) {
      this.imageSrc = input.files[0];
      const reader = new FileReader();
      reader.onload = (e: ProgressEvent) => {
        // @ts-ignore
        this.renderer.setStyle(imagePreview, 'background-image', 'url(' + e.currentTarget.result + ')');
        $('#imagePreview').hide().fadeIn(100);
      };
      reader.readAsDataURL(input.files[0]);
    }
  }

}


