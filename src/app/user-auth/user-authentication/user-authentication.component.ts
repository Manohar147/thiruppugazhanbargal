import {Component, OnInit} from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import * as fromAuth from '../auth-store/auth.reducer';
import {Observable} from 'rxjs';
import {Store} from '@ngrx/store';

@Component({
  selector: 'app-user-authentication',
  templateUrl: './user-authentication.component.html',
  styleUrls: ['./user-authentication.component.css']
})
export class UserAuthenticationComponent implements OnInit {
  authState: Observable<{
    authTitle: string,
    isLoggedIn: boolean
  }>;

  constructor(private route: ActivatedRoute, private store: Store<fromAuth.State>) {
  }

  ngOnInit() {
    this.authState = this.store.select('auth');
  }

}
