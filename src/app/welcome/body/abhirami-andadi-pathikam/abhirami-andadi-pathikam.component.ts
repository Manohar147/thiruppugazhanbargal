import {Component, OnInit} from '@angular/core';
import {Store} from '@ngrx/store';
import * as fromWelcome from '../../store/welcome.reducer';
import {ActivatedRoute, Data} from '@angular/router';
import * as welcomeActions from '../../store/welcome.actions';
import {DownloadTable} from '../../table/download-table/download-table.component';

const ABHIRAMI_DATA: DownloadTable[] = [
  {name: `Guruji's Speech`, url: 'https://drive.google.com/file/d/1VLiwoP6rDNG82roIw5s0F69sL5hDSU4C/view?usp=sharing'},
  {name: 'Bhupaalam', url: 'https://drive.google.com/file/d/1cSMkWoBwJP7Mwv-kgRHdXS6_wdl9hqcb/view?usp=sharing'},
  {name: 'Bilahari', url: 'https://drive.google.com/file/d/1lnxq0AYdGQvEEPsSrW4JyGVH9gElSFu4/view?usp=sharing'},
  {name: 'Saaveri_a', url: 'https://drive.google.com/file/d/10xCQucJkPGV-aFWzwuEaqTo66qzG8pjF/view?usp=sharing'},
  {name: 'Saaveri_b', url: 'https://drive.google.com/file/d/1P1sU-IJ1d18DGXXp4TC8kv2tktA2TDmf/view?usp=sharing'},
  {name: 'Hamsaanandi', url: 'https://drive.google.com/file/d/1ShSm9cAZ5OjzBe-z0ndsqND_KX7MIfw3/view?usp=sharing'},
  {name: 'Darbauri Kaanada', url: 'https://drive.google.com/file/d/1Iklnsgk9lAqXAb1ARVjm02lFPTtMq5QO/view?usp=sharing'},
  {name: 'Mohanam', url: 'https://drive.google.com/file/d/1zyr7uMeZy8OpXL_qLwR6KXSWIlSmns2L/view?usp=sharing'},
  {name: 'Panthuvaraali', url: 'https://drive.google.com/file/d/1-phlEzIWNtA9bTkecQnrurYdYp4Dd8fi/view?usp=sharing'},
  {name: 'Kaambojhi', url: 'https://drive.google.com/file/d/1JmsEIzYw-q6eNFAugDAdvfV950kZfOJt/view?usp=sharing'},
  {name: 'Dhanyaasi', url: 'https://drive.google.com/file/d/10tpLnjp3e10i9zB8jOyUoF36mNcq6rkV/view?usp=sharing'},
  {name: 'Naayagi', url: 'https://drive.google.com/file/d/1FuiO5Jcnnwjo5qwxBXjScVXIL96m4hEB/view?usp=sharing'},
  {name: 'Sarangaa', url: 'https://drive.google.com/file/d/1khAy07f797bQ_7hfebst7nh17uoGNjY9/view?usp=sharing'},
  {name: 'Kaapi', url: 'https://drive.google.com/file/d/1NoMx0pnCgja_z-A0tigRuniIUYTGImA7/view?usp=sharing'},
  {name: 'Aahiri', url: 'https://drive.google.com/file/d/1HtNxPHLG8A8z2hCOJ7UnC9GQidb_Mh_k/view?usp=sharing'},
  {name: 'Sahaana', url: 'https://drive.google.com/file/d/1DGw-guQnoHKr4za4Gbz7nypnKC4x9YTE/view?usp=sharing'},
  {name: 'YamunaaKalyaani', url: 'https://drive.google.com/file/d/17Sc2wyv4GzX56dajciHUUoIP5nSklfI1/view?usp=sharing'},
  {name: 'Bhairavi', url: 'https://drive.google.com/file/d/19LdOQePc4iK5JTuKw2eH0ofyz8KkayV8/view?usp=sharing'},
  {name: 'Durgaa', url: 'https://drive.google.com/file/d/135ej5RxnfaFpZUTvU7_FhCCOvDQuliU-/view?usp=sharing'},
  {name: 'Ataanaa', url: 'https://drive.google.com/file/d/1BFFkw1MCRVOIczCkkKA9OL1g2ql5wj4a/view?usp=sharing'},
  {name: 'Suruti', url: 'https://drive.google.com/file/d/1JWyUccjdrJlF9e4rGk7Hpco1V9MguMUm/view?usp=sharing'},
  {name: 'Madhyamaavathi', url: 'https://drive.google.com/file/d/1qJ28pn1bxKDd9k4xj3zBUfz3OsT7zhd2/view?usp=sharing'},
  {name: 'Aathalai', url: 'https://drive.google.com/file/d/1u2N6sjOoCKiV62vrYf5XFYlFSnTgbV0_/view?usp=sharing'},
  {name: 'Pathigam Gurujis Speech', url: 'https://drive.google.com/file/d/1_S9K5tEudf231izVkcjA--8tAtVWsTL5/view?usp=sharing'},
  {name: 'Naattai', url: 'https://drive.google.com/file/d/1ukSOW5wvCly9c4GzEQAKPSbP4V83uKKF/view?usp=sharing'},
  {name: 'Sowraashtram', url: 'https://drive.google.com/file/d/1Zq6ytCn4eb-l8C25PCvlvQms0Dy5YC3s/view?usp=sharing'},
  {name: 'Sowraashtram', url: 'https://drive.google.com/file/d/1bDRW1JNSBVhdVz0I7TcAf5hp6wBKluCK/view?usp=sharing'},
  {name: 'PurviKalyaani', url: 'https://drive.google.com/file/d/1u9QSGfEALDczsReKwXb3prSTBo0GQXz8/view?usp=sharing'},
  {name: 'PurviKalyaani', url: 'https://drive.google.com/file/d/1xXj5Ucr0WMlv1y_gy8yHkD8KBW0n5ThZ/view?usp=sharing'},
  {name: 'Hindolam', url: 'https://drive.google.com/file/d/1tm_x-Ic19KuDnZ6AoITyMoFQG-1DOuhe/view?usp=sharing'},
  {name: 'Hindolam', url: 'https://drive.google.com/file/d/1CiuJpMS_n7jUUb1H9KlKwBwcgp0AH7g2/view?usp=sharing'},
  {name: 'Hindolam', url: 'https://drive.google.com/file/d/1P3mKsStpGHTch7PGfmqsdWwWAzf1fHaI/view?usp=sharing'},
  {name: 'Hindolam', url: 'https://drive.google.com/file/d/1C-392UObl_Y8LN_gC7wQ4OI22Leybx0_/view?usp=sharing'},
  {name: 'NataiKurunji', url: 'https://drive.google.com/file/d/1PLg1t7Uqb0d6NWYZRw9BhjeD1BuT0jAw/view?usp=sharing'},
  {name: 'NataiKurunji', url: 'https://drive.google.com/file/d/1p9Y2m-r2fxraG2-pchyNdTIdQKhTzTje/view?usp=sharing'},
  {name: 'NataiKurunji', url: 'https://drive.google.com/file/d/1weE69QecxlrXFV4g8fEs_LU0fmNT9YLH/view?usp=sharing'},
  {name: 'NataiKurunji', url: 'https://drive.google.com/file/d/1-cgcURUFe2gHNmT2-4m7k93lhF1m79kX/view?usp=sharing'},
  {name: 'Panthuvaraali', url: 'https://drive.google.com/file/d/1x6sJP9y-KtM-joDyMDlIzNnT4K68Z-Pj/view?usp=sharing'},
  {name: 'Panthuvaraali', url: 'https://drive.google.com/file/d/1xYvU6gJdWqY4KXiALC8dNBHd8J8mZZdJ/view?usp=sharing'},
  {name: 'Panthuvaraali', url: 'https://drive.google.com/file/d/126_ntarjud30w9SRBlq_6WeILexpAaGp/view?usp=sharing'},
  {name: 'Panthuvaraali', url: 'https://drive.google.com/file/d/1mVoMhNIUTTwrH_zELPbOXZXXr2iY0MiQ/view?usp=sharing'},
  {name: 'AnandaBhairavi', url: 'https://drive.google.com/file/d/13ki_7dpTk_VRcDM2ocUwnw1Je2tLUk_0/view?usp=sharing'},
  {name: 'AnandaBhairavi', url: 'https://drive.google.com/file/d/1fAi7qpzXGtXrj5ZSrn6nImgRWsKbgXWg/view?usp=sharing'},
  {name: 'AnandaBhairavi', url: 'https://drive.google.com/file/d/1gwzxiXxQFE8IEiAVo2tdHWNoBTAsI2Sw/view?usp=sharing'},
  {name: 'AnandaBhairavi', url: 'https://drive.google.com/file/d/1gwzxiXxQFE8IEiAVo2tdHWNoBTAsI2Sw/view?usp=sharing'},
  {name: 'Ranjani', url: 'https://drive.google.com/file/d/1CuCypSQQn1807kTXuzeHWAl6LZqyGaZI/view?usp=sharing'},
  {name: 'Ranjani', url: 'https://drive.google.com/file/d/1CuCypSQQn1807kTXuzeHWAl6LZqyGaZI/view?usp=sharing'},
  {name: 'Ranjani', url: 'https://drive.google.com/file/d/1KRUo5r7-lwnrktykMQf7Ql9fQKwyQ5Og/view?usp=sharing'},
  {name: 'Ranjani', url: 'https://drive.google.com/file/d/1lJzJHa65IlNTEzP6eDl7t1vS9VJyrvjc/view?usp=sharing'},
  {name: 'SinduBhairavi', url: 'https://drive.google.com/file/d/1IH3nCDIXmrzBiJGt8I-P98tsHSw7LMnL/view?usp=sharing'},
  {name: 'SinduBhairavi', url: 'https://drive.google.com/file/d/1ohFKOEi93M9beRDjFUNiI2iz9fa4VV32/view?usp=sharing'},
  {name: 'SinduBhairavi', url: 'https://drive.google.com/file/d/1Ge50pgxTawTYiBMN-x4HGIqnxJAAPqyM/view?usp=sharing'},
  {name: 'SinduBhairavi', url: 'https://drive.google.com/file/d/1w59em9ieT-L6jzCN2gr-s0nebCBKyRve/view?usp=sharing'},
  {name: 'Thilang', url: 'https://drive.google.com/file/d/1Gsx2meOKqs-f0_lRR68GLt4Dp1kym0O_/view?usp=sharing'},
  {name: 'Thilang', url: 'https://drive.google.com/file/d/1acaPY3NQgyZ-I4lcpiT7oEONuxDD0ALq/view?usp=sharing'},
  {name: 'Thilang', url: 'https://drive.google.com/file/d/1vs5lMrbJc3XqRDr7xA48l9qTzOlcP8B3/view?usp=sharing'},
  {name: 'Thilang', url: 'https://drive.google.com/file/d/1g1oQ-DhDqRurti-epM7rizMKi1Plx6Nd/view?usp=sharing'},
  {name: 'Manolayam', url: 'https://drive.google.com/file/d/1s6a479BMfngqyT0dIh9EKJ_YP0QKv4yO/view?usp=sharing'},
  {name: 'Manolayam', url: 'https://drive.google.com/file/d/1SFGhkGrQU0tgwbBTpu12qJVz-LMbdw8c/view?usp=sharing'},
  {name: 'Manolayam', url: 'https://drive.google.com/file/d/1CsGz0Fp2FESZng5AhgPk5ZncvqMgJw2f/view?usp=sharing'},
  {name: 'Manolayam', url: 'https://drive.google.com/file/d/1hEBXq3KuFN-t0GUUw6CAuAmPOSfwrhae/view?usp=sharing'},
  {name: 'Madhyamaavathi', url: 'https://drive.google.com/file/d/1JBH0EAJJEcDQvoPHZNV8wDtTh9loLiH5/view?usp=sharing'},
  {name: 'Madhyamaavathi', url: 'https://drive.google.com/file/d/1plRmlIIj8OC5EPpMXLuoaKrcOJ-LK-nj/view?usp=sharing'},
  {name: 'Madhyamaavathi', url: 'https://drive.google.com/file/d/1UiEeKpwWU4jUv7EGRBPR1LfOWKJkC00d/view?usp=sharing'},
  {name: 'Madhyamaavathi', url: 'https://drive.google.com/file/d/1fftw_ONa6Uc1tKcj6CWvMi-8JQhf4WYa/view?usp=sharing'},
];

@Component({
  selector: 'app-abhirami-andadi-pathikam',
  templateUrl: './abhirami-andadi-pathikam.component.html',
  styleUrls: ['./abhirami-andadi-pathikam.component.css']
})
export class AbhiramiAndadiPathikamComponent implements OnInit {
  public abhiramiData: DownloadTable[] = ABHIRAMI_DATA;


  constructor(private store: Store<fromWelcome.State>, private route: ActivatedRoute) {
  }

  ngOnInit() {
    this.route.data.subscribe(
      (data: Data) => {
        this.store.dispatch(new welcomeActions.ChangeTitle(data.title));
      }
    );
  }

}
