import {Component, OnInit} from '@angular/core';
import {Store} from '@ngrx/store';

import * as fromWelcome from '../../store/welcome.reducer';
import * as welcomeActions from '../../store/welcome.actions';
import {ActivatedRoute, Data} from '@angular/router';

@Component({
  selector: 'app-about-guruji',
  templateUrl: './about-guruji.component.html',
  styleUrls: ['./about-guruji.component.css']
})
export class AboutGurujiComponent implements OnInit {

  constructor(private store: Store<fromWelcome.State>, private route: ActivatedRoute) {
  }

  ngOnInit() {
    this.route.data.subscribe(
      (data: Data) => {
        this.store.dispatch(new welcomeActions.ChangeTitle(data.title));
      }
    );

  }

}
