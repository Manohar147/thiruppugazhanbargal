import {Component, OnInit} from '@angular/core';
import {DownloadTable} from '../../table/download-table/download-table.component';
import {Store} from '@ngrx/store';
import * as fromWelcome from '../../store/welcome.reducer';
import {ActivatedRoute, Data} from '@angular/router';
import * as welcomeActions from '../../store/welcome.actions';

const ALL_SONGS_WITH_MEANING: DownloadTable[] = [
  {name: 'Kandar Anubhuti', url: 'https://drive.google.com/open?id=1u4sRLsnlgPTUO0JYpZtLaVxYgi20z214'},
  {name: 'Vinayakar and Thiruparan kundram', url: 'https://drive.google.com/open?id=17Razy7iLNGd0MYCMm1MSpCvhGBn98Lky'},
  {name: 'Palani', url: 'https://drive.google.com/open?id=1I-j_76UX7pCusfGmxPI64F2CKMgKvm_u'},
  {name: 'Thiruttani', url: 'https://drive.google.com/open?id=1DEJDbdxL6HDCDx1F7Ey8h2VItJ6rP2_T'},
  {name: 'Swami Malai', url: 'https://drive.google.com/open?id=1tQtKB2ZY5HlG29D1iPrCx8TCvCJOWcD7'},
  {name: 'Tiruchendur', url: 'https://drive.google.com/open?id=17Razy7iLNGd0MYCMm1MSpCvhGBn98Lky'},
  {name: 'Pazhamudirsolai', url: 'javascript:void(0)'},
  {name: 'Panchabhoota sthalam', url: 'https://drive.google.com/open?id=1TFyb4tl5-f9aInfwk8Aiq6DAR6GV9kPd'},
  {name: 'Kundru Thoradal', url: 'https://drive.google.com/open?id=1QDFZYS1PE_RzGfip3vmVUeJEI6gx8ybF'},
  {name: 'Enaya Sthalam', url: 'https://drive.google.com/open?id=1t4vLTV-r9DpGqD-_unIG_wMzRXiRgF8f'},
  {name: 'Vaguppugal', url: 'https://drive.google.com/open?id=1CkE7l1MUVAbcICExy3GVV39EjM9OhRNG'},
  {name: 'Velmayil Virutham', url: 'https://drive.google.com/open?id=1wyCOjTNK_-cKnaTjUzaMEQoh9EZFUT0A'},
  {name: 'Deviyin Sthuthi', url: 'https://drive.google.com/open?id=1T7TEvRFNpO6ZDc_fA6maq2CsbF9fgH7o'},
  {name: 'Abhirami Andadi', url: 'https://drive.google.com/open?id=12FFxFLFVcB4jmwRtPGYX6k-gWqZ_Rumb'}
];

@Component({
  selector: 'app-all-songs-with-meaning',
  templateUrl: './all-songs-with-meaning.component.html',
  styleUrls: ['./all-songs-with-meaning.component.css']
})
export class AllSongsWithMeaningComponent implements OnInit {
  allSongsWithMeaning: DownloadTable[] = ALL_SONGS_WITH_MEANING;

  constructor(private store: Store<fromWelcome.State>, private route: ActivatedRoute) {
  }

  ngOnInit() {
    this.route.data.subscribe(
      (data: Data) => {
        this.store.dispatch(new welcomeActions.ChangeTitle(data.title));
      }
    );
  }

}
