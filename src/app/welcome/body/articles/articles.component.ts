import {Component, OnInit} from '@angular/core';
import {Store} from '@ngrx/store';
import * as fromWelcome from '../../store/welcome.reducer';
import {ActivatedRoute, Data} from '@angular/router';
import * as welcomeActions from '../../store/welcome.actions';
import {animate, state, style, transition, trigger} from '@angular/animations';


export interface Article {
  name: string;
  imageUrl: string[];
}

const ARTICLES: Article[] = [
  {
    name: `Felicitation's to Guruji`,
    imageUrl: ['../../../../assets/articles/Felicitation-1-min.jpg']
  },
  {
    name: `Pada Thamarai by Guruji`,
    imageUrl: [
      '../../../../assets/articles/Felicitation-2-min.jpg',
      '../../../../assets/articles/Felicitation-3-min.jpg',
      '../../../../assets/articles/Felicitation-4-min.jpg',
      '../../../../assets/articles/Felicitation-5-min.jpg'
    ]
  },
  {
    name: `Arunagirinathar Arutparvaiyil Aatruppadai Thalangal by Smt Chitra Murthy`,
    imageUrl: [
      '../../../../assets/articles/Felicitation-7-min.jpg',
      '../../../../assets/articles/Felicitation-8-min.jpg',
      '../../../../assets/articles/Felicitation-9-min.jpg',
      '../../../../assets/articles/Felicitation-10-min.jpg',
      '../../../../assets/articles/Felicitation-11-min.jpg',
      '../../../../assets/articles/Felicitation-12-min.jpg',
      '../../../../assets/articles/Felicitation-13-min.jpg'
    ]
  },
  {
    name: `Thiruppugazhai Vaibhavam Aakkiya by Guruji`,
    imageUrl: [
      '../../../../assets/articles/Felicitation-14-min.jpg',
      '../../../../assets/articles/Felicitation-15-min.jpg',
      '../../../../assets/articles/Felicitation-16-min.jpg',
    ]
  },
  {
    name: `Cultivating Indianess by Hindu Paper`,
    imageUrl: [
      '../../../../assets/articles/Felicitation-17-min.jpg',
    ]
  },
  {
    name: `ArunagiriNatharum Arputha Vakkum by Uma Bala Subramanian`,
    imageUrl: [
      '../../../../assets/articles/Felicitation-18-min.jpg',
      '../../../../assets/articles/Felicitation-19-min.jpg',
      '../../../../assets/articles/Felicitation-20-min.jpg',
      '../../../../assets/articles/Felicitation-21-min.jpg',
      '../../../../assets/articles/Felicitation-22-min.jpg',
      '../../../../assets/articles/Felicitation-23-min.jpg',
      '../../../../assets/articles/Felicitation-24-min.jpg',
    ]
  },
  {
    name: `About Guruji Mami by Smt Padma Subramanian`,
    imageUrl: [
      '../../../../assets/articles/Felicitation-25-min.jpg',
      '../../../../assets/articles/Felicitation-26-min.jpg',
    ]
  },
  {
    name: `Grace by Sri Vs Krishnan`,
    imageUrl: [
      '../../../../assets/articles/Felicitation-27-min.jpg',
    ]
  },
];

@Component({
  selector: 'app-articles',
  templateUrl: './articles.component.html',
  styleUrls: ['./articles.component.css'],
  animations: [
    trigger('detailExpand', [
      state('collapsed', style({height: '0px', minHeight: '0'})),
      state('expanded', style({height: '*'})),
      transition('expanded <=> collapsed', animate('225ms cubic-bezier(0.4, 0.0, 0.2, 1)')),
    ]),
  ],
})
export class ArticlesComponent implements OnInit {
  dataSource = ARTICLES;
  columnsToDisplay = ['Article'];
  expandedElement: Article | null;

  constructor(private store: Store<fromWelcome.State>, private route: ActivatedRoute) {
  }

  ngOnInit() {
    this.route.data.subscribe(
      (data: Data) => {
        this.store.dispatch(new welcomeActions.ChangeTitle(data.title));
      }
    );
  }
}
