import {Component, OnInit} from '@angular/core';
import {Store} from '@ngrx/store';
import * as fromWelcome from '../../store/welcome.reducer';
import {ActivatedRoute, Data} from '@angular/router';
import * as welcomeActions from '../../store/welcome.actions';
import {DownloadTable} from '../../table/download-table/download-table.component';

const BHAJANS: DownloadTable[] = [
  {name: '1988 Bangalore Vaibavam', url: 'https://drive.google.com/file/d/1AHdqfBnMI-cS3oeDs4VaEMXp-DwKiQRm/view?usp=sharing'},
  {name: '1989 Thai poosam Chennai', url: 'https://drive.google.com/file/d/1dX7QWZX4YQ0Z43O_wBbg1X2VpAonwwuz/view?usp=sharing'},
];

@Component({
  selector: 'app-bhajans',
  templateUrl: './bhajans.component.html',
  styleUrls: ['./bhajans.component.css']
})
export class BhajansComponent implements OnInit {

  bhajans: DownloadTable[] = BHAJANS;

  constructor(private store: Store<fromWelcome.State>, private route: ActivatedRoute) {
  }

  ngOnInit() {
    this.route.data.subscribe(
      (data: Data) => {
        this.store.dispatch(new welcomeActions.ChangeTitle(data.title));
      }
    );
  }

}
