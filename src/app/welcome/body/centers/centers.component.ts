import {Component, OnInit} from '@angular/core';
import {Store} from '@ngrx/store';
import * as fromWelcome from '../../store/welcome.reducer';
import {ActivatedRoute, Data} from '@angular/router';
import * as welcomeActions from '../../store/welcome.actions';
import {Observable} from 'rxjs';


export interface TwoTab {
  tab1: string;
  tab2: string;
}

export interface ThreeTab {
  tab1: string;
  tab2: string;
  tab3: string;
}



export interface Chennai {
  sno: number;
  event: string;
  venue: string;
  time: string;
}

const Delhi: TwoTab[] = [
  {tab1: 'Padi Vizha', tab2: 'Malai mandir'},
  {tab1: 'Arunagirinathar Vizha', tab2: 'Malai mandir'},
  {tab1: 'Thai Poosam', tab2: 'Karunya Mahaganapathy Mandir at Mayur Vihar II,'},
  {tab1: 'Sri Ramanavami Bhajan', tab2: 'Mayur Vihar I, South Indian Association'},
  {tab1: 'Tamil New Year Bhajan', tab2: 'Dwaraka'},
  {tab1: 'Maha Skanda Shasti', tab2: 'Subha Siddhi Vinayagar Mandir, Mayur Vihar I,'},
  {tab1: 'Ayyappa Mandala Pooja Bhajan', tab2: 'Aishwarya Mahaganapathi Temple (Lawrence Road)'},
  {tab1: 'Thiru Karthigai Bhajan', tab2: 'Ganeshji Mandir, Irwin Road, Connaught Place'},
  {tab1: '25th December', tab2: 'Devi Kamakshi Temple'},
  {tab1: 'Navarathri Bhajan', tab2: 'Sri Sharadambal Koil, VasantVihar'},
  {tab1: 'Guruvandanam', tab2: 'Bhajan at Malai Mandir, R.K.Puram'},
  {tab1: 'Skanda Sashti Bhajan', tab2: 'Malai Mandir, R.K.Puram'},
];

const Chennai: Chennai[] = [
  {sno: 1, event: 'Krithikai', venue: 'Kapaliswarar temple, Mylapore', time: 'Every Month'},
  {sno: 2, event: 'Shashti', venue: 'Ramana Kendra, Mylapore', time: 'Every Month'},
  {sno: 3, event: 'Visakham', venue: 'Marundeeswarar temple, Thiruvanmiyur and Ramana Kendra, Mylapore', time: 'Every Month'},
  {sno: 4, event: 'Padi Vizha', venue: 'Kunrathur temple', time: 'Second Sunday of January'},
  {sno: 5, event: 'Vaikuntha Ekadasi', venue: 'Ananthapadmanabhaswamy temple, Adyar', time: 'December-January'},
  {sno: 6, event: 'Thaippoosam', venue: 'Rathnagiriswarar temple, Besant Nagar.', time: 'January-February'},
  {sno: 7, event: 'Thai Amavasya– Abhirami Anthadi and Pathigam', venue: 'Madhya Kailash Vinayaka Temple, Adyar', time: 'January-February'},
  {
    sno: 8,
    event: 'Thai-velli – Abhirami Anthadi and Pathigam',
    venue: 'Ramana Kendra, Mylapore',
    time: 'One of the Fridays of Thai (January-February)'
  },
  {sno: 9, event: 'Panguni Uthiram', venue: 'Ganesha temple, Srinagar, Saidapet', time: 'March-April'},
  {sno: 10, event: 'Tamil New Year’s Day', venue: 'Sringeri Mandiram hall, Raja Annamalaipuram.', time: 'April 13 or 14'},
  {sno: 11, event: 'Guruji’s Remembrance Day', venue: '(Location may vary)', time: 'May'},
  {sno: 12, event: 'Vaikasi Visakham', venue: 'Sachidananda Ashram Hall, Velachery', time: 'May-June'},
  {sno: 13, event: 'Aani Moolam', venue: 'Vadapalani Murugan temple', time: 'June-July'},
  {
    sno: 14,
    event: 'Adi-velli - Abhirami Anthadi and Pathigam',
    venue: 'Kamakshi Amman temple,Raja Annamalaipuram',
    time: 'One of the Fridays of Adi (July-August)'
  },
  {sno: 15, event: 'Arunagirinathar Day', venue: 'Sringeri Mandiram hall , Raja Annamalaipuram.', time: 'August 15'},
  {sno: 16, event: 'Ganesha Chaturthi', venue: 'Ramana Kendra, Mylapore', time: 'August-September\n'},
  {sno: 17, event: 'Guru Vandanam', venue: 'Rathnagiriswarar temple, Besant Nagar.', time: 'September'},
  {sno: 18, event: 'Skanda Shashti – Days 1-5', venue: 'Various locations', time: 'October-November'},
  {sno: 19, event: 'Maha Skanda Shashti', venue: 'Sringeri Mandiram hall, Raja Annamalaipuram', time: 'October-November'},
];

const Bombay: TwoTab[] = [
  {tab1: 'TIV', tab2: 'Vaikasi Visakam'},
  {tab1: 'TIV', tab2: 'Aadi Moolam'},
  {tab1: 'TIV', tab2: 'Adi Kruthigai Aadi Friday'},
  {tab1: 'TIV', tab2: 'Abhirami Andadi and Padhigam'},
  {tab1: 'TIV', tab2: 'Arunagirinathar Vizha on aug 15th'},
  {tab1: 'TIV', tab2: 'Padi Vizha 26 Jan at Chembur'},
  {tab1: 'TIV', tab2: 'Skanda Sashti, Thai Poosam'},
  {tab1: 'TIV', tab2: 'Mumbai and Pune anbargal – Combined Padi Vizha at Pune on 25 Dec'},
];

const Banglore: TwoTab[] = [
  {tab1: 'TIV', tab2: 'Sashti, Krithigai, Moolam and Visakam'},
  {tab1: 'TIV', tab2: 'Skanda Sashti'},
  {tab1: 'TIV', tab2: 'Makhara Sankranthi'},
  {tab1: 'TIV', tab2: 'Vaikasi Visakam'},
  {tab1: 'TIV', tab2: 'Aani Moolam'},
  {tab1: 'TIV', tab2: 'Aadi Krithigai'},
  {tab1: 'TIV', tab2: 'Vinayaka Chathurthi'},
  {tab1: 'TIV', tab2: 'Thirukarthigai'},
  {tab1: 'TIV', tab2: 'Karthigai Moolam'},
  {tab1: 'TIV', tab2: 'Thai Poosam'},
  {tab1: 'TIV', tab2: 'Panguni Uthiram'},
  {tab1: 'TIV', tab2: 'Guru Vandanam'},
  {tab1: 'TIV', tab2: 'Sri Ramanavami'},
  {tab1: 'Abirami Andadhi and Abirami Padigam', tab2: 'Fridays during the months of Aadi and Thai'},
  {tab1: 'Thirppugazh Paamaalai', tab2: '15th August'},
  {tab1: 'Arunagiri Anjali', tab2: '2nd October'},
  {tab1: 'Padivizha', tab2: '7th January'},
];

const HyderabadT1: ThreeTab[] = [
  {tab1: 'Sashti', tab2: 'Subramanya swami temple', tab3: 'Marredpally'},
  {tab1: 'Krithikai', tab2: 'Ganesh temple', tab3: 'Marredpally'},
  {tab1: '1st Sunday', tab2: 'Skandagiri murugan temple', tab3: 'Padmarao nagar'},
  {tab1: 'Gurusamarpanam', tab2: 'Hanuman temple', tab3: 'Padmarao nagar'},
  {tab1: 'Visakam', tab2: 'temple', tab3: 'Malkajgiri'},
];

const HyderabadT2: ThreeTab[] = [
  {tab1: 'Padi Vizha', tab2: 'Skandagiri temple', tab3: 'Padmarao nagar'},
  {tab1: 'Arunagirinathar Vizha', tab2: 'Skandagiri temple', tab3: 'Padmarao nagar'},
  {tab1: 'Vaikasi Visakam', tab2: 'Veda Bhavan', tab3: 'Malkajgiri'},
];

const HyderabadT3: ThreeTab[] = [
  {tab1: 'First Jan', tab2: 'Veda Bhavan', tab3: 'Malkajgiri'},
  {tab1: 'Tamil New Year', tab2: 'veda Bhavan', tab3: 'Malkajgiri'},
  {tab1: 'Guruvandanam', tab2: 'Hanuman temple', tab3: 'Padmarao nagar'},
  {tab1: 'Vijayadashami', tab2: 'Residence', tab3: '-'},
  {tab1: 'Skanda Sashti, all 6 days', tab2: 'Various temples', tab3: '-'},
  {tab1: 'Thai poosam', tab2: '-', tab3: '-'},
  {tab1: 'Panguni Uthiram', tab2: '-', tab3: '-'},
];

const HyderabadT4: ThreeTab[] = [
  {tab1: 'All Fridays', tab2: 'Thai month', tab3: 'various temples'},
  {tab1: 'All Fridays', tab2: 'Adi month', tab3: 'various temples'},
  {tab1: 'Navratri all days', tab2: '-', tab3: 'residences'},
];

const Pune: TwoTab[] = [
  {tab1: 'TIV', tab2: 'Padi vizha'},
  {tab1: 'TIV', tab2: 'Venkataraman memory day'},
  {tab1: 'TIV', tab2: 'Sivaratri'},
  {tab1: 'TIV', tab2: 'Arunigirinathar vizha'},
  {tab1: 'TIV', tab2: 'Gurujis birthday'},
  {tab1: 'TIV', tab2: 'Skanda Sashti tiv'},
  {tab1: 'TIV', tab2: 'Pazhaiya adiyavar dinam'},
  {tab1: 'TIV', tab2: 'Thai poosam'},
  {tab1: 'TIV', tab2: 'panguniuthiram'},
  {tab1: 'TIV', tab2: 'Vaikasi visakam'},
  {tab1: 'TIV', tab2: 'Aadi kruthigai'},
  {tab1: 'TIV', tab2: 'Every kruthigai'},
];


const Coimbatore: TwoTab[] = [
  {tab1: 'TIV', tab2: 'Vaikasi visakam'},
  {tab1: 'TIV', tab2: 'Panguni Uthiram'},
  {tab1: 'TIV', tab2: 'Thai Poosam'},
  {tab1: 'TIV', tab2: 'Skanda Sashti'},
  {tab1: 'TIV', tab2: 'Valli Kalyanam'},
];

const Thiruvananthapuram: ThreeTab[] = [
  {tab1: 'Classes', tab2: 'Vinayaka Nagar', tab3: 'Sunday'},
  {tab1: '', tab2: 'Thaliyal', tab3: 'Monday'},
  {tab1: '', tab2: 'Karamana', tab3: 'Saturday'},
  {tab1: 'TIV', tab2: 'Mangala Vinayaka Temple', tab3: 'Saturday'},
  {tab1: 'TIV', tab2: 'Advaita Hall', tab3: 'Dec 25'},
  {tab1: 'TIV', tab2: 'Various temples', tab3: 'Thai poosam'},
  {tab1: '', tab2: '', tab3: 'Shiva ratri'},
  {tab1: '', tab2: '', tab3: 'Vaikasi visakam'},
  {tab1: '', tab2: '', tab3: 'Skanda sashti'},
  {tab1: '', tab2: '', tab3: 'Mandala pooja samapthi'},
  {tab1: 'TIV', tab2: 'Temple', tab3: 'Monthly sashti'},
];

const Baroda: ThreeTab[] = [
  {tab1: 'Classes', tab2: 'Subhanpura', tab3: 'Thursdays'},
  {tab1: 'Sashti tiv', tab2: 'Residence', tab3: 'Monthly'},
  {tab1: 'Krithikai tiv', tab2: 'Station jayambe temple', tab3: 'Monthly'},
  {tab1: 'Visakam tiv', tab2: 'Thapovan temple', tab3: 'Monthly'},
  {tab1: 'Abhirami padikam', tab2: 'Sama temple', tab3: 'Adi pooram'},
  {tab1: 'Abhirami andadi', tab2: 'Jayambe temple', tab3: 'Navratri amavasya'},
  {tab1: 'TIV', tab2: 'Sama temple', tab3: 'skanda sashti'},
  {tab1: 'Bhagavatham songs', tab2: 'Koch Guruvayur temple', tab3: 'Gokulashtami'},
  {tab1: 'Thiruppugazh', tab2: 'Temples', tab3: 'Panguni uthram'},
  {tab1: '', tab2: '', tab3: 'Thai poosam'},
  {tab1: 'Valli kalyanam', tab2: 'Thapovanam', tab3: 'Annually'},
];

const Kolkata: ThreeTab[] = [
  {tab1: 'TIV', tab2: 'Visakham and Krithigai', tab3: 'Sri Ganesh Murugan Temple'},
  {tab1: 'TIV', tab2: 'Sashti', tab3: 'Veda Bhavan (Kanchi Mutt)'},
  {tab1: 'TIV', tab2: 'Saturday of every month', tab3: 'Anjaneya Temple'},
  {tab1: 'TIV', tab2: 'Special bhajans', tab3: 'Skanda Sashti (7 days)'},
  {tab1: 'TIV', tab2: 'Thai Poosam, Aani Moolam', tab3: 'Veda Bhavan'},
];

const Toronto: TwoTab[] = [
  {tab1: 'TIV', tab2: 'Sashti, Vaikasi Visakam'},
  {tab1: 'TIV', tab2: 'Skanda Sashti on day 6'},
  {tab1: 'Abhirami Andadi,Padikam', tab2: 'During Navaratri'},
  {tab1: '', tab2: 'Adi velli'},
  {tab1: 'Valli Kalyanam\t', tab2: 'Sringeri mutt, Adi parasakthi temple'},
];

export interface Contact {
  name: string;
  phone: string;
}

const delhiC: Contact[] = [
  {name: 'Sri S.Ganesh', phone: '9810413265'},
  {name: 'Smt Girija ', phone: '9953727467'},
];


const chennaiC: Contact[] = [
  {name: 'Sri Muthuswamy', phone: '9444140101'},
  {name: 'Sri S. Arunachalam', phone: '94449537335'},
  {name: 'Smt Alamelu Santhanam', phone: '9962905136'},
];

const bombayC: Contact[] = [
  {name: 'Smt Raja Lakshmi Balasubramanian', phone: '9967664594'},
  {name: 'Sri A.S. Mani', phone: '9820993406'},
];

const bangaloreC: Contact[] = [
  {name: 'Sri R.Nagesh', phone: '9448089383'},
];

const hyderabadC: Contact[] = [
  {name: 'Smt Lakshmi Venkatraman', phone: '9395528128'},
  {name: 'Dr Girija Krishnan', phone: '9848812301'}
];

const puneC: Contact[] = [
  {name: 'Smt Dharini Ganesh', phone: '8793685864'},
];


const coimbatoreC: Contact[] = [
  {name: 'Sri Vaidyanathan', phone: '9994158712'},
  {name: 'Sri VS Krishnan', phone: '6381606744'}
];

const thiruvananthapuramC: Contact[] = [
  {name: 'Sri Balu Iyer', phone: '9447092866'},
];

const barodaC: Contact[] = [
  {name: 'Mrs Rama', phone: '9727766292'},
];

const kolkataC: Contact[] = [
  {name: 'Mr Viji kumar', phone: '9874753777'},
];

const torontoC: Contact[] = [
  {name: 'Smt Shanta Krishnamurthy', phone: '001 416 888 9169'},
];

const newJerseyC: Contact[] = [
  {name: 'Smt Subha Srinivasan', phone: '+1 609 902 4091'},
];

const chicagoC: Contact[] = [
  {name: 'Mrs Purna Sethuraman', phone: '+1 630 806 6500'},
];

const bostonC: Contact[] = [
  {name: 'Mala Ramakrishnan', phone: '-'},
];

const dubaiC: Contact[] = [
  {name: 'Sri Srirama', phone: '00971 501505631'},
  {name: 'Mrs Vidya Ram', phone: '00971 505518246'},
];

const singaporeC: Contact[] = [
  {name: 'Mrs Janaki Vishwanath', phone: '0065 967 099855'},
];

@Component({
  selector: 'app-centers',
  templateUrl: './centers.component.html',
  styleUrls: ['./centers.component.css']
})
export class CentersComponent implements OnInit {


  delhi: TwoTab[] = Delhi;
  bombay: TwoTab[] = Bombay;
  hyd1: ThreeTab[] = HyderabadT1;
  hyd2: ThreeTab[] = HyderabadT2;
  hyd3: ThreeTab[] = HyderabadT3;
  hyd4: ThreeTab[] = HyderabadT4;
  pune: TwoTab[] = Pune;
  coimbatore: TwoTab[] = Coimbatore;
  thiruvananthapuram: ThreeTab[] = Thiruvananthapuram;
  baroda: ThreeTab[] = Baroda;
  kolkata: ThreeTab[] = Kolkata;
  toronto: TwoTab[] = Toronto;
  bangolore: TwoTab[] = Banglore;
  chennai: Chennai[] = Chennai;
  delhiC = delhiC;
  chennaiC = chennaiC;
  bombayC = bombayC;
  hydC = hyderabadC;
  puneC = puneC;
  coimbatoreC = coimbatoreC;
  thiruC = thiruvananthapuramC;
  barodaC = barodaC;
  kolkattaC = kolkataC;
  torontoC = torontoC;
  bangloreC = bangaloreC;
  newJerseyC = newJerseyC;
  chicagoC = chicagoC;
  bostonC = bostonC;
  dubaiC = dubaiC;
  singaporeC = singaporeC;

  welcomeState: Observable<fromWelcome.State>;

  step = 0;

  setStep(index: number) {
    this.step = index;
  }

  nextStep() {
    this.step++;
  }

  prevStep() {
    this.step--;
  }

  constructor(private store: Store<fromWelcome.State>, private route: ActivatedRoute) {
  }

  ngOnInit() {
    this.route.data.subscribe(
      (data: Data) => {
        this.store.dispatch(new welcomeActions.ChangeTitle(data.title));
      }
    );
    this.welcomeState = this.store.select('welcome');
  }

}


