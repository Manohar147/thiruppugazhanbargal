import {Component, OnInit} from '@angular/core';
import {MatTableDataSource} from '@angular/material';
import {phoneNumber} from '../../../data/phoneNumber';
import {Store} from '@ngrx/store';
import * as fromWelcome from '../../../store/welcome.reducer';
import {takeWhile} from 'rxjs/operators';

@Component({
  selector: 'app-phone-number-table',
  templateUrl: './phone-number-table.component.html',
  styleUrls: ['./phone-number-table.component.css']
})
export class PhoneNumberTableComponent implements OnInit {

  displayedColumns: string[] = ['name', 'place', 'phone'];

  dataSource: MatTableDataSource<phoneNumber>;

  constructor(private store: Store<fromWelcome.State>) {
  }

  ngOnInit() {
    this.store.select('welcome').subscribe(
      (data: fromWelcome.State) => {
        this.dataSource = new MatTableDataSource(data.phoneNumber);
      }
    );


  }

  applyFilter(filterValue: string) {
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }

}
