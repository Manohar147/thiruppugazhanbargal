import {Component, Input, OnInit} from '@angular/core';
import {ThreeTab} from '../../centers.component';
import {MatTableDataSource} from '@angular/material';

@Component({
  selector: 'app-chennai-table',
  templateUrl: './chennai-table.component.html',
  styleUrls: ['./chennai-table.component.css']
})
export class ChennaiTableComponent implements OnInit {

  displayedColumns: string[] = ['sno', 'event', 'venue', 'time'];

  @Input() table: ThreeTab[];

  dataSource: MatTableDataSource<ThreeTab>;

  constructor() {
  }

  ngOnInit() {
    this.dataSource = new MatTableDataSource(this.table);
  }


}
