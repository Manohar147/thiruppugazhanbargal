import {Component, Input, OnInit} from '@angular/core';
import {ThreeTab} from '../../centers.component';
import {MatTableDataSource} from '@angular/material';

@Component({
  selector: 'app-three-table',
  templateUrl: './three-table.component.html',
  styleUrls: ['./three-table.component.css']
})
export class ThreeTableComponent implements OnInit {

  displayedColumns: string[] = ['tab1', 'tab2', 'tab3'];

  @Input() table: ThreeTab[];

  dataSource: MatTableDataSource<ThreeTab>;

  constructor() {
  }

  ngOnInit() {
    this.dataSource = new MatTableDataSource(this.table);
  }

}
