import {Component, Input, OnInit} from '@angular/core';
import {TwoTab} from '../../centers.component';
import {MatTableDataSource} from '@angular/material';

@Component({
  selector: 'app-two-table',
  templateUrl: './two-table.component.html',
  styleUrls: ['./two-table.component.css']
})
export class TwoTableComponent implements OnInit {

  displayedColumns: string[] = ['tab1', 'tab2'];

  @Input() table: TwoTab[];

  dataSource: MatTableDataSource<TwoTab>;

  constructor() {
  }

  ngOnInit() {
    this.dataSource = new MatTableDataSource(this.table);
  }

}
