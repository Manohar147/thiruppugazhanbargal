import {Component, OnInit} from '@angular/core';
import {Store} from '@ngrx/store';
import * as fromWelcome from '../../store/welcome.reducer';
import {ActivatedRoute, Data} from '@angular/router';
import * as welcomeActions from '../../store/welcome.actions';
import {DownloadTable} from '../../table/download-table/download-table.component';


const LIST_OF_SONGS: DownloadTable[] = [
  {
    name: 'krithikai-sashti-1',
    url: 'https://firebasestorage.googleapis.com/v0/b/thiruppugazhanbargal-515e8.appspot.com/o/listOfSongs%2Fkrithikai-sashti%20list%201.xlsx?alt=media&token=6f2012f8-7a2f-4d49-b4bc-cb643b6e41ee'
  },
  {
    name: 'krithikai-sashti-1a',
    url: 'https://firebasestorage.googleapis.com/v0/b/thiruppugazhanbargal-515e8.appspot.com/o/listOfSongs%2Fkrithikai-sashti%20list%201a.xlsx?alt=media&token=fa986072-e9e6-4265-aabf-69659aeef7fb'
  },
  {
    name: 'krithikai-sashti-2',
    url: 'https://firebasestorage.googleapis.com/v0/b/thiruppugazhanbargal-515e8.appspot.com/o/listOfSongs%2Fkrithikai-sashti%20list%202.xlsx?alt=media&token=b5a99566-e248-46f3-ad3c-5b8ce07ef9a7'
  },
  {
    name: 'krithikai-sashti-2a',
    url: 'https://firebasestorage.googleapis.com/v0/b/thiruppugazhanbargal-515e8.appspot.com/o/listOfSongs%2Fkrithikai-sashti%20list%202a.xlsx?alt=media&token=ed126ceb-69ec-48cd-a7a2-fae1da380c03'
  },
  {
    name: 'krithikai-sashti-3',
    url: 'https://firebasestorage.googleapis.com/v0/b/thiruppugazhanbargal-515e8.appspot.com/o/listOfSongs%2Fkrithikai-sashti%20list%203.xlsx?alt=media&token=ded54849-48ea-4555-a060-da06d821e627'
  },
  {
    name: 'krithikai-sashti-3a',
    url: 'https://firebasestorage.googleapis.com/v0/b/thiruppugazhanbargal-515e8.appspot.com/o/listOfSongs%2Fkrithikai-sashti%20list%203a.xlsx?alt=media&token=8af94c14-9524-4931-bfc0-ce680e11efdd'
  },
  {
    name: 'krithikai-sashti-4',
    url: 'https://firebasestorage.googleapis.com/v0/b/thiruppugazhanbargal-515e8.appspot.com/o/listOfSongs%2Fkrithikai-sashti%20list%204.xlsx?alt=media&token=c558788d-1398-47d0-8a34-2875fc006c64'
  },
  {
    name: 'krithikai-sashti-4a',
    url: 'https://firebasestorage.googleapis.com/v0/b/thiruppugazhanbargal-515e8.appspot.com/o/listOfSongs%2Fkrithikai-sashti%20list%204a.xlsx?alt=media&token=43955fc6-d024-4448-91ae-876c15bd1177'
  },
  {
    name: 'krithikai-sashti-5',
    url: 'https://firebasestorage.googleapis.com/v0/b/thiruppugazhanbargal-515e8.appspot.com/o/listOfSongs%2Fkrithikai-sashti%20list%205.xlsx?alt=media&token=d8886741-ff0b-4e6a-b514-4835c1a160a6'
  },
  {
    name: 'krithikai-sashti-5a',
    url: 'https://firebasestorage.googleapis.com/v0/b/thiruppugazhanbargal-515e8.appspot.com/o/listOfSongs%2Fkrithikai-sashti%20list%205a.xlsx?alt=media&token=434416c0-9a4b-4739-83db-50539f642b92'
  },
  {
    name: 'krithikai-sashti-6',
    url: 'https://firebasestorage.googleapis.com/v0/b/thiruppugazhanbargal-515e8.appspot.com/o/listOfSongs%2Fkrithikai-sashti%20list%206.xlsx?alt=media&token=d9570e11-0400-4b13-929d-b8622d890cd1'
  },
  {
    name: 'krithikai-sashti-6a',
    url: 'https://firebasestorage.googleapis.com/v0/b/thiruppugazhanbargal-515e8.appspot.com/o/listOfSongs%2Fkrithikai-sashti%20list%206a.xlsx?alt=media&token=5458781f-7eb2-40e2-8ca1-fbcbd392760e'
  },
  {
    name: 'krithikai-sashti-7',
    url: 'https://firebasestorage.googleapis.com/v0/b/thiruppugazhanbargal-515e8.appspot.com/o/listOfSongs%2Fkrithikai-sashti%20list%207.xlsx?alt=media&token=d0e5ab01-ea7a-4e5b-8132-2c804a8a5972'
  },
  {
    name: 'krithikai-sashti-7a',
    url: 'https://firebasestorage.googleapis.com/v0/b/thiruppugazhanbargal-515e8.appspot.com/o/listOfSongs%2Fkrithikai-sashti%20list%207a.xlsx?alt=media&token=487e69aa-4efd-4e6e-8620-f9752d633a55'
  },
  {
    name: 'krithikai-sashti-8',
    url: 'https://firebasestorage.googleapis.com/v0/b/thiruppugazhanbargal-515e8.appspot.com/o/listOfSongs%2Fkrithikai-sashti%20list%208.xlsx?alt=media&token=eff2b3d0-098e-4266-b30e-010fd107cfba'
  },
  {
    name: 'krithikai-sashti-8a',
    url: 'https://firebasestorage.googleapis.com/v0/b/thiruppugazhanbargal-515e8.appspot.com/o/listOfSongs%2Fkrithikai-sashti%20list%208a.xlsx?alt=media&token=5723c5ec-697e-4c9f-bb49-b14951fe8530'
  },
  {
    name: 'krithikai-sashti-9',
    url: 'https://firebasestorage.googleapis.com/v0/b/thiruppugazhanbargal-515e8.appspot.com/o/listOfSongs%2Fkrithikai-sashti%20list%209.xls?alt=media&token=684b34a4-e559-44f5-8051-a3d76d016ba1'
  },
  {
    name: 'krithikai-sashti-9a',
    url: 'https://firebasestorage.googleapis.com/v0/b/thiruppugazhanbargal-515e8.appspot.com/o/listOfSongs%2Fkrithikai-sashti%20list%209a.xls?alt=media&token=e4245351-c46b-4ae0-97c6-8052658f6a42'
  },
  {
    name: 'krithikai-sashti-10',
    url: 'https://firebasestorage.googleapis.com/v0/b/thiruppugazhanbargal-515e8.appspot.com/o/listOfSongs%2Fkrithikai-sashti%20list%2010.xls?alt=media&token=2975700a-8c52-4471-ad5e-3d52058a82d4'
  },
  {
    name: 'krithikai-sashti-10a',
    url: 'https://firebasestorage.googleapis.com/v0/b/thiruppugazhanbargal-515e8.appspot.com/o/listOfSongs%2Fkrithikai-sashti%20list%2010a.xls?alt=media&token=99ad735b-7497-45a1-9c2a-49ccfbf0f2a6'
  },
  {
    name: 'krithikai-sashti-11',
    url: 'https://firebasestorage.googleapis.com/v0/b/thiruppugazhanbargal-515e8.appspot.com/o/listOfSongs%2Fkrithikai-sashti%20list%2011.xls?alt=media&token=99cd85c6-392c-4a8b-aa5e-1d7ee7890a88'
  },
  {
    name: 'krithikai-sashti-11a',
    url: 'https://firebasestorage.googleapis.com/v0/b/thiruppugazhanbargal-515e8.appspot.com/o/listOfSongs%2Fkrithikai-sashti%20list%2011a.xls?alt=media&token=17ae2483-fef4-49d6-bd8a-ab812ceaa2bd'
  },
  {
    name: 'krithikai-sashti-12',
    url: 'https://firebasestorage.googleapis.com/v0/b/thiruppugazhanbargal-515e8.appspot.com/o/listOfSongs%2Fkrithikai-sashti%20list%2012.xls?alt=media&token=c8821248-b784-4cce-a12f-4e7a9c424e2c'
  },
  {
    name: 'krithikai-sashti-12a',
    url: 'https://firebasestorage.googleapis.com/v0/b/thiruppugazhanbargal-515e8.appspot.com/o/listOfSongs%2Fkrithikai-sashti%20list%2012a.xls?alt=media&token=57d98de6-a740-4c1d-a33f-ed0759a84bd4'
  },
  {
    name: 'krithikai-sashti-13',
    url: 'https://firebasestorage.googleapis.com/v0/b/thiruppugazhanbargal-515e8.appspot.com/o/listOfSongs%2Fkrithikai-sashti%20list%2013.xls?alt=media&token=ca894cbd-2b86-470d-b408-f37bff681e1e'
  },
  {
    name: 'krithikai-sashti-13a',
    url: 'https://firebasestorage.googleapis.com/v0/b/thiruppugazhanbargal-515e8.appspot.com/o/listOfSongs%2Fkrithikai-sashti%20list%2013a.xls?alt=media&token=0d5589ea-10dd-42fd-b840-713d74e046d6'
  },
  {
    name: 'krithikai-sashti-14',
    url: 'https://firebasestorage.googleapis.com/v0/b/thiruppugazhanbargal-515e8.appspot.com/o/listOfSongs%2Fkrithikai-sashti%20list%2014.xls?alt=media&token=8b8736a8-6f2e-4a51-b00a-d5282a90149a'
  },
  {
    name: 'krithikai-sashti-14a',
    url: 'https://firebasestorage.googleapis.com/v0/b/thiruppugazhanbargal-515e8.appspot.com/o/listOfSongs%2Fkrithikai-sashti%20list%2014a.xls?alt=media&token=7489a0ab-cbf9-4e78-97cf-2399e373d9f7'
  },
  {
    name: 'krithikai-sashti-15',
    url: 'https://firebasestorage.googleapis.com/v0/b/thiruppugazhanbargal-515e8.appspot.com/o/listOfSongs%2Fkrithikai-sashti%20list%2015.xls?alt=media&token=85626d99-ff84-4fb6-93da-852f7b8a8348'
  },
  {
    name: 'krithikai-sashti-15a',
    url: 'https://firebasestorage.googleapis.com/v0/b/thiruppugazhanbargal-515e8.appspot.com/o/listOfSongs%2Fkrithikai-sashti%20list%2015a.xlsx?alt=media&token=89f1ddb8-a7e0-404a-af89-29d0adcc9dde'
  },
  {
    name: 'krithikai-sashti-16',
    url: 'https://firebasestorage.googleapis.com/v0/b/thiruppugazhanbargal-515e8.appspot.com/o/listOfSongs%2Fkrithikai-sashti%20list%2016.xls?alt=media&token=374723b1-251c-4e89-8077-db0525e06bb6'
  },
  {
    name: 'krithikai-sashti-16a',
    url: 'https://firebasestorage.googleapis.com/v0/b/thiruppugazhanbargal-515e8.appspot.com/o/listOfSongs%2Fkrithikai-sashti%20list%2016a.xls?alt=media&token=2afc11a0-abf6-4932-ada7-506131d19e2f'
  },
  {
    name: 'krithikai-sashti-17',
    url: 'https://firebasestorage.googleapis.com/v0/b/thiruppugazhanbargal-515e8.appspot.com/o/listOfSongs%2Fkrithikai-sashti%20list%2017.xls?alt=media&token=0bec479d-0248-4023-b741-651019844640'
  },
  {
    name: 'krithikai-sashti-17a',
    url: 'https://firebasestorage.googleapis.com/v0/b/thiruppugazhanbargal-515e8.appspot.com/o/listOfSongs%2Fkrithikai-sashti%20list%2017a.xls?alt=media&token=d336a680-8504-4eeb-90a6-b61abe622b87'
  },
  {
    name: 'krithikai-sashti-18',
    url: 'https://firebasestorage.googleapis.com/v0/b/thiruppugazhanbargal-515e8.appspot.com/o/listOfSongs%2Fkrithikai-sashti%20list%2018.xls?alt=media&token=02a67be4-91f5-4acd-804f-09922b938c46'
  },
  {
    name: 'krithikai-sashti-18a',
    url: 'https://firebasestorage.googleapis.com/v0/b/thiruppugazhanbargal-515e8.appspot.com/o/listOfSongs%2Fkrithikai-sashti%20list%2018a.xls?alt=media&token=c2462b71-9462-48b3-a4a5-e54c3ed82d86'
  },
  {
    name: 'krithikai-sashti-19',
    url: 'https://firebasestorage.googleapis.com/v0/b/thiruppugazhanbargal-515e8.appspot.com/o/listOfSongs%2Fkrithikai-sashti%20list%2019.xls?alt=media&token=7f5bbfa2-c25c-442b-aa4c-0db8cac7b909'
  },
  {
    name: 'krithikai-sashti-19a',
    url: 'https://firebasestorage.googleapis.com/v0/b/thiruppugazhanbargal-515e8.appspot.com/o/listOfSongs%2Fkrithikai-sashti%20list%2019a.xls?alt=media&token=719168ce-f7a5-4ba5-9769-5deb5bd7513e'
  },
  {
    name: 'krithikai-sashti-20',
    url: 'https://firebasestorage.googleapis.com/v0/b/thiruppugazhanbargal-515e8.appspot.com/o/listOfSongs%2Fkrithikai-sashti%20list%2020.xls?alt=media&token=e4c6b860-1e17-49e5-a6df-07b423a130a9'
  },
  {
    name: 'krithikai-sashti-20a',
    url: 'https://firebasestorage.googleapis.com/v0/b/thiruppugazhanbargal-515e8.appspot.com/o/listOfSongs%2Fkrithikai-sashti%20list%2020a.xls?alt=media&token=dae60bbb-d485-47fc-8798-c057ce424360'
  },
  {
    name: 'krithikai-sashti-21',
    url: 'https://firebasestorage.googleapis.com/v0/b/thiruppugazhanbargal-515e8.appspot.com/o/listOfSongs%2Fkrithikai-sashti%20list%2021.xls?alt=media&token=d0553fd1-8e83-4c52-8cf1-72086ea3ef5f'
  },
  {
    name: 'krithikai-sashti-21a',
    url: 'https://firebasestorage.googleapis.com/v0/b/thiruppugazhanbargal-515e8.appspot.com/o/listOfSongs%2Fkrithikai-sashti%20list%2021a.xls?alt=media&token=4fd298ed-379c-4db6-b7db-0cc9e1edd36c'
  },
];

@Component({
  selector: 'app-list-of-songs',
  templateUrl: './list-of-songs.component.html',
  styleUrls: ['./list-of-songs.component.css']
})
export class ListOfSongsComponent implements OnInit {

  listOfSongsData: DownloadTable[] = LIST_OF_SONGS;

  constructor(private store: Store<fromWelcome.State>, private route: ActivatedRoute) {
  }

  ngOnInit() {
    this.route.data.subscribe(
      (data: Data) => {
        this.store.dispatch(new welcomeActions.ChangeTitle(data.title));
      }
    );
  }

}
