import {Component, OnInit} from '@angular/core';
import {Store} from '@ngrx/store';
import * as fromWelcome from '../../store/welcome.reducer';
import {ActivatedRoute, Data} from '@angular/router';
import * as welcomeActions from '../../store/welcome.actions';


@Component({
  selector: 'app-major-events-and-office-bearers',
  templateUrl: './major-events-and-office-bearers.component.html',
  styleUrls: ['./major-events-and-office-bearers.component.css']
})
export class MajorEventsAndOfficeBearersComponent implements OnInit {



  constructor(private store: Store<fromWelcome.State>, private route: ActivatedRoute) {
  }

  ngOnInit() {
    this.route.data.subscribe(
      (data: Data) => {
        this.store.dispatch(new welcomeActions.ChangeTitle(data.title));
      }
    );
  }

}
