import {Component, Input, OnInit} from '@angular/core';
import {Events} from '../major-events.component';
import {MatBottomSheet, MatBottomSheetRef} from '@angular/material/bottom-sheet';
import {EventBottomSheetComponent} from '../../../../bottom-sheet/event-bottom-sheet/event-bottom-sheet.component';


@Component({
  selector: 'app-event',
  templateUrl: './event.component.html',
  styleUrls: ['./event.component.css']
})
export class EventComponent implements OnInit {

  @Input('events') events: Events[];

  constructor(private _bottomSheet: MatBottomSheet) {
  }

  ngOnInit() {
  }

  openBottomSheet(date, event) {
    this._bottomSheet.open(EventBottomSheetComponent, {
      data: {date: date, event}
    });
  }

}
