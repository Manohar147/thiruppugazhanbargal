import {Component, OnInit} from '@angular/core';

export interface Events {
  date: string;
  event: string;
}

const EVENTS: Events[] = [
  {date: '1965', event: 'First St Arunagirinathar Ninaivu Vizha in Delhi.'},
  {
    date: '1975',
    event: 'Sixth birth centenary celebrations of St Arunagirinathar,including release of a commemorative Indian postal stamp.'
  },
  {date: '1980', event: 'Guruji’s visit to UK for spreading Thiruppugazh.'},
  {date: '1983', event: 'Silver Jubilee Celebrations at Delhi.'},
  {
    date: '1988',
    event: 'Thiruppugazh Vaibhavam in Bangalore and visit to USA and Canada to propogate the teachings of St Arunagirinathar.'
  },
  {date: '1989', event: 'Arunagiri Mahotsavam celebrated in Delhi.'},
  {date: '1991', event: 'Release of Thiruppugazh Madaani at Chennai.'},
  {date: '1992', event: 'Thiruppugazh Utsavam—commemorating the completion of two decades of Thiruppugazh in Mumbai.'},
  {date: '1993', event: 'Release of audio cassette of Vel Mayil Viruthangal.'},
  {date: '1993', event: 'Release of book—Thiruppugazh Isai Vazhipadu—a compilation of 425 songs with ragas ,thals and chandams.'},
  {date: '1994', event: 'Silver Jubilee celebrations of Karnataka region.'},
  {date: '1996', event: 'Release of audio cassettes containing all songs on Thiruppugazh Isai Vazhipadu book.'},
  {date: '1997', event: 'Silver Jubilee celebrations of Mumbai region.'},
  {date: '1998', event: 'Thiruppugazh Thiruppani Vizha and Silver Jubilee of Chennai region.'},
  {
    date: '2001',
    event: 'Arunagirinathar Aanmeeka Peru Vizha and release of book with musical notations for 75 thiruppugazh songs at Chennai.'
  },
  {date: '2003', event: 'Pavazha Vizha of Guruji Sri A.S. raghavan at Chennai.'},
  {date: '2008', event: 'Golden Jubilee celebrations at Bangalore.'},
  {date: '2013', event: 'Thiruppugazh Aanmeeka Peru Vizha at Chennai.'},
  {date: '2014', event: 'Golden Jubilee year of Padi Vizha and Arunagirinathar vizha at Delhi.'},
  {date: '2015', event: 'Thiruppugazh Peru Vizha and Valli Kalyanam at Bangalore.'},
  {date: '2017  ', event: 'Guru Samarpana Thiruppugazh Isai Vizha (singing all 505 songs taught by our Guruji) at Chennai.'},
  {
    date: '2018',
    event: 'Mani Vizha-60th year celebrations of Thiruppugazh teaching started by Guruji Sri A.S. Raghavan,Release of the Isai Vazhipadu book in Telugu'
  },
  {
    date: '2019',
    event: 'Velli Vizha-Silver Jubilee of Thiruppugazh activities in Hyderabad Secunderabad. Launching of a web site for Thiruppugazh Anbargal with the aim to remember Guruji and his Paddhathi. Release of the Isai Vazhipadu book in English'
  },
];

@Component({
  selector: 'app-major-events',
  templateUrl: './major-events.component.html',
  styleUrls: ['./major-events.component.css']
})
export class MajorEventsComponent implements OnInit {

  public events: Events[] = EVENTS;

  constructor() {
  }

  ngOnInit() {
  }

}
