import {Component, Input, OnInit} from '@angular/core';
import {OfficeBearers, OfficeBearersHeading} from '../office-bearers.component';

@Component({
  selector: 'app-office-bearers-table',
  templateUrl: './office-bearers-table.component.html',
  styleUrls: ['./office-bearers-table.component.css']
})
export class OfficeBearersTableComponent implements OnInit {
  @Input('data') data: OfficeBearers[];
  @Input('heading') heading: OfficeBearersHeading;
  @Input('hasFour') hasFour: boolean;
  displayedColumns: string[] = ['h1', 'h2', 'h3', 'h4'];
  displayedColumnsNew: string[] = ['h1', 'h2', 'h3', 'h4', 'h5'];
  dataSource = [];

  constructor() {
  }

  ngOnInit() {
    this.dataSource = this.data;
  }

}
