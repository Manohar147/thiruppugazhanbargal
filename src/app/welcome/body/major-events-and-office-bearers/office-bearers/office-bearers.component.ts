import {Component, OnInit} from '@angular/core';
import {post} from 'selenium-webdriver/http';


export interface OfficeBearers {
  post: string;
  nameA: string;
  nameB: string;
  nameC: string;
  nameD?: string;
}

export interface OfficeBearersHeading {
  h1: string;
  h2: string;
  h3: string;
  h4: string;
  h5?: string;
}

export interface OfficeBearersObject {
  data: OfficeBearers[];
  heading: OfficeBearersHeading;
}

const TABLE_ONE: OfficeBearers[] = [
  {post: 'PRESIDENT', nameA: 'Sri A.S.Raghavan', nameB: 'Sri A.S.Raghavan', nameC: 'Sri A.S.Raghavan'},
  {post: 'VICE PRESIDENT', nameA: 'Sri S.B.Subramanian', nameB: 'Sri S.B.Subramanian', nameC: 'Sri A.G.Krishnan'},
  {post: 'SECRETARY', nameA: 'Sri K.Lakshminarayanan', nameB: 'Sri G.Krishnan', nameC: 'Sri A.G.Krishnan'},
  {post: 'JOINT SECRETARY', nameA: '-', nameB: '-', nameC: 'Sri N.R.subbaram'},
  {post: 'TREASURER', nameA: 'Sri N.Ramamurthi', nameB: 'Sri N.Ramamurthi', nameC: 'Sri N.Ramamurthi'},
  {
    post: 'MEMBERS', nameA: 'Sri G.Sundaram' +
      'Sri A.Sundaram,' +
      ' Sri G.Krishnan,' +
      ' Sri T.M.subramanian,' +
      ' Smt Lakshmi Kumari,' +
      'Smt Jaya Ramachandran', nameB: '-', nameC: 'Sri E.N.Murthy,' +
      ' Sri G.Sundaram,' +
      ' Sri A.Natarajan,' +
      ' Sri T.M.subramanian,' +
      ' Smt S.Doraiswamy,' +
      ' Smt Lakshmi Kumari'
  },
  {
    post: 'REGIONS - CHENNAI, KARNATAKA & MUMBAI', nameA: '-', nameB: '-', nameC: 'Sri S.K.Ramanathan,' +
      ' Sri R.Venkataraman,' +
      ' Sri A.S.Subramanian'
  },
];
const TABLE_ONE_HEADING: OfficeBearersHeading = {
  h1: 'Office bearers',
  h2: 'For purpose of registration 26.8 1987',
  h3: 'For purpose of accounts' +
    '26.8 87 to 31.3.90',
  h4: '1990-91,' +
    ' 1991-92,' +
    ' 1992-93'
};

const TABLE_TWO: OfficeBearers[] = [
  {post: 'PRESIDENT', nameA: 'Sri A.S.Raghavan', nameB: 'Sri A.S.Raghavan', nameC: 'Sri A.S.Raghavan'},
  {post: 'VICE PRESIDENT', nameA: 'Sri K.Lakshminarayanan', nameB: 'Sri G.Sundaram', nameC: 'Sri G.Sundaram'},
  {post: 'SECRETARY', nameA: 'Sri E.N.Murthy', nameB: 'Sri E.N.Murthy', nameC: 'Sri E.N.Murthy'},
  {post: 'JOINT SECRETARY', nameA: 'Smt S.Padma', nameB: 'Smt S.Padma', nameC: 'Smt S.Padma'},
  {post: 'TREASURER', nameA: 'Sri N.Ramamurthi', nameB: 'Sri N.Ramamurthi', nameC: 'Sri N.Ramamurthi'},
  {
    post: 'MEMBERS', nameA: 'Sri G.Sundaram,' +
      ' Sri M.B.Kumaraswamy,' +
      ' Sri N.Rajan,' +
      ' Sri K.Subbiah,' +
      ' Sri G.V.subramanian',
    nameB: 'Sri G.V.Subramanian,' +
      ' Sri K.Subbiah,' +
      ' Sri M.R.Ramani,' +
      ' Smt Uma Balasubramanian', nameC: 'Sri K.Subbiah,' +
      ' Sri C V Jayaraman,' +
      ' Sri C N K Swamy,' +
      ' Smt Uma Balasubramanian,' +
      ' Smt Chandra P'
  },
  {
    post: 'REGIONS - CHENNAI, KARNATAKA & MUMBAI', nameA: 'Sri A.S. Subramanian,' +
      ' Sri R.Venkataraman,' +
      ' Sri K.N.Sivaraman',
    nameB: 'Sri A.S. Subramanian,' +
      ' Sri R.Venkataraman,' +
      ' Smt Padma venkataraman,' +
      ' Sri G Krishnan,' +
      ' Sri T V Ramachandran', nameC: 'Sri G Balasubramanian,' +
      ' Sri T V Ramachandran,' +
      ' Smt Padma venkataraman,' +
      ' Sri N S Mani (Delhi)'
  },
];

const TABLE_TWO_HEADING = {
  h1: 'Office bearers',
  h2: '1993-94,' +
    '1994-95,' +
    '1995-96',
  h3: '1996-97,' +
    '1997-98,' +
    '1998-99',
  h4: '1999-2000,' +
    '2000-2001,' +
    '2001-2002'
};

const TABLE_THREE: OfficeBearers[] = [
  {post: 'PRESIDENT', nameA: 'Sri A.S.Raghavan', nameB: 'Sri A.S.Raghavan', nameC: 'Sri A.S.Raghavan'},
  {post: 'VICE PRESIDENT', nameA: 'Sri Ganesh Sundaram', nameB: 'Sri EN Murthy', nameC: 'Sri Ganesh Sundaram'},
  {post: 'SECRETARY', nameA: 'Smt S Padma', nameB: 'Smt S Padma', nameC: 'Smt S Padma'},
  {post: 'JOINT SECRETARY', nameA: 'Smt Uma Balasubramanian', nameB: 'Smt Uma Balasubramanian', nameC: 'Smt Uma Balasubramanian'},
  {post: 'TREASURER', nameA: 'Sri N Rajan', nameB: 'Sri N Rajan', nameC: 'Sri N Ramamurthi'},
  {
    post: 'MEMBERS',
    nameA: 'Sri K Subbiah,' +
      ' Sri CV Jayaraman,' +
      ' Sri N Ramamurthi,' +
      ' Sri E N Muthy,' +
      ' Sri V Sethuraman',
    nameB: 'Sri K Subbiah,' +
      ' Sri C V Jayaraman,' +
      ' Sri N Ramamurthi,' +
      ' Sri V Sethuraman',
    nameC: 'Sri K Subbiah,' +
      ' Sri C V Jayaraman,' +
      ' Sri G Krishnan,' +
      ' Sri N Rajan,' +
      ' Smt Chandra P'
  },
  {
    post: 'REGIONS - CHENNAI, KARNATAKA & MUMBAI',
    nameA: 'Sri G Balasubramanian,' +
      ' Sri KN Krishnamurthy,' +
      ' Smt Padma V',
    nameB: 'Sri G Balasubramanian,' +
      ' Sri KN Krishnamurthy,' +
      ' Smt Padma V',
    nameC: 'Sri G Balasubramanian,' +
      ' Sri KN Krishnamurthy,' +
      ' Smt Padma V'
  },
];
const TABLE_THREE_HEADING: OfficeBearersHeading = {
  h1: 'Office bearers',
  h2: '2002-2003,' +
    '2003-2004',
  h3: '2004-2005',
  h4: '2005-2006,' +
    '2006-2007,' +
    '2007-2008'
};

const TABLE_FOUR: OfficeBearers[] = [
  {post: 'PRESIDENT', nameA: 'Sri A.S.Raghavan', nameB: 'Smt Padma V', nameC: 'Sri KN Krishnamurthy', nameD: 'Sri KN Krishnamurthy'},
  {
    post: 'VICE PRESIDENT',
    nameA: 'Sri Ganesh Sundaram',
    nameB: 'Sri Ganesh Sundaram',
    nameC: 'Sri Ganesh Sundaram',
    nameD: 'Sri Ganesh Sundaram'
  },
  {post: 'SECRETARY', nameA: 'Smt S Padma', nameB: 'Smt S Padma', nameC: 'Sri R Nagesh', nameD: 'Sri R Nagesh'},
  {post: 'JOINT SECRETARY', nameA: 'Smt S.Padma', nameB: 'Smt S.Padma', nameC: 'Smt S.Padma', nameD: '-'},
  {
    post: 'TREASURER',
    nameA: 'Sri N.Ramamurthi',
    nameB: 'Sri N Rajan',
    nameC: 'Sri JA Kuppu Subramanian',
    nameD: 'Sri JA Kuppu Subramanian'
  },
  {
    post: 'MEMBERS',
    nameA: 'Sri G Krishnan,' +
      ' Sri CV Jayaraman,' +
      ' Sri G Vasudevan,' +
      ' Smt Uma B,' +
      ' Smt Girija R',
    nameB: 'Sri G.V.Subramanian,' +
      ' Sri CV Jayaraman,' +
      ' Sri G Vasudevan,' +
      ' Smt Uma B,' +
      ' Smt Seetha V',
    nameC: 'Sri R Ramanathan,' +
      ' Sri N Rajan,' +
      ' Sri K Balasubramanian,' +
      ' Dr M Natarajan,' +
      ' Smt Vasantha P',
    nameD: 'Sri TS Sakthidharan,' +
      ' Sri RS Balaji,' +
      ' Sri K Balasubramanian,' +
      ' Smt Vasantha P,' +
      ' Sri AS Nagarajan'
  },
  {
    post: 'REGIONS - CHENNAI, KARNATAKA & MUMBAI',
    nameA: 'Sri G Balasubramanian,' +
      ' Sri KN Krishnamurthy,' +
      ' Smt Padma V,' +
      ' Sri R Thiagarajan',
    nameB: 'Sri G Balasubramanian,' +
      ' Sri KN Krishnamurthy,' +
      ' Sri R Thiagarajan',
    nameC: 'Sri G Balasubramanian,' +
      ' Sri AN Arumugan,' +
      ' Sri Ramnath S Mani',
    nameD: 'Sri G Balasubramanian,' +
      ' Sri SG Ramakrishnan,' +
      ' Sri Ramnath S Mani'
  },
];

const TABLE_FOUR_HEADING: OfficeBearersHeading = {
  h1: 'Office bearers',
  h2: '2008-2009,' +
    '2009-2010,' +
    '2010-2011',
  h3: '2011-2012,' +
    '2012-2013,' +
    '2013-2014',
  h4: '2014-2015,' +
    '2015-2016,' +
    '2016-2017',
  h5: '2017-2018,' +
    '2018-2019'
};

@Component({
  selector: 'app-office-bearers',
  templateUrl: './office-bearers.component.html',
  styleUrls: ['./office-bearers.component.css']
})
export class OfficeBearersComponent implements OnInit {

  tableOne: OfficeBearersObject = {
    data: TABLE_ONE,
    heading: TABLE_ONE_HEADING
  };
  tableTwo: OfficeBearersObject = {
    data: TABLE_TWO,
    heading: TABLE_TWO_HEADING
  };
  tableThree: OfficeBearersObject = {
    data: TABLE_THREE,
    heading: TABLE_THREE_HEADING
  };
  tableFour: OfficeBearersObject = {
    data: TABLE_FOUR,
    heading: TABLE_FOUR_HEADING
  };

  constructor() {
  }

  ngOnInit() {
  }

}
