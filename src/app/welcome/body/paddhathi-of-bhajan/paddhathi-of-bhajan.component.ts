import {Component, OnInit} from '@angular/core';
import {ActivatedRoute, Data} from '@angular/router';
import * as welcomeActions from '../../store/welcome.actions';
import {Store} from '@ngrx/store';
import * as fromWelcome from '../../store/welcome.reducer';

@Component({
  selector: 'app-paddhathi-of-bhajan',
  templateUrl: './paddhathi-of-bhajan.component.html',
  styleUrls: ['./paddhathi-of-bhajan.component.css']
})
export class PaddhathiOfBhajanComponent implements OnInit {

  constructor(private store: Store<fromWelcome.State>, private route: ActivatedRoute) {
  }

  ngOnInit() {
    this.route.data.subscribe(
      (data: Data) => {
        this.store.dispatch(new welcomeActions.ChangeTitle(data.title));
      }
    );
  }

}
