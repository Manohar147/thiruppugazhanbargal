import { Component, OnInit } from '@angular/core';
import {Store} from '@ngrx/store';
import * as fromWelcome from '../../store/welcome.reducer';
import {ActivatedRoute, Data} from '@angular/router';
import * as welcomeActions from '../../store/welcome.actions';


@Component({
  selector: 'app-photo-gallery',
  templateUrl: './photo-gallery.component.html',
  styleUrls: ['./photo-gallery.component.css']
})
export class PhotoGalleryComponent implements OnInit {

  constructor(private store: Store<fromWelcome.State>, private route: ActivatedRoute) { }

  ngOnInit() {
    this.route.data.subscribe(
      (data: Data) => {
        this.store.dispatch(new welcomeActions.ChangeTitle(data.title));
      }
    );
  }

}
