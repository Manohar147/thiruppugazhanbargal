import {AfterViewInit, Component, OnInit} from '@angular/core';
import {Observable} from 'rxjs';
import * as fromAuth from '../../../../user-auth/auth-store/auth.reducer';
import * as welcomeActions from '../../../store/welcome.actions';
import {Store} from '@ngrx/store';
import PlayList from '../../../data/playlist';
import {MatDialog, MatSnackBar} from '@angular/material';
import {ViewPlaylistDialogComponent} from '../../../dialog/view-playlist-dialog/view-playlist-dialog.component';
import {LoginErrorSnackbarComponent} from '../../../snackbar/login-error-snackbar/login-error-snackbar.component';
import {ExcelExportService} from '../../../../excel/excel-export.service';
import {User} from 'firebase';

@Component({
  selector: 'app-playlist',
  templateUrl: './playlist.component.html',
  styleUrls: ['./playlist.component.css']
})
export class PlaylistComponent implements OnInit, AfterViewInit {
  index = 0;
  durationInSeconds = 3000;
  public authState: Observable<fromAuth.State>;
  isLoggedIn = false;
  user: User;

  constructor(private authStore: Store<fromAuth.State>, public dialog: MatDialog, private _snackBar: MatSnackBar
              ) {
  }

  ngOnInit() {
    this.authState = this.authStore.select('auth');
    this.authStore.select('auth').subscribe(
      (data: fromAuth.State) => {
        this.isLoggedIn = data.isLoggedIn;
        this.user = data.user;
      }
    );
  }

  exportPlayList(playList: PlayList) {
    if (this.isLoggedIn) {
      ExcelExportService.exportPlaylistToExcel(playList, this.user);
    } else {
      this.openLoginSnackBar();
    }

  }


  viewPlayList(playList: PlayList, index: number) {
    if (this.isLoggedIn) {
      this.index = index;
      this.openDialog(playList);
    } else {
      this.openLoginSnackBar();
    }

  }

  deletePlayList(playlist: PlayList, index: number) {
    if (this.isLoggedIn) {
      this.authStore.dispatch(new welcomeActions.DeleteUserPlaylist({playlist: playlist, index: index}));
    } else {
      this.openLoginSnackBar();
    }
  }


  openDialog(playList: PlayList): void {
    const dialogRef = this.dialog.open(ViewPlaylistDialogComponent, {
      data: {...playList}
    });

  }

  openLoginSnackBar() {
    this._snackBar.openFromComponent(LoginErrorSnackbarComponent, {
      duration: this.durationInSeconds,
      data: {message: 'view/delete/export playlist', route: 'welcome/SongsWithScript', queryParams: {showPlaylist: 1}}
    });
  }

  ngAfterViewInit(): void {

  }

}
