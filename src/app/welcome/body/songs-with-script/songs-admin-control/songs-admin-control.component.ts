import {Component, OnInit} from '@angular/core';
import {Store} from '@ngrx/store';
import * as fromAuth from '../../../../user-auth/auth-store/auth.reducer';
import * as fromWelcome from '../../../store/welcome.reducer';
import {Observable} from 'rxjs';
import {FormControl, FormGroup, FormGroupDirective, NgForm, Validators} from '@angular/forms';
import {ErrorStateMatcher, MatDialog, MatSnackBar} from '@angular/material';
import {DataService, RetriveSong} from '../../../data/data.service';
import {AdminSongUpdateDialogComponent} from '../../../dialog/admin-song-update-dialog/admin-song-update-dialog.component';

export class MyErrorStateMatcher implements ErrorStateMatcher {
  isErrorState(control: FormControl | null, form: FormGroupDirective | NgForm | null): boolean {
    const isSubmitted = form && form.submitted;
    return !!(control && control.invalid && (control.dirty || control.touched || isSubmitted));
  }
}

@Component({
  selector: 'app-songs-admin-control',
  templateUrl: './songs-admin-control.component.html',
  styleUrls: ['./songs-admin-control.component.css']
})
export class SongsAdminControlComponent implements OnInit {

  authState: Observable<fromAuth.State>;
  welcomeState: Observable<fromWelcome.State>;
  adminForm: FormGroup;
  matcher = new MyErrorStateMatcher();


  constructor(private authStore: Store<fromAuth.State>, private welcomeStore: Store<fromWelcome.State>,
              private dataService: DataService, private _snackBar: MatSnackBar, public dialog: MatDialog,) {
  }

  ngOnInit() {
    this.authState = this.authStore.select('auth');
    this.welcomeState = this.welcomeStore.select('welcome');

    this.adminForm = new FormGroup({
      songNumber: new FormControl(null, [Validators.required,
        Validators.min(1),Validators.max(503)]),
    });
  }

  onSubmit() {
    if (this.adminForm.valid) {
      this.dataService.getSongDetails(this.adminForm.value.songNumber).subscribe(
        (song: RetriveSong) => {
          this.openDialog(song);
        }
      );
    } else {
      this._snackBar.open(`Please enter valid number`, null, {
        duration: 2000
      });
    }
  }

  openDialog(songDetails: RetriveSong): void {
    const dialogRef = this.dialog.open(AdminSongUpdateDialogComponent, {
      data: songDetails,
    });
  }

}
