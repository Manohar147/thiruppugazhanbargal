import {AfterViewInit, Component, Input, OnInit, ViewChild} from '@angular/core';
import Song from '../../../data/song';
import {MatDialog, MatPaginator, MatSnackBar, MatTableDataSource} from '@angular/material';
import {SelectionModel} from '@angular/cdk/collections';
import {CreatePlaylistDialog} from '../../../dialog/create-playlist-dialog/create-playlist-dialog';
import {Store} from '@ngrx/store';
import * as fromAuth from '../../../../user-auth/auth-store/auth.reducer';
import {LoginErrorSnackbarComponent} from '../../../snackbar/login-error-snackbar/login-error-snackbar.component';
import {SignInDialogComponent} from '../../../dialog/sign-in-dialog/sign-in-dialog.component';
import PlayList from '../../../data/playlist';

@Component({
  selector: 'app-songs-table',
  templateUrl: './songs-table.component.html',
  styleUrls: ['./songs-table.component.css']
})
export class SongsTableComponent implements OnInit, AfterViewInit {

  @ViewChild(MatPaginator) paginator: MatPaginator;
  displayedColumns: string[] = ['select', 'song name', 'new number', 'old number', 'ragam', 'thalam', 'tamil meaning', 'meaning', 'classify1', 'classify2', 'classify3'];
  @Input('songs') songs: Song[];
  dataSource: MatTableDataSource<Song>;
  selection = new SelectionModel<Song>(true);
  playListName = null;
  description = null;
  durationInSeconds = 3000;
  isLoggedIn: boolean = false;
  playlist: PlayList[] = [];


  constructor(private _snackBar: MatSnackBar, public dialog: MatDialog,
              private authStore: Store<fromAuth.State>) {
  }

  ngOnInit() {
    this.dataSource = new MatTableDataSource(this.songs);
    this.dataSource.paginator = this.paginator;
    this.authStore.select('auth').subscribe(
      (data: fromAuth.State) => {
        this.isLoggedIn = data.isLoggedIn;
      }
    );
    this.authStore.select('auth').subscribe(
      (data: fromAuth.State) => {
        if (data.userLibrary) {
          if (data.userLibrary.playList) {
            this.playlist = data.userLibrary.playList;
          }
        }
      }
    );

  }

  /** Whether the number of selected elements matches the total number of rows. */
  isAllSelected() {
    const numSelected = this.selection.selected.length;
    const numRows = this.dataSource.data.length;
    return numSelected === numRows;
  }

  /** Selects all rows if they are not all selected; otherwise clear selection. */
  masterToggle() {
    this.isAllSelected() ?
      this.selection.clear() :
      this.dataSource.data.forEach(row => this.selection.select(row));
  }

  /** The label for the checkbox on the passed row */
  checkboxLabel(row?: Song): string {
    if (!row) {
      return `${this.isAllSelected() ? 'select' : 'deselect'} all`;
    }
    return `${this.selection.isSelected(row) ? 'deselect' : 'select'} row ${row.newNumber + 1}`;
  }

  openDialog(): void {
    const dialogRef = this.dialog.open(CreatePlaylistDialog, {
      data: {
        playListName: this.playListName,
        description: this.description,
        playlist: (this.selection.selected)
      },
    });

    dialogRef.afterClosed().subscribe(result => {
      this.selection.clear();
      if (result) {
        this._snackBar.open(`Playlist ${result} saved successfully`, null, {
          duration: this.durationInSeconds
        });
      }
    });
  }

  savePlaylist() {
    if (this.isLoggedIn) {
      if (this.playlist.length < 6) {
        if (this.selection.selected.length > 0) {
          this.openDialog();
        } else {
          this._snackBar.open(`Please select a checkbox`, null, {
            duration: this.durationInSeconds
          });
        }
      } else {
        this._snackBar.open(`You can have only upto 6 playlist's`, null, {
          duration: this.durationInSeconds
        });
      }
    } else {
      this.openLoginSnackBar();
    }
  }

  applyFilter(filterValue: string) {
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }

  appendRow(row: Song) {
    if (this.isLoggedIn) {
      this.selection.toggle(row);
    } else {
      this.openLoginSnackBar();
    }
  }

  openLoginSnackBar() {
    this._snackBar.openFromComponent(LoginErrorSnackbarComponent, {
      duration: this.durationInSeconds,
      data: {message: 'save/create playlist', route: 'welcome/SongsWithScript', queryParams: {showPlaylist: 0}}
    });
  }

  showLoginDialog() {
    this.dialog.open(SignInDialogComponent, {
      data: {
        route: 'welcome/SongsWithScript',
        queryParams: {showPlaylist: 0}
      }
    });
  }

  ngAfterViewInit(): void {

  }
}
