import {AfterViewInit, Component, OnInit} from '@angular/core';
import {SignInDialogComponent} from '../../../dialog/sign-in-dialog/sign-in-dialog.component';
import {MatDialog} from '@angular/material';
import {Store} from '@ngrx/store';
import * as fromAuth from '../../../../user-auth/auth-store/auth.reducer';
import {Observable} from 'rxjs';
import {skip, take, takeLast} from 'rxjs/operators';


@Component({
  selector: 'app-songs-with-script-header',
  templateUrl: './songs-with-script-header.component.html',
  styleUrls: ['./songs-with-script-header.component.css']
})
export class SongsWithScriptHeaderComponent implements OnInit, AfterViewInit {
  authState: Observable<fromAuth.State>;
  isLoggedIn: boolean = false;
  isDialogOpen = false;


  constructor(public dialog: MatDialog, private authStore: Store<fromAuth.State>) {
  }

  ngOnInit() {
    this.authState = this.authStore.select('auth');
    this.authStore.select('auth').pipe(
      take(1),
    ).subscribe(
      (data: fromAuth.State) => {
        if (data.isLoggedIn === true) {
          this.isLoggedIn = data.isLoggedIn;
        }
        if (!this.isLoggedIn && !this.isDialogOpen) {
          setTimeout(() => {
            this.showLoginDialog();
          });
        }
      }
    );
    this.dialog.afterAllClosed.subscribe(
      () => {
        this.isDialogOpen = false;
      }
    );
  }

  showLoginDialog() {
    this.dialog.open(SignInDialogComponent, {
      data: {
        route: 'welcome/SongsWithScript',
        queryParams: {showPlaylist: 0}
      }
    });
    this.isDialogOpen = true;
  }

  ngAfterViewInit(): void {

  }

}
