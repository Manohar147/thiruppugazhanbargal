import {AfterViewInit, Component, OnInit, ViewChild} from '@angular/core';
import {Store} from '@ngrx/store';
import * as fromWelcome from '../../store/welcome.reducer';
import {ActivatedRoute, Data, Router} from '@angular/router';
import * as welcomeActions from '../../store/welcome.actions';
import Song from '../../data/song';
import {MatPaginator} from '@angular/material/paginator';
import {Observable,} from 'rxjs';
import * as fromAuth from '../../../user-auth/auth-store/auth.reducer';

export interface DialogData {
  playlistName: string;
  description: string;
  playlist: Song[];
}


@Component({
  selector: 'app-songs-with-script',
  templateUrl: './songs-with-script.component.html',
  styleUrls: ['./songs-with-script.component.css']
})
export class SongsWithScriptComponent implements OnInit, AfterViewInit {

  @ViewChild(MatPaginator) paginator: MatPaginator;
  authState: Observable<fromAuth.State>;
  welcomeState: Observable<fromWelcome.State>;
  selectedTab = 0;


  constructor(private welcomeStore: Store<fromWelcome.State>,
              private authStore: Store<fromAuth.State>,
              private route: ActivatedRoute,
              private router: Router) {
  }

  ngOnInit() {
    this.route.data.subscribe(
      (data: Data) => {
        this.welcomeStore.dispatch(new welcomeActions.ChangeTitle(data.title));
      }
    );
    this.route.queryParams.subscribe(
      params => {
        this.selectedTab = +params.showPlaylist;
      }
    );
    this.authState = this.authStore.select('auth');
    this.welcomeState = this.welcomeStore.select('welcome');
  }

  ngAfterViewInit(): void {

  }

  onTabChange(event) {
    this.router.navigate(['welcome', 'SongsWithScript'], {queryParams: {showPlaylist: event.index}}).catch(
      error => console.error('Routing Error : ', error)
    );
  }

}
