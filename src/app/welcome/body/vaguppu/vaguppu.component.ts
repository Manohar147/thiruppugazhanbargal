  import {Component, OnInit} from '@angular/core';
import {ActivatedRoute, Data} from '@angular/router';
import * as welcomeActions from '../../store/welcome.actions';
import {Store} from '@ngrx/store';
import * as fromWelcome from '../../store/welcome.reducer';
import {DownloadTable} from '../../table/download-table/download-table.component';


const VAGUPPU_DATA: DownloadTable[] = [
  {name: 'seerpaadhavagupu', url: 'https://drive.google.com/open?id=1zUvL7E5bHvn4u-PTnSmbHztVZUu3IFaB'},
  {name: 'devendrasanga', url: 'https://drive.google.com/open?id=1-nDynNboR9Nd-rHA5WUvSO2_yxatt1ol'},
  {name: 'velvagupu', url: 'https://drive.google.com/open?id=1FwaMwRo0LK0kHzHofbcF8iMKvqfcCCVs'},
  {name: 'thiruveLaikaran', url: 'https://drive.google.com/open?id=1zqzgp_j8UEy1BxpipV9GEpKFGL2dysgS'},
  {name: 'peruthavachanam', url: 'https://drive.google.com/open?id=1rWjR_bLsY24Fhd5AkBrzRJifA5QQA6n0'},
  {name: 'vedichikavalan', url: 'https://drive.google.com/open?id=1HJjULAapfsa6OBuaW9i3Hftl7dKOzgAs'},
  {name: 'sevagan', url: 'https://drive.google.com/open?id=1NFCJVU8AWrIFcf0idlWo_PCxg0FwjZ2J'},
  {name: 'puyavagupu', url: 'https://drive.google.com/open?id=1JPAsY-y2Z2i19MkivYNhu4gX_KEV9jEr'},
  {name: 'kadaikaniyal', url: 'https://drive.google.com/open?id=1gX0MHSD8ShRsbpQ-_mPM7pGuZVIkxh9y'},
  {name: 'mayilvagupu', url: 'https://drive.google.com/open?id=1bA2ohdv9456pR-riJ7Jwc34kHnE_XjmT'},
  {name: 'veerevaal', url: 'https://drive.google.com/open?id=1Pab3rjxjyvJ6_dJhsiJIKiH0KCmZ26Qf'},
  {name: 'Pazhani vagupu', url: 'https://drive.google.com/open?id=1yJtU2dW5tlg9dup8ElwtdXLK7Bb1WKvF'},
];

@Component({
  selector: 'app-vaguppu',
  templateUrl: './vaguppu.component.html',
  styleUrls: ['./vaguppu.component.css']
})
export class VaguppuComponent implements OnInit {

  public vaguppuData: DownloadTable[] = VAGUPPU_DATA;

  constructor(private store: Store<fromWelcome.State>, private route: ActivatedRoute) {
  }

  ngOnInit() {
    this.route.data.subscribe(
      (data: Data) => {
        this.store.dispatch(new welcomeActions.ChangeTitle(data.title));
      }
    );
  }

}
