import {Component, OnInit} from '@angular/core';
import {Store} from '@ngrx/store';
import * as fromWelcome from '../../store/welcome.reducer';
import {ActivatedRoute, Data} from '@angular/router';
import * as welcomeActions from '../../store/welcome.actions';
import {DownloadTable} from '../../table/download-table/download-table.component';


const VALLI_KALTANAM: DownloadTable[] = [
  {name: '01GajananamViruthamNattai', url: 'https://drive.google.com/file/d/1rVD6hnhHKu7ZatU812ONAmC6HhAqUp3j/view?usp=sharing'},
  {name: '03AnthamViruthamSaveri', url: 'https://drive.google.com/file/d/1GfP6lbbF_6mt-mpheb2TZBXMHizhw657/view?usp=sharing'},
  {
    name: '04ThiruvadiyumViruthamMohanamMurthiSir',
    url: 'https://drive.google.com/file/d/1I9xKJJ0A3nsMbsebu4kvwJyfB8uIP7i-/view?usp=sharing'
  },
  {name: '05SenKelViruthamDarbariKanadaManiSir', url: 'https://drive.google.com/file/d/1-dsU0zporsqJzGNg_AUqERqLLUlKw5Rs/view?usp=sharing'},
  {
    name: '06AmararSigamaniyeViruthamHamsananthiRamaniSir',
    url: 'https://drive.google.com/file/d/1Z2tJzHAMI2yEr7e49UGKViugEh9A8sVL/view?usp=sharing'
  },
  {name: '07VallikkuSivaranjani', url: 'https://drive.google.com/file/d/1D3DLJfgUKX59kgYE6I_wjNAYkzojFYsL/view?usp=sharing'},
  {name: '08VelMurugaManolayam', url: 'https://drive.google.com/file/d/18Y974sJbehj_A9p_V2izVSfhPnF5wLaD/view?usp=sharing'},
  {
    name: '09ArimugamMuruganAluthariyaArumugamum',
    url: 'https://drive.google.com/file/d/1Q1oNesPaObY4YaklvrN_QaQqJUSvjbo9/view?usp=sharing'
  },
  {name: '10MalaiMatralThilang', url: 'https://drive.google.com/file/d/1CvV6sbJq-Y4D5hAxtYM1NKApdHsdO0SK/view?usp=sharing'},
  {name: '11MalaiMatral', url: 'https://drive.google.com/file/d/13q18sxrFxmScC_M3_N8wQMrP-phae4v0/view?usp=sharing'},
  {name: '12UnjalAduthal', url: 'https://drive.google.com/file/d/1QeU2Ts72AJbOYvX_5izqhDfRsjJ-FeFj/view?usp=sharing'},
  {name: '13Kannunjal', url: 'https://drive.google.com/file/d/1z4cjIPmYVR0KELhuewNPuXD1hB9qsywK/view?usp=sharing'},
  {name: '14VelMurugaVelan', url: 'https://drive.google.com/file/d/1dx1xDxxffDKQO7BnmtiRIXBnh3picyfp/view?usp=sharing'},
  {name: '15ValliKalyanaVaibogame', url: 'https://drive.google.com/file/d/1jq7GooqhC3eQhu2Qwv1pbFrbJdbQa2La/view?usp=sharing'},
  {name: '16Pravaram', url: 'https://drive.google.com/file/d/1m4uH5eS3vavRQ-i_TSp1JipqqIUnfdRT/view?usp=sharing'},
  {name: '17Pravaram', url: 'https://drive.google.com/file/d/19xapBASdEf9yX1mJsI0PyTT6LJUqzB_P/view?usp=sharing'},
  {name: '18Manthiram', url: 'https://drive.google.com/file/d/13KQzNugGqLx_-cKtEnD5d3mcCa6TezvW/view?usp=sharing'},
  {name: '19Manamenum', url: 'https://drive.google.com/file/d/15AvSbYWt3-aiPca5bRP2Y90mN0Bz2z2F/view?usp=sharing'},
  {name: '20PumathudaneeAni', url: 'https://drive.google.com/file/d/1qUaJ79ABIEmnNizsNz8yAfimgrpirX2M/view?usp=sharing'},
  {name: '21MurugaNamavali', url: 'https://drive.google.com/file/d/1Kzs16r31FgsR4v8YLSqQYvKxsp6Kby-P/view?usp=sharing'},
  {name: '22GurujiSpeak', url: 'https://drive.google.com/file/d/1evVky7cfmY0YPeyIJ7hQGn0O7WQYm5eC/view?usp=sharing'},
  {name: '23Mangalyatharanam', url: 'https://drive.google.com/file/d/16qSTDWcWu7kicKGK6CGDTE2BjpQFoarn/view?usp=sharing'},
  {name: '24PerumViruthamHindolam', url: 'https://drive.google.com/file/d/1YCx8SG8-woB8eAZ23KiGwjlu0-IH-8-6/view?usp=sharing'},
  {name: '25Puraipadum', url: 'https://drive.google.com/file/d/12d-Nsb8DSwE95LQ2UPYpGoztFIS48soe/view?usp=sharing '},
  {name: '26Olamaraikal', url: 'https://drive.google.com/file/d/1eV1BEmWZFqkn7Q_xwUNtT5q09hTk7EFn/view?usp=sharing'},
  {name: '27JayaJaya', url: 'https://drive.google.com/file/d/1Z6Zxa3jNQOHwbyxUAO8Ex0uSqoZV8qIL/view?usp=sharing'},
  {name: '28Pangayanaar', url: 'https://drive.google.com/file/d/1n8HDvgqfaNoysrlNX-aQCfO2CCmEe5CN/view?usp=sharing'},
  {name: '29PangayaContd', url: 'https://drive.google.com/file/d/1WJ0MSeAvox7umNLqeWM0wYnNeCy9rDp4/view?usp=sharing'},
  {name: '30AvagunaViraganai', url: 'https://drive.google.com/file/d/1nv-Zs7OQUu_geECP089R1JJp10QFoiil/view?usp=sharing'},
  {name: '31AlliVizhiyaalum', url: 'https://drive.google.com/file/d/1JzODXiw_B8fmc9RFCi11Q8bVJenYM4hk/view?usp=sharing'},
  {name: '32AgaramuMaakiPurvikalyani', url: 'https://drive.google.com/file/d/1v0eNermsvzYogjEGiTOFyChjVOy0SaAi/view?usp=sharing'},
  {
    name: '33SollukakkuViruthamBrinthavanaSarangaSaravanabava',
    url: 'https://drive.google.com/file/d/1gyRgzZawOYoxLw3mkft4OAC8fXC4HCrW/view?usp=sharing'
  },
  {name: '34VazhthuSinduBhairavi', url: 'https://drive.google.com/file/d/11BoWg4N5Xmpav6qULsWu07nKEsrSoI4a/view?usp=sharing'},
  {name: '36Archanai 108', url: 'https://drive.google.com/file/d/1OoDVdgH4-c6Zo1H_M9LJqSn1xt55afHo/view?usp=sharing'},
  {name: '37Ashtothiram', url: 'https://drive.google.com/file/d/19U36vlayxmjqkimA7HBmKqdJRTqpaZau/view?usp=sharing'},
  {name: '38ThevaramThunsalil', url: 'https://drive.google.com/file/d/1OHlEtKSkW5NuhkMU9wZOxWyv9_s2-Bta/view?usp=sharing'},
  {name: '39KummiThirumagal', url: 'https://drive.google.com/file/d/1aPyn1oH5ez3cKDAo7kSsihlTj9vgHMcf/view?usp=sharing'},
  {name: '40KolattamAlaiKadal', url: 'https://drive.google.com/file/d/1kJq5NT54jvM_5KjYbUkewfAAi7BNh5hi/view?usp=sharing'},
  {name: '41Athlasedanarada', url: 'https://drive.google.com/file/d/1z1Uc6-0fCQdIrv-FZ4oOebBEeU-uPcp3/view?usp=sharing'},
  {name: '42Manthirangal', url: 'https://drive.google.com/file/d/11Rwj4z9K7dNw4oEWulMeTzI6lzTs-kj5/view?usp=sharing'},
  {name: '43ErumayilMadyamavathi', url: 'https://drive.google.com/file/d/1C_aoJY3P9Jyho_gS386HgBQPsUB4VSoH/view?usp=sharing'},
  {name: '44ArulRepeatVersion', url: 'https://drive.google.com/file/d/1LoUHX7QKCMQg1GJqPb_n5lvI7kUPZpTH/view?usp=sharing'},
];

@Component({
  selector: 'app-valli-kalyanam',
  templateUrl: './valli-kalyanam.component.html',
  styleUrls: ['./valli-kalyanam.component.css']
})
export class ValliKalyanamComponent implements OnInit {

  valliKalyanam: DownloadTable[] = VALLI_KALTANAM;

  constructor(private store: Store<fromWelcome.State>, private route: ActivatedRoute) {
  }

  ngOnInit() {
    this.route.data.subscribe(
      (data: Data) => {
        this.store.dispatch(new welcomeActions.ChangeTitle(data.title));
      }
    );
  }
}
