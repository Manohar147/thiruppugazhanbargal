import {Component, OnInit} from '@angular/core';
import {DownloadTable} from '../../table/download-table/download-table.component';
import {ActivatedRoute, Data} from '@angular/router';
import * as welcomeActions from '../../store/welcome.actions';
import {Store} from '@ngrx/store';
import * as fromWelcome from '../../store/welcome.reducer';

const VIRUTHAM_DATA: DownloadTable[] = [
  {name: '001 Kaithala with Viruththam', url: 'https://drive.google.com/file/d/1OnnBlWfE89rLQ1TZOjSCdpITyHFvT-9r/view?usp=sharing'},
  {name: '001A_Vinaayagar Viruththam', url: 'https://drive.google.com/file/d/1SMhZvPokpW3P-rxbN2bXxn1ZbA55H0iy/view?usp=sharing'},
  {name: '003A_Agaval Viruththam  in AnandaBhairavi', url: 'https://drive.google.com/file/d/1mP3dGq9dDfOmXq-VUHHORJ0UdcqI01X7/view?usp=sharing'},
  {name: '003A_Viruththam  Agaval in Anantha Bhairavi', url: 'https://drive.google.com/file/d/1mP3dGq9dDfOmXq-VUHHORJ0UdcqI01X7/view?usp=sharing'},
  {name: '003Umbar tharu with virutham Sithakalaba', url: 'https://drive.google.com/file/d/1mP3dGq9dDfOmXq-VUHHORJ0UdcqI01X7/view?usp=sharing'},
  {name: '005A_Antham Viruththam', url: 'https://drive.google.com/file/d/12rfSOjzBaOsTHhmnm5JrbX4sTIct87Sv/view?usp=sharing'},
  {name: '005A_Viruththam Aruvamum', url: 'https://drive.google.com/file/d/1eJyPioxxOllAjUrhC5k_mgW9RMdu6w9Y/view?usp=sharing'},
  {name: '007_Kanagam with Viruththam  Sankarabaranam', url: 'https://drive.google.com/file/d/1o7fkl6TTmiPzWK2F32kC95oRgsXNsK6y/view?usp=sharing'},
  {name: '007Kanakam with virutham', url: 'https://drive.google.com/file/d/1nU7U4Om0XP27emC6gbXk3sGMZ3JE1ms9/view?usp=sharing'},
  {name: '007Karuvadainthu with Varaali Viruththam_Aruvamum', url: 'https://drive.google.com/file/d/1w_Lex-1o1FNbOGMLRzzTi0XKvFm15p7G/view?usp=sharing'},
  {name: '007Karuvadainthu with Varali Viruththam', url: 'https://drive.google.com/file/d/18GdeGBIfzdTOKsmW7UBkFmhLr2b7MHNI/view?usp=sharing'},
  {name: '008A_Anthathi Virutham Sankaraabaranam', url: 'https://drive.google.com/file/d/1opibIrsnBZqxfTsN9I5Ez0IuS-v7MyzM/view?usp=sharing'},
  {name: '008A_ViruththamNeelambari', url: 'https://drive.google.com/file/d/1MBhyHEfr0QqS60BKt8Kx6GxOEdh5Z_ez/view?usp=sharing'},
  {name: '009A_Ainthu buthamum Virutham', url: 'https://drive.google.com/file/d/1C1cKA5xqcRe97Qh2zTd1TvYZnsOJycyn/view?usp=sharing'},
  {name: '009AinthuButhamum Viruththam in Hindolam', url: 'https://drive.google.com/file/d/1C1cKA5xqcRe97Qh2zTd1TvYZnsOJycyn/view?usp=sharing'},
  {name: '021A_Maanikkame Viruththam', url: 'https://drive.google.com/file/d/1Zh5qb2IUPE_5xV6a0PI5X3yRxloQ9_-_/view?usp=sharing'},
  {name: '024Thandai with ViruththamThiruvadiyum', url: 'https://drive.google.com/file/d/1Hm6BLLOsw98Nvz3nBk6HKN_o2I2o2iSp/view?usp=sharing'},
  {name: '029Nalumainthu with Virutham Velpattu', url: 'https://drive.google.com/file/d/1nkw-RByd_ayKSMKmhmn8s0zL4EFx75Vv/view?usp=sharing'},
  {name: '033Padarpuvi Viruththam Sethanam', url: 'https://drive.google.com/file/d/1FSYJuD4T5w1K59nOZZYovZtT3ttqklvC/view?usp=sharing'},
  {name: '033Padarpuvi with virutham sethu', url: 'https://drive.google.com/file/d/12bHkyC8_2NY66tKaD-ldSyr8ROiSDju5/view?usp=sharing'},
  {name: '037A_Viruththam_Senthur Karuthu', url: 'https://drive.google.com/file/d/1ufcSA-5qcXgDkPzUzTDHd5jH6dGKGPl5/view?usp=sharing'},
  {name: '037Manaththin with Viruththam_senthurkaruthu', url: 'https://drive.google.com/file/d/1xe2AOkbjsoLvmiX1QbWQfnyQn_BLk2lR/view?usp=sharing'},
  {name: '041A_Virutham Maanikkame', url: 'https://drive.google.com/file/d/1rpJsxMMqVcP8NlLGXj8WN72OAZ-gXBxk/view?usp=sharing'},
  {name: '041Vanjam with Viruththam_Senthurkaruthu', url: 'https://drive.google.com/file/d/19CVVuQkCO_64-3c7ndTaOIBNKQHwNs0c/view?usp=sharing'},
  {name: '045Vinthathin with Niraval', url: 'https://drive.google.com/file/d/1RpNW4yOemmHZeVfU6Q2qj4PoefR_0Q5s/view?usp=sharing'},
  {name: '050A_Virutham in Peruthargu', url: 'https://drive.google.com/file/d/1nCYQk9K54gFDtek6fxTpGHI1pFOtUT4N/view?usp=sharing'},
  {name: '050A_Virutham Padikintrilai in Nattakurunji', url: 'https://drive.google.com/file/d/1CJAqefHI0ipWfIABboThA5uVU4du2Z2b/view?usp=sharing'},
  {name: '050A_Virutham Padikintrilai in Nattakurunji', url: 'https://drive.google.com/file/d/1CJAqefHI0ipWfIABboThA5uVU4du2Z2b/view?usp=sharing'},
  {name: '054A_Gathithanai Viruththam in Danyasi', url: 'https://drive.google.com/file/d/1YezDB3vQEhDBw9x7B8PDFpozIPJUJYHc/view?usp=sharing'},
  {name: '063Oruvarai with Viruththam_Padikkintrilai', url: 'https://drive.google.com/file/d/1YezDB3vQEhDBw9x7B8PDFpozIPJUJYHc/view?usp=sharing'},
  {name: '076viruttham and vasanamiga', url: 'https://drive.google.com/file/d/1zBxS0UYUWZOoNL09HpXvGyF5-Cvg6hur/view?usp=sharing'},
  {name: '079Aanaatha with Viruththam  Maivarum', url: 'https://drive.google.com/file/d/1BSQvO3d_G6oxDlU8O3Cl9Ugcg-7bAGNS/view?usp=sharing'},
  {name: '079Anaatha with virutham maloon', url: 'https://drive.google.com/file/d/1NTBq7w-G9eYp6HCt5z4friB1JjGiYCxC/view?usp=sharing'},
  {name: '085A_Maivarum Viruththam in YamunaKalyaani', url: 'https://drive.google.com/file/d/1svdBpEyClO9gLYuM17KE0bpEMRW9O0VA/view?usp=sharing'},
  {name: '086Kumaragurupara with Viruththam  Maivarum', url: 'https://drive.google.com/file/d/1miiyoqI1be5gpIGj0aCb9RlY7M7jZEle/view?usp=sharing'},
  {name: '088Sarana with Virutham  Agama', url: 'https://drive.google.com/file/d/1CRdBlO4zXMprrL-GczbVVih0XDI3UKJl/view?usp=sharing'},
  {name: '096Oruvarai with Vituththam_seeramkaraa', url: 'https://drive.google.com/file/d/1BXa6zRTSZnSW7LFK8hUeB0jHZdBUnX-i/view?usp=sharing'},
  {name: '100Yiruppaval with Virutham Aavikku', url: 'https://drive.google.com/file/d/1Cuz2F0SNGhVHP5QIbHkgKAYeOpUnKUiz/view?usp=sharing'},
  {name: '106A_Maalon viruththam', url: 'https://drive.google.com/file/d/1VTabBce3n3UwniHCPNun9OAabWgNh9j7/view?usp=sharing'},
  {name: '113Thuppaarappa with viruththam sintha', url: 'https://drive.google.com/file/d/114H0y3XuMxf8bwBw3Nc20QDablsVkv1B/view?usp=sharing'},
  {name: '113Thuppaarappa with viruththam sintha', url: 'https://drive.google.com/file/d/1At9Cl11tjsKm9qTfE4PmMj3c5YG6kol-/view?usp=sharing'},
  {name: '137A_Viruththam_ Maaloon', url: 'https://drive.google.com/file/d/13u2pIB03mScEfKcbprjqAj8JuyGH01ZX/view?usp=sharing'},
  {name: '137B_Vanjakaloba_ Virutham_Maloon_Contd', url: 'https://drive.google.com/file/d/16cXq6q9A6FkISB3yXFeRbf4vqLOiaqMj/view?usp=sharing'},
  {name: '160Aiyumuru with viruththam Seeram kara', url: 'https://drive.google.com/file/d/1c75oOPZRl7UwXnehM7CPVgdo9yNSiV0U/view?usp=sharing'},
  {name: '183A_Viruththam Velayuthan', url: 'https://drive.google.com/file/d/1DsNJRX1BrIU93IvegMjikwUqVloJUmA0/view?usp=sharing'},
  {name: '227A_Neelasikandiyil', url: 'https://drive.google.com/file/d/1IHB_uoCyBjuTFIadnTUPPFChnSdT7WNQ/view?usp=sharing'},
  {name: '233A_Gathithanai virutham', url: 'https://drive.google.com/file/d/1APB3dC1eM1nLrhLsQvykrZdHK3RvWSku/view?usp=sharing'},
  {name: '363Seritharum  with Virutham Thoolaal', url: 'https://drive.google.com/file/d/198s870u_1pHOrfVTR8NSFIU9nq4UpF03/view?usp=sharing'},
  {name: '373Kuruthipulaal with Viruththam_Gnanamuthalvi', url: 'https://drive.google.com/file/d/198s870u_1pHOrfVTR8NSFIU9nq4UpF03/view?usp=sharing'},
  {name: '373Kuruthipulaal with Viruththam_Gnanamuthalvi', url: 'https://drive.google.com/file/d/1p1rMkXqzeubS1RV1uxEfy8JW23gvRGKr/view?usp=sharing'},
  {name: '400Yelupputhol with Viruththam_Perutharkariya', url: 'https://drive.google.com/file/d/1p1rMkXqzeubS1RV1uxEfy8JW23gvRGKr/view?usp=sharing'},
  {name: '422 Thalai mayir with virutham Perutharkariya', url: 'https://drive.google.com/file/d/1ZDM17COPuzP6yYFD0MIMm0RbP4dUo8p5/view?usp=sharing'},
];


@Component({
  selector: 'app-virutham',
  templateUrl: './virutham.component.html',
  styleUrls: ['./virutham.component.css']
})
export class ViruthamComponent implements OnInit {

  public viruthamData: DownloadTable[] = VIRUTHAM_DATA;


  constructor(private store: Store<fromWelcome.State>, private route: ActivatedRoute) {
  }

  ngOnInit() {
    this.route.data.subscribe(
      (data: Data) => {
        this.store.dispatch(new welcomeActions.ChangeTitle(data.title));
      }
    );
  }

}
