import {Component, Inject, OnInit} from '@angular/core';
import {MatBottomSheetRef} from '@angular/material/bottom-sheet';
import {Events} from '../../body/major-events-and-office-bearers/major-events/major-events.component';
import {MAT_BOTTOM_SHEET_DATA} from '@angular/material/bottom-sheet';


@Component({
  selector: 'app-event-bottom-sheet',
  templateUrl: './event-bottom-sheet.component.html',
  styleUrls: ['./event-bottom-sheet.component.css']
})
export class EventBottomSheetComponent implements OnInit {

  event: Events;

  constructor(private _bottomSheetRef: MatBottomSheetRef<EventBottomSheetComponent>
    , @Inject(MAT_BOTTOM_SHEET_DATA) public data: Events) {
    this.event = data;
  }

  ngOnInit() {
  }

  closeBottomSheet() {
    this._bottomSheetRef.dismiss();
  }

}
