import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {catchError, first, map} from 'rxjs/operators';
import {AngularFirestore} from '@angular/fire/firestore';
import {of} from 'rxjs';
import PlayList from './playlist';
import Song from './song';

export interface RetriveSong {
  id: string,
  song: Song
}

@Injectable()
export class DataService {

  constructor(private httpClient: HttpClient, private firestore: AngularFirestore) {
  }

  getSongsData() {
    return this.firestore.collection('songs', ref => ref.orderBy('newNumber')).valueChanges().pipe(
      first(),
      catchError(err => of('Access Denied! : ', err))
    );
  }

  saveUserPlaylist(uid: string, playList: PlayList, eList: PlayList[]) {
    return this.firestore.collection('users').doc(uid).update({
      playList: [...eList, playList]
    });
  }

  getPhoneNumber() {
    return this.firestore.collection('phoneNumbers').valueChanges().pipe(
      first(),
      catchError(err => of('Access Denied! : ', err))
    );
  }

  deleteUserPlaylist(uid: string, playList: PlayList, eList: PlayList[], index: number) {
    if (index > -1) {
      eList.splice(index, 1);
    }
    return this.firestore.collection('users').doc(uid).update({
      playList: [...eList]
    });
  }

  getSongDetails(songNumber: number) {
    return this.firestore.collection(
      'songs',
      ref => ref.where('newNumber', '==', songNumber)
    ).snapshotChanges().pipe(
      first(),
      map(response => {
        return {
          id: response[0].payload.doc.id,
          song: response[0].payload.doc.data()
        };
      })
    );
  }


  UpdateSongDetails(data: RetriveSong) {
    return this.firestore.collection('songs').doc(data.id).update({
      ...data.song
    });
  }
}
