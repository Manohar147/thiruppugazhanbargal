import Song from './song';

export default interface PlayList {
  playListName: string;
  playList: Song[];
  description: string;
}
