export default class Song {
  songName: string;
  songUrl: string;
  newNumber: number;
  oldNumber: number;
  classify1: string;
  classify2: string;
  classify3: string;
  meaning: string;
  ragam: string;
  tamilMeaning: string;
  tamilMeaningUrl: string;
  thalam: string;
  thalamUrl: string;
  isChecked = false;
}
