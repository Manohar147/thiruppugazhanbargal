import {Component, Inject, OnInit} from '@angular/core';
import {ErrorStateMatcher, MAT_DIALOG_DATA, MatDialogRef, MatSnackBar} from '@angular/material';
import {RetriveSong} from '../../data/data.service';
import {FormControl, FormGroup, FormGroupDirective, NgForm, Validators} from '@angular/forms';
import Song from '../../data/song';
import {Store} from '@ngrx/store';
import * as fromWelcome from '../../store/welcome.reducer';
import * as welcomeActions from '../../store/welcome.actions';

export class MyErrorStateMatcher implements ErrorStateMatcher {
  isErrorState(control: FormControl | null, form: FormGroupDirective | NgForm | null): boolean {
    const isSubmitted = form && form.submitted;
    return !!(control && control.invalid && (control.dirty || control.touched || isSubmitted));
  }
}

@Component({
  selector: 'app-admin-song-update-dialog',
  templateUrl: './admin-song-update-dialog.component.html',
  styleUrls: ['./admin-song-update-dialog.component.css']
})
export class AdminSongUpdateDialogComponent implements OnInit {

  songDetails: RetriveSong;
  songsUpdateForm: FormGroup;
  matcher = new MyErrorStateMatcher();

  constructor(public dialogRef: MatDialogRef<AdminSongUpdateDialogComponent>,
              @Inject(MAT_DIALOG_DATA) public data: RetriveSong, private _snackBar: MatSnackBar,
              private store: Store<fromWelcome.State>) {
  }

  ngOnInit() {
    this.songDetails = this.data;
    this.songsUpdateForm = new FormGroup({
      thalam: new FormControl(this.songDetails.song.thalam, Validators.required),
      thalamLink: new FormControl(this.songDetails.song.thalamUrl, Validators.required),
      tamilMeaning: new FormControl(this.songDetails.song.tamilMeaning, Validators.required),
      tamilMeaningLink: new FormControl(this.songDetails.song.tamilMeaningUrl, Validators.required),
    });
  }

  onNoClick(): void {
    this.dialogRef.close();
  }

  onSubmit() {
    if (this.songsUpdateForm.valid) {
      const song: Song = {
        ...this.songDetails.song,
        thalam: this.songsUpdateForm.value.thalam,
        thalamUrl: this.songsUpdateForm.value.thalamLink,
        tamilMeaning: this.songsUpdateForm.value.tamilMeaning,
        tamilMeaningUrl: this.songsUpdateForm.value.tamilMeaningLink
      };
      this.store.dispatch(new welcomeActions.UpdateSongDetails(
        {
          id: this.songDetails.id,
          song: song
        }
      ));
      this.dialogRef.close();
    } else {
      this._snackBar.open(`Please enter valid details`, null, {
        duration: 2000
      });
    }
  }

}
