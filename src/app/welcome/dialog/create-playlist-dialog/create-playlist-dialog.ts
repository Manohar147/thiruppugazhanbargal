import {AfterViewInit, Component, Inject, OnInit, ViewChild} from '@angular/core';
import {MatDialogRef, MAT_DIALOG_DATA} from '@angular/material/dialog';
import {DialogData} from '../../body/songs-with-script/songs-with-script.component';
import {MatPaginator, MatTableDataSource} from '@angular/material';
import Song from '../../data/song';
import {Store} from '@ngrx/store';
import * as fromWelcome from '../../store/welcome.reducer';
import {FormControl, FormGroup, FormGroupDirective, NgForm, Validators} from '@angular/forms';
import {Router} from '@angular/router';
import {ErrorStateMatcher} from '@angular/material/core';
import * as welcomeActions from '../../store/welcome.actions';


export class MyErrorStateMatcher implements ErrorStateMatcher {
  isErrorState(control: FormControl | null, form: FormGroupDirective | NgForm | null): boolean {
    const isSubmitted = form && form.submitted;
    return !!(control && control.invalid && (control.dirty || control.touched || isSubmitted));
  }
}

@Component({
  selector: 'app-custom-dialog',
  templateUrl: './create-playlist-dialog.html',
  styleUrls: ['./create-playlist-dialog.css']
})
export class CreatePlaylistDialog implements OnInit, AfterViewInit {
  @ViewChild(MatPaginator) paginator: MatPaginator;
  displayedColumns: string[] = ['song name', 'new number', 'old number', 'ragam', 'thalam', 'tamil meaning', 'meaning', 'classify1', 'classify2', 'classify3'];
  dataSource: MatTableDataSource<Song>;
  playlistForm: FormGroup;
  matcher = new MyErrorStateMatcher();

  constructor(
    public dialogRef: MatDialogRef<CreatePlaylistDialog>,
    @Inject(MAT_DIALOG_DATA) public data: DialogData, private store: Store<fromWelcome.State>, private router: Router) {
    this.dataSource = new MatTableDataSource<Song>(this.data.playlist);
  }

  onNoClick(): void {
    this.dialogRef.close();
  }


  removeRow(row: Song) {
    this.dataSource = new MatTableDataSource<Song>([...this.dataSource.data.filter(song => {
      return song.newNumber != row.newNumber;
    })]);
    this.dataSource.paginator = this.paginator;
  }

  ngAfterViewInit(): void {
    this.dataSource.paginator = this.paginator;
  }

  ngOnInit() {
    this.store.select('welcome');
    this.playlistForm = new FormGroup({
      playListName: new FormControl({value: '', disabled: this.dataSource.data.length === 0}, Validators.required),
      description: new FormControl({value: '', disabled: this.dataSource.data.length === 0}, Validators.required),
    });
  }

  onSubmit() {
    if (this.playlistForm.valid) {
      this.store.dispatch(new welcomeActions.SaveUserPlayList({
        playList: this.dataSource.data,
        playListName: this.playlistForm.value.playListName,
        description: this.playlistForm.value.description
      }));
      this.router.navigate(['welcome', 'SongsWithScript'], {queryParams: {showPlaylist: 1}}).catch(
        error => console.error('Routing Error : ', error)
      );
      this.dialogRef.close();
    }
  }

}
