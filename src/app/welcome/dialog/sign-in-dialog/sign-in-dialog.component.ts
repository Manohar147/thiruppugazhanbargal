import {Component, Inject, OnDestroy, OnInit} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material';
import {Store} from '@ngrx/store';
import * as fromWelcome from '../../store/welcome.reducer';
import {Router} from '@angular/router';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {MyErrorStateMatcher} from '../../../user-auth/user-authentication/sign-in/sign-in.component';
import * as firebaseUi from 'firebaseui';
import * as firebase from 'firebase/app';
import * as fromAuth from '../../../user-auth/auth-store/auth.reducer';
import * as AuthActions from '../../../user-auth/auth-store/auth.actions';
import {AngularFireAuth} from '@angular/fire/auth';
import {UserClass} from '../../../user-auth/ThirupUser/user';

@Component({
  selector: 'app-sign-in-dialog',
  templateUrl: './sign-in-dialog.component.html',
  styleUrls: ['./sign-in-dialog.component.css']
})
export class SignInDialogComponent implements OnInit, OnDestroy {

  signInForm: FormGroup;
  ui: firebaseUi.auth.AuthUI;
  matcher = new MyErrorStateMatcher();
  passIconColor: string = '';
  route: string;

  constructor(
    public dialogRef: MatDialogRef<SignInDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data: { route: string, queryParams: Object },
    private authStore: Store<fromAuth.State>,
    private store: Store<fromWelcome.State>, private router: Router,
    private afAuth: AngularFireAuth) {
  }

  ngOnInit() {
    this.signInForm = new FormGroup({
      email: new FormControl(null, [Validators.required, Validators.email]),
      password: new FormControl(null, [Validators.required])
    });
    const uiConfig = {
      signInOptions: [
        firebase.auth.GoogleAuthProvider.PROVIDER_ID
      ],
      callbacks: {
        signInSuccessWithAuthResult: this.onLoginSuccess.bind(this)
      }
    };
    this.ui = new firebaseUi.auth.AuthUI(this.afAuth.auth);
    this.ui.start('#firebaseui-auth-container', uiConfig);

    this.authStore.select('auth').subscribe(
      (authData: fromAuth.State) => {
        if (authData.isLoggedIn) {
          this.dialogRef.close();
        }
      }
    );
  }

  onLoginSuccess(result) {
    if (result.additionalUserInfo.isNewUser) {
      this.store.dispatch(new AuthActions.CreateUserLibrary(new UserClass(result.user.uid, false, [], result.user.displayName, result.user.email, result.user.phoneNumber, null, result.user.photoURL)));
    } else {
      this.store.dispatch(new AuthActions.TrySyncUser(result.user.uid));
    }
    this.store.dispatch(new AuthActions.GoogleSignIn({
      result: result,
      route: this.data.route, queryParams: this.data.queryParams
    }));
  }

  onSubmit() {
    if (this.signInForm.valid) {
      this.store.dispatch(new AuthActions.TrySignIn({
        ...this.signInForm.value,
        route: this.data.route, queryParams: this.data.queryParams
      }));
    }
  }

  ngOnDestroy(): void {
    this.ui.delete().catch(error => console.error('Error in Sign-in-component OnDestroy() Error : ', error));
  }

  onNoClick(): void {
    this.dialogRef.close();
  }

  togglePassword(password, type) {
    password.setAttribute('type', type);
    if (type === 'text') {
      this.passIconColor = 'primary';
    } else {
      this.passIconColor = '';
    }
  }


}
