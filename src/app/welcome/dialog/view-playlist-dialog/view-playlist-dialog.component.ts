import {AfterViewInit, Component, Inject, OnInit, ViewChild} from '@angular/core';
import {MatDialogRef, MAT_DIALOG_DATA} from '@angular/material/dialog';
import {MatPaginator, MatTableDataSource} from '@angular/material';
import Song from '../../data/song';
import PlayList from '../../data/playlist';


@Component({
  selector: 'app-view-playlist-dialog',
  templateUrl: './view-playlist-dialog.component.html',
  styleUrls: ['./view-playlist-dialog.component.css']
})
export class ViewPlaylistDialogComponent implements OnInit, AfterViewInit {
  @ViewChild(MatPaginator) paginator: MatPaginator;
  displayedColumns: string[] = ['song name', 'new number', 'old number', 'ragam', 'thalam', 'tamil meaning', 'meaning', 'classify1', 'classify2', 'classify3'];
  dataSource: MatTableDataSource<Song>;

  constructor(
    public dialogRef: MatDialogRef<ViewPlaylistDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data: PlayList) {
    this.dataSource = new MatTableDataSource<Song>(this.data.playList);
  }

  onNoClick(): void {
    this.dialogRef.close();
  }

  ngAfterViewInit(): void {
    this.dataSource.paginator = this.paginator;
  }

  ngOnInit() {
  }

}
