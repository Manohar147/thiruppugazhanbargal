import {Component, Inject, OnInit} from '@angular/core';
import {MAT_SNACK_BAR_DATA, MatDialog} from '@angular/material';
import {SignInDialogComponent} from '../../dialog/sign-in-dialog/sign-in-dialog.component';

@Component({
  selector: 'app-login-error-snackbar',
  templateUrl: './login-error-snackbar.component.html',
  styleUrls: ['./login-error-snackbar.component.css']
})
export class LoginErrorSnackbarComponent implements OnInit {

  constructor(@Inject(MAT_SNACK_BAR_DATA) public data: { message: string, route: string, queryParams: Object }, public dialog: MatDialog) {
  }

  ngOnInit() {
  }

  showLoginDialog() {
    this.dialog.open(SignInDialogComponent, {
      data: {
        route: this.data.route,
        queryParams: this.data.queryParams
      }
    });
  }

}
