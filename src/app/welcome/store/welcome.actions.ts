import {Action} from '@ngrx/store';
import Song from '../data/song';
import PlayList from '../data/playlist';
import {phoneNumber} from '../data/phoneNumber';
import {RetriveSong} from '../data/data.service';

export const CHANGE_TITLE = 'CHANGE_TITLE';
export const GET_SONG_DATA = 'GET_SONG_DATA';
export const SET_SONG_DATA = 'SET_SONG_DATA';
export const SAVE_USER_PLAYLIST = 'SAVE_USER_PLAYLIST';
export const GET_PHONE_NUMBER = 'GET_PHONE_NUMBER';
export const SET_PHONE_NUMBER = 'SET_PHONE_NUMBER';
export const DELETE_USER_PLAYLIST = 'DELETE_USER_PLAYLIST';
export const UPDATE_SONG_DETAILS = 'UPDATE_SONG_DETAILS';


export class ChangeTitle implements Action {
  readonly type = CHANGE_TITLE;

  constructor(public payload: string) {
  }
}

export class GetSongData implements Action {
  readonly type = GET_SONG_DATA;
}

export class SetSongData implements Action {
  readonly type = SET_SONG_DATA;

  constructor(public payload: Song[]) {
  }
}

export class SaveUserPlayList implements Action {
  readonly type = SAVE_USER_PLAYLIST;

  constructor(public payload: PlayList) {
  }
}

export class DeleteUserPlaylist implements Action {
  readonly type = DELETE_USER_PLAYLIST;

  constructor(public payload: { playlist: PlayList, index: number }) {
  }
}


export class GetPhoneNumber implements Action {
  readonly type = GET_PHONE_NUMBER;
}

export class SetPhoneNumber implements Action {
  readonly type = SET_PHONE_NUMBER;

  constructor(public payload: phoneNumber[]) {
  }
}

export class UpdateSongDetails implements Action {
  readonly type = UPDATE_SONG_DETAILS;

  constructor(public payload: RetriveSong) {
  }
}


export  type WelcomeActions =
  ChangeTitle
  | GetSongData
  | SetSongData
  | SaveUserPlayList
  | GetPhoneNumber
  | SetPhoneNumber
  | DeleteUserPlaylist
  | UpdateSongDetails;
