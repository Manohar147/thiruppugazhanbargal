import {Effect, Actions, ofType} from '@ngrx/effects';
import {catchError, first, map, mergeMap, switchMap} from 'rxjs/operators';
import * as WelcomeActions from '../store/welcome.actions';
import {DataService, RetriveSong} from '../data/data.service';
import {Injectable} from '@angular/core';
import PlayList from '../data/playlist';
import {from, throwError} from 'rxjs';
import {Store} from '@ngrx/store';
import * as fromAuth from '../../user-auth/auth-store/auth.reducer';
import * as ToastActions from '../../toaster/store/toaster.actions';
import {phoneNumber} from '../data/phoneNumber';

@Injectable()
export class WelcomeEffects {

  constructor(private actions$: Actions, private dataService: DataService, private store: Store<fromAuth.State>) {
  }

  @Effect()
  getSongData = this.actions$.pipe(
    ofType(WelcomeActions.GET_SONG_DATA),
    switchMap(() => {
      return this.dataService.getSongsData().pipe(
        catchError(err => throwError(err))
      );
    }),
    mergeMap((data: any) => {
      return [
        {
          type: WelcomeActions.SET_SONG_DATA,
          payload: data
        }
      ];
    })
  );

  @Effect()
  saveUserPlayList = this.actions$.pipe(
    ofType(WelcomeActions.SAVE_USER_PLAYLIST),
    map((action: WelcomeActions.SaveUserPlayList) => {
      return action.payload;
    }),
    switchMap((data: PlayList) => {
      return this.store.select('auth').pipe(
        first(),
        map((authData: fromAuth.State) => {
          let playlist = [];
          if (authData.userLibrary && authData.userLibrary.playList) {
            playlist = authData.userLibrary.playList;
          }
          return {
            uid: authData.user.uid,
            existingList: playlist,
            playList: data
          };
        })
      );
    }),
    switchMap((response: { uid: string, playList: PlayList, existingList: PlayList[] }) => {
      return from(this.dataService.saveUserPlaylist(response.uid, response.playList, response.existingList).then(
        () => true
        , () => false
      )).pipe(
        catchError(err => throwError(err))
      );
    }),
    mergeMap((response: boolean) => {
      const actions = [];
      if (response) {
        actions.push(
          {
            type: ToastActions.TRIGGER_TOAST,
            payload: {status: 'SUCCESS', message: 'Playlist saved successfully', title: 'Success'}
          }
        );
      } else {
        actions.push(
          {
            type: ToastActions.TRIGGER_TOAST,
            payload: {status: 'FAILED', message: 'Failed to save playlist, Please try again later', title: 'Failed'}
          }
        );
      }
      return actions;
    })
  );

  @Effect()
  getPhoneNumber = this.actions$.pipe(
    ofType(WelcomeActions.GET_PHONE_NUMBER),
    switchMap(() => {
      return this.dataService.getPhoneNumber().pipe(
        catchError(err => throwError(err))
      );
    }),
    mergeMap((data: phoneNumber[]) => {
      return [
        {
          type: WelcomeActions.SET_PHONE_NUMBER,
          payload: data
        }
      ];
    })
  );

  @Effect()
  deleteUserPlaylist = this.actions$.pipe(
    ofType(WelcomeActions.DELETE_USER_PLAYLIST),
    map((action: WelcomeActions.SaveUserPlayList) => {
      return action.payload;
    }),
    switchMap((data: any) => {
      return this.store.select('auth').pipe(
        first(),
        map((authData: fromAuth.State) => {
          return {
            uid: authData.user.uid,
            existingList: authData.userLibrary.playList ? authData.userLibrary.playList : [],
            data: data
          };
        })
      );
    }),
    switchMap((response: { uid: string, data: { playlist: PlayList, index: number }, existingList: PlayList[] }) => {
      return from(this.dataService.deleteUserPlaylist(response.uid, {...response.data.playlist},
        [...response.existingList], response.data.index).then(
        () => true
        , () => false
      )).pipe(
        catchError(err => throwError(err))
      );
    }),
    mergeMap((response: boolean) => {
      const actions = [];
      if (response) {
        actions.push(
          {
            type: ToastActions.TRIGGER_TOAST,
            payload: {status: 'SUCCESS', message: 'Playlist deleted successfully', title: 'Success'}
          }
        );
      } else {
        actions.push(
          {
            type: ToastActions.TRIGGER_TOAST,
            payload: {status: 'FAILED', message: 'Failed to delete playlist, Please try again later', title: 'Failed'}
          }
        );
      }
      return actions;
    })
  );

  @Effect()
  updateSongDetails = this.actions$.pipe(
    ofType(WelcomeActions.UPDATE_SONG_DETAILS),
    map((action: WelcomeActions.UpdateSongDetails) => action.payload),
    switchMap((data: RetriveSong) => {
      return from(this.dataService.UpdateSongDetails(data)
        .then(
          () => true,
        ).catch(
          error => {
            console.error(error);
            return false;
          }
        ),
      );
    }),
    mergeMap((response: boolean) => {
        console.debug('Update Status : ' + response ? 'success' : 'failed');
        const actions = [];
        if (response) {
          actions.push(
            {
              type: ToastActions.TRIGGER_TOAST,
              payload: {status: 'SUCCESS', message: 'Song updated successfully, Please reload the page to see changes', title: 'Success'}
            }
          );
        } else {
          actions.push(
            {
              type: ToastActions.TRIGGER_TOAST,
              payload: {status: 'FAILED', message: 'Failed to update Song', title: 'Failed'}
            }
          );
        }
        return actions;
      }
    )
  );
}
