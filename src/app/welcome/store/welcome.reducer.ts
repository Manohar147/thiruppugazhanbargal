import * as welcomeActions from './welcome.actions';
import Song from '../data/song';
import {phoneNumber} from '../data/phoneNumber';

export interface State {
  title: string;
  songs: Song[];
  selectedSongs: Song[];
  phoneNumber: phoneNumber[];

}

const initialState: State = {
  title: 'Welcome',
  songs: [],
  selectedSongs: [],
  phoneNumber: []
};

export function welcomeReducer(state = initialState, action: welcomeActions.WelcomeActions) {

  switch (action.type) {
    case welcomeActions.CHANGE_TITLE:
      return {
        ...state,
        title: action.payload
      };
    case welcomeActions.SET_SONG_DATA:
      return {
        ...state,
        songs: action.payload
      };
    case welcomeActions.SET_PHONE_NUMBER:
      return {
        ...state,
        phoneNumber: action.payload
      };
    default:
      return {
        ...state
      };
  }

}
