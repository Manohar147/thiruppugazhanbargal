import {Component, Input, OnInit} from '@angular/core';
import {MatTableDataSource} from '@angular/material';


export interface DownloadTable {
  name: string;
  url: string;
}


@Component({
  selector: 'app-download-table',
  templateUrl: './download-table.component.html',
  styleUrls: ['./download-table.component.css']
})
export class DownloadTableComponent implements OnInit {
  displayedColumns: string[] = ['index', 'song', 'download'];
  @Input('table') dataTable: DownloadTable[];
  dataSource: MatTableDataSource<DownloadTable>;

  constructor() {
  }

  applyFilter(filterValue: string) {
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }

  ngOnInit() {
    this.dataSource = new MatTableDataSource(this.dataTable);
  }

  openLink(url) {
    window.open(url, '_blank');
  }

}
