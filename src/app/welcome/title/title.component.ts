import {Component, Input, OnInit} from '@angular/core';
import {Store} from '@ngrx/store';
import * as fromWelcome from '../store/welcome.reducer';
import * as welcomeActions from '../store/welcome.actions';
import {Observable} from 'rxjs';

@Component({
  selector: 'app-title',
  templateUrl: './title.component.html',
  styleUrls: ['./title.component.css']
})
export class TitleComponent implements OnInit {

  @Input() welcomeState: Observable<fromWelcome.State>;

  constructor(private store: Store<fromWelcome.State>) {
  }

  ngOnInit() {
    this.welcomeState = this.store.select('welcome');
  }

}
