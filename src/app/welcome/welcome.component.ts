import {Component, ElementRef, OnInit, ViewChild} from '@angular/core';
import {Store} from '@ngrx/store';
import * as fromWelcome from './store/welcome.reducer';
import * as WelcomeActions from './store/welcome.actions';

import {Observable} from 'rxjs';
import {Router} from '@angular/router';

@Component({
  selector: 'app-welcome',
  templateUrl: './welcome.component.html',
  styleUrls: ['./welcome.component.css']
})
export class WelcomeComponent implements OnInit {

  authStore: Observable<fromWelcome.State>;
  title: string;
  @ViewChild('scroll') scrollHere: ElementRef;

  constructor(private store: Store<fromWelcome.State>, private router: Router) {
  }

  ngOnInit() {
    this.authStore = this.store.select('welcome');
    this.store.dispatch(new WelcomeActions.GetSongData());

    this.store.dispatch(new WelcomeActions.GetPhoneNumber());
    this.router.events.subscribe(() => {
      window.scrollTo({
        top: this.scrollHere.nativeElement.offsetTop + (window.screen.width < 768 ? 15 : 20),
        left: this.scrollHere.nativeElement.offsetLeft,
        behavior: 'smooth'
      });
    });

  }

}
