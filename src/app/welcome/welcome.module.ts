import {NgModule} from '@angular/core';
import {WelcomeComponent} from './welcome.component';
import {RouterModule, Routes} from '@angular/router';
import {CommonModule} from '@angular/common';
import {TitleComponent} from './title/title.component';
import {AboutGurujiComponent} from './body/about-guruji/about-guruji.component';
import {PageResolveGuardService} from '../guards/page-resolve-guard.service';
import {StoreModule} from '@ngrx/store';
import {welcomeReducer} from './store/welcome.reducer';
import {PhotoGalleryComponent} from './body/photo-gallery/photo-gallery.component';
import {GoldenRulesComponent} from './body/golden-rules/golden-rules.component';
import {CentersComponent} from './body/centers/centers.component';
import {SongsWithScriptComponent} from './body/songs-with-script/songs-with-script.component';
import {DataService} from './data/data.service';
import {HttpClientModule} from '@angular/common/http';
import {EffectsModule} from '@ngrx/effects';
import {WelcomeEffects} from './store/welcome.effects';
import {AngularFireDatabaseModule} from '@angular/fire/database';
import {MalihuScrollbarModule} from 'ngx-malihu-scrollbar';
import {NgxPaginationModule} from 'ngx-pagination';
import {CreatePlaylistDialog} from './dialog/create-playlist-dialog/create-playlist-dialog';
import {PlaylistComponent} from './body/songs-with-script/playlist/playlist.component';
import {TwoTableComponent} from './body/centers/tables/two-table/two-table.component';
import {ThreeTableComponent} from './body/centers/tables/three-table/three-table.component';
import {ChennaiTableComponent} from './body/centers/tables/chennai-table/chennai-table.component';
import {ViewPlaylistDialogComponent} from './dialog/view-playlist-dialog/view-playlist-dialog.component';
import {SongsResolveGuardService} from '../guards/songs-resolve-guard.service';
import {PhoneNumberTableComponent} from './body/centers/phone-number-table/phone-number-table.component';
import {SongsTableComponent} from './body/songs-with-script/songs-table/songs-table.component';
import {SharedModule} from '../shared-module/shared.module';
import {SongsWithScriptHeaderComponent} from './body/songs-with-script/songs-with-script-header/songs-with-script-header.component';
import {VaguppuComponent} from './body/vaguppu/vaguppu.component';
import {DownloadTableComponent} from './table/download-table/download-table.component';
import {ViruthamComponent} from './body/virutham/virutham.component';
import {AbhiramiAndadiPathikamComponent} from './body/abhirami-andadi-pathikam/abhirami-andadi-pathikam.component';
import {MajorEventsAndOfficeBearersComponent} from './body/major-events-and-office-bearers/major-events-and-office-bearers.component';
import {MajorEventsComponent} from './body/major-events-and-office-bearers/major-events/major-events.component';
import {OfficeBearersComponent} from './body/major-events-and-office-bearers/office-bearers/office-bearers.component';
import {EventComponent} from './body/major-events-and-office-bearers/major-events/event/event.component';
import {EventBottomSheetComponent} from './bottom-sheet/event-bottom-sheet/event-bottom-sheet.component';
import {OfficeBearersTableComponent} from './body/major-events-and-office-bearers/office-bearers/office-bearers-table/office-bearers-table.component';
import {PaddhathiOfBhajanComponent} from './body/paddhathi-of-bhajan/paddhathi-of-bhajan.component';
import {ListOfSongsComponent} from './body/list-of-songs/list-of-songs.component';
import {ExcelExportService} from '../excel/excel-export.service';
import {AllSongsWithMeaningComponent} from './body/all-songs-with-meaning/all-songs-with-meaning.component';
import {ValliKalyanamComponent} from './body/valli-kalyanam/valli-kalyanam.component';
import {BhajansComponent} from './body/bhajans/bhajans.component';
import {ArticlesComponent} from './body/articles/articles.component';
import {SongsAdminControlComponent} from './body/songs-with-script/songs-admin-control/songs-admin-control.component';
import {AdminSongUpdateDialogComponent} from './dialog/admin-song-update-dialog/admin-song-update-dialog.component';


const welcomeRoutes: Routes = [
  {
    path: '', component: WelcomeComponent, resolve: {pageName: PageResolveGuardService}, children: [
      {path: 'AboutGuruji', component: AboutGurujiComponent, data: {title: 'About Guruji'}},
      {path: 'photoGallery', component: PhotoGalleryComponent, data: {title: 'Photo Gallery'}},
      {path: 'GoldenRules', component: GoldenRulesComponent, data: {title: 'Golden Rules & Message from Guruji'}},
      {path: 'Centers', component: CentersComponent, data: {title: 'Centers in India and World'}},
      {path: 'Vaguppu', component: VaguppuComponent, data: {title: 'Vaguppu'}},
      {path: 'Virutham', component: ViruthamComponent, data: {title: 'Virutham'}},
      {path: 'AbhiramiAndadiPathikam', component: AbhiramiAndadiPathikamComponent, data: {title: 'Abhirami Andadi Pathikam'}},
      {path: 'MajorEvents&OfficeBearers', component: MajorEventsAndOfficeBearersComponent, data: {title: 'Major Events & Office Bearers'}},
      {path: 'PaddhathiOfBhajan', component: PaddhathiOfBhajanComponent, data: {title: 'Paddhathi Of A Thiruppugazh Isai Vazhipadu Set'}},
      {path: 'ListOfSongs', component: ListOfSongsComponent, data: {title: 'List Of Songs'}},
      {path: 'AllSongsWithMeaning', component: AllSongsWithMeaningComponent, data: {title: 'All Songs With Meaning'}},
      {path: 'ValliKalyanam', component: ValliKalyanamComponent, data: {title: 'Valli Kalyanam'}},
      {path: 'Bhajans', component: BhajansComponent, data: {title: 'Bhajans'}},
      {path: 'Articles', component: ArticlesComponent, data: {title: 'Articles'}},
      {
        path: 'SongsWithScript', component: SongsWithScriptComponent,
        data: {title: 'Songs With Script and Audio'}
      },
    ]
  }
];

@NgModule({
  declarations: [
    WelcomeComponent,
    TitleComponent,
    AboutGurujiComponent,
    PhotoGalleryComponent,
    GoldenRulesComponent,
    CentersComponent,
    SongsWithScriptComponent,
    CreatePlaylistDialog,
    PlaylistComponent,
    TwoTableComponent,
    ThreeTableComponent,
    ChennaiTableComponent,
    ViewPlaylistDialogComponent,
    PhoneNumberTableComponent,
    SongsTableComponent,
    SongsWithScriptHeaderComponent,
    VaguppuComponent,
    DownloadTableComponent,
    ViruthamComponent,
    AbhiramiAndadiPathikamComponent,
    MajorEventsAndOfficeBearersComponent,
    MajorEventsComponent,
    OfficeBearersComponent,
    EventComponent,
    EventBottomSheetComponent,
    OfficeBearersTableComponent,
    PaddhathiOfBhajanComponent,
    ListOfSongsComponent,
    AllSongsWithMeaningComponent,
    ValliKalyanamComponent,
    BhajansComponent,
    ArticlesComponent,
    SongsAdminControlComponent,
    AdminSongUpdateDialogComponent
  ], imports: [
    CommonModule,
    SharedModule,
    HttpClientModule,
    NgxPaginationModule,
    AngularFireDatabaseModule,
    MalihuScrollbarModule.forRoot(),
    RouterModule.forChild(welcomeRoutes),
    EffectsModule.forFeature([WelcomeEffects]),
    StoreModule.forFeature('welcome', welcomeReducer)
  ],
  entryComponents: [AdminSongUpdateDialogComponent, CreatePlaylistDialog, ViewPlaylistDialogComponent, EventBottomSheetComponent],
  exports: [RouterModule],
  providers: [PageResolveGuardService, DataService, SongsResolveGuardService, ExcelExportService]
})
export class WelcomeModule {

}
