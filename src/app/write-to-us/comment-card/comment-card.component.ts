import {Component, Input, OnInit} from '@angular/core';

import {Comment, Reply} from '../write-to-us.component';
import {MatSnackBar} from '@angular/material';
import {User} from 'firebase';
import {Store} from '@ngrx/store';
import * as fromApp from '../../app-store/app.reducer';
import * as appActions from '../../app-store/app.actions';
import {LoginErrorSnackbarComponent} from '../../welcome/snackbar/login-error-snackbar/login-error-snackbar.component';


@Component({
  selector: 'app-comment-card',
  templateUrl: './comment-card.component.html',
  styleUrls: ['./comment-card.component.css']
})
export class CommentCardComponent implements OnInit {
  @Input('comment') comment: Comment;
  @Input('user') user: User;
  isReplyActive: boolean = false;

  constructor(private _snackBar: MatSnackBar, private appStore: Store<fromApp.State>) {
  }

  ngOnInit() {
  }

  activateReply() {
    if (this.user) {
      this.isReplyActive = !this.isReplyActive;
    } else {
      this.openLoginSnackBar();
    }
  }

  openLoginSnackBar() {
    this._snackBar.openFromComponent(LoginErrorSnackbarComponent, {
      duration: 3000,
      data: {message: 'reply', route: 'WriteToUs', queryParams: {}}
    });
  }

  submitReply(reply: HTMLTextAreaElement) {
    if (reply.value !== '') {
      const replyDoc: Reply = {
        id: this.comment.id,
        reply: {
          commentMsg: reply.value,
          commentedOn: new Date(),
          commentedBy: this.user.displayName,
          commentImage: this.user.photoURL,
          commentedUserUid: this.user.uid
        }
      };
      this.appStore.dispatch(new appActions.UpdateReply(replyDoc));
    } else {
      this._snackBar.open(`Please give a valid reply`, null, {
        duration: 3000
      });
    }

  }

}
