import {Component, Input, OnInit} from '@angular/core';
import {CommentData} from '../../write-to-us.component';

@Component({
  selector: 'app-reply-card',
  templateUrl: './reply-card.component.html',
  styleUrls: ['./reply-card.component.css']
})
export class ReplyCardComponent implements OnInit {

  @Input('reply') reply: CommentData;

  constructor() {
  }

  ngOnInit() {
  }

}
