import {Component, Inject, OnInit} from '@angular/core';
import {MAT_SNACK_BAR_DATA} from '@angular/material';
import {Router} from '@angular/router';

@Component({
  selector: 'app-comment-snackbar',
  templateUrl: './comment-snackbar.component.html',
  styleUrls: ['./comment-snackbar.component.css']
})
export class CommentSnackbarComponent implements OnInit {

  constructor(@Inject(MAT_SNACK_BAR_DATA) public data: { commentFrom: string, route: string, queryParams: Object },
              private router: Router) {
  }

  ngOnInit() {
  }

  redirectToComments() {
    this.router.navigate([this.data.route], {queryParams: this.data.queryParams});

  }

}
