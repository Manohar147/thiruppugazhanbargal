import {Component, OnInit} from '@angular/core';
import {Store} from '@ngrx/store';
import * as fromAuth from '../user-auth/auth-store/auth.reducer';
import * as fromApp from '../app-store/app.reducer';
import * as appActions from '../app-store/app.actions';
import {Observable} from 'rxjs';
import {FormControl, FormGroup, FormGroupDirective, NgForm, Validators} from '@angular/forms';
import {User} from 'firebase';
import {SignInDialogComponent} from '../welcome/dialog/sign-in-dialog/sign-in-dialog.component';
import {ErrorStateMatcher, MatDialog} from '@angular/material';
import {take} from 'rxjs/operators';

export interface Comment {
  id: string;
  comment: CommentData
}

export interface Reply {
  id: string;
  reply: ReplyData
}

export interface CommentData {
  commentedUserUid: string;
  commentedBy: string;
  commentedOn: Date;
  commentMsg: string;
  commentImage: string;
  replies: ReplyData[];
}

export interface ReplyData {
  commentedUserUid: string;
  commentedBy: string;
  commentedOn: Date;
  commentMsg: string;
  commentImage: string;
}

export class MyErrorStateMatcher implements ErrorStateMatcher {
  isErrorState(control: FormControl | null, form: FormGroupDirective | NgForm | null): boolean {
    const isSubmitted = form && form.submitted && !form.pristine;
    return !!(control && control.invalid && (control.dirty || control.touched || isSubmitted));
  }
}

@Component({
  selector: 'app-write-to-us',
  templateUrl: './write-to-us.component.html',
  styleUrls: ['./write-to-us.component.css']
})
export class WriteToUsComponent implements OnInit {

  commentForm: FormGroup;
  matcher = new MyErrorStateMatcher();
  public authState: Observable<fromAuth.State>;
  public appState: Observable<fromApp.State>;
  user: User;
  isDialogOpen = false;
  isLoggedIn: boolean = false;


  constructor(private authStore: Store<fromAuth.State>,
              private appStore: Store<fromApp.State>, public dialog: MatDialog) {
  }

  ngOnInit() {
    this.authState = this.authStore.select('auth');
    this.authStore.select('auth').subscribe(
      (data: fromAuth.State) => {
        this.user = data.user;
      }
    );
    this.authStore.select('auth').pipe(
      take(1),
    ).subscribe(
      (data: fromAuth.State) => {
        if (data.isLoggedIn === true) {
          this.isLoggedIn = data.isLoggedIn;
        }
        if (!this.isLoggedIn && !this.isDialogOpen) {
          setTimeout(() => {
            this.showLoginDialog();
          });
        }
      }
    );
    this.appState = this.appStore.select('app');
    this.commentForm = new FormGroup({
      comment: new FormControl(null, [Validators.required]),
    });
    this.dialog.afterAllClosed.subscribe(
      () => {
        this.isDialogOpen = false;
      }
    );
  }

  onSubmit() {
    if (this.commentForm.valid) {
      const Comment: CommentData = {
        commentedUserUid: this.user.uid,
        commentedBy: this.user.displayName,
        commentedOn: new Date(),
        commentMsg: this.commentForm.value.comment,
        commentImage: this.user.photoURL,
        replies: []
      };
      this.appStore.dispatch(new appActions.TrySaveComment(Comment));
      this.commentForm.reset();
      this.commentForm.markAsPristine();
    }
  }

  showLoginDialog() {
    this.dialog.open(SignInDialogComponent, {
      data: {
        route: 'WriteToUs',
        queryParams: {}
      }
    });
    this.isDialogOpen = true;

  }
}
