// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
   firebaseConfig : {
    apiKey: 'AIzaSyBZj1DcjriYkzGsu6su5mgVdzFD0yC4-r8',
    authDomain: 'thiruppugazhanbargal-515e8.firebaseapp.com',
    databaseURL: 'https://thiruppugazhanbargal-515e8.firebaseio.com',
    projectId: 'thiruppugazhanbargal-515e8',
    storageBucket: 'thiruppugazhanbargal-515e8.appspot.com',
    messagingSenderId: '480701966077',
    appId: '1:480701966077:web:35118115fd6e0237'
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
