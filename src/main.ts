import {enableProdMode} from '@angular/core';
import {platformBrowserDynamic} from '@angular/platform-browser-dynamic';
import 'hammerjs';

import {AppModule} from './app/app.module';
import {environment} from './environments/environment';

if (environment.production) {
  enableProdMode();
}
try {
  platformBrowserDynamic().bootstrapModule(AppModule)
    .catch(err => console.error(err));

  if (localStorage.getItem('theme')) {
    document.getElementById('themeLink').setAttribute('href', localStorage.getItem('theme'));
  } else {
    localStorage.setItem('theme', 'https://thiruppugazhanbargal-515e8.firebaseapp.com/assets/themes/indigo-pink.css');
  }
} catch (e) {
  console.error('An Error has occurred ', e);
}





